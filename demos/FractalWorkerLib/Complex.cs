using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Platform.Demo.FractalGenerator.Worker
{
    public struct Complex
    {
        public double real;
        public double imaginary;

        public Complex(double real, double imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }

        public double Abs()
        {
            return Math.Sqrt(real*real + imaginary*imaginary);
        }

        public static Complex Sin(Complex z)
        {
            //sin(x+iy) = sin(x)cosh(y) + i*cos(x)sinh(y)
            return new Complex(Math.Sin(z.real) * Math.Cosh(z.imaginary), Math.Cos(z.real) * Math.Sinh(z.imaginary));
        }

        public static Complex Cos(Complex z)
        {
            //cos(x+iy) = cos(x)cosh(y) - i*sin(x)sinh(y)
            return new Complex(Math.Cos(z.real) * Math.Cosh(z.imaginary), Math.Sin(z.real) * Math.Sinh(z.imaginary));
        }

        public static Complex Log(Complex z)
        {
            double argZ = Math.Atan2(z.imaginary, z.real);
            return new Complex(Math.Log(z.real), argZ);
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1.real + c2.real, c1.imaginary + c2.imaginary);
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1.real - c2.real, c1.imaginary - c2.imaginary);
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex((c1.real*c2.real) - (c1.imaginary*c2.imaginary), (c1.imaginary*c2.real) + (c1.real*c2.imaginary));
        }

        public static Complex operator /(Complex c1, Complex c2)
        {
            double denominator = c2.real * c2.real + c2.imaginary * c2.imaginary;
            Complex numerator = new Complex(c1.real * c2.real + c1.imaginary * c2.imaginary,
                                            c1.imaginary * c2.real - (c1.real * c2.imaginary));
            return new Complex(numerator.real / denominator, numerator.imaginary / denominator);
        }
    }
}
