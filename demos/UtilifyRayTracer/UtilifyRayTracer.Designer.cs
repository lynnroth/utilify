﻿namespace Utilify.Platform.Demo.UtilifyRayTracer
{
    partial class UtilifyRayTracer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rowsUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLocalRender = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renderingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.antiAliasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.veryHighToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phongHighlightsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.castShadowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reflectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUtilifyRayTracerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbSceneLocal = new System.Windows.Forms.PictureBox();
            this.pbSceneUtilify = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ballsUpDown = new System.Windows.Forms.NumericUpDown();
            this.lblStatusLocal = new System.Windows.Forms.Label();
            this.lblStatusUtilify = new System.Windows.Forms.Label();
            this.btnUtilifyRender = new System.Windows.Forms.Button();
            this.lblStatusJobs = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRenderAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rowsUpDown)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSceneLocal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSceneUtilify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ballsUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // rowsUpDown
            // 
            this.rowsUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rowsUpDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.rowsUpDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rowsUpDown.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rowsUpDown.ForeColor = System.Drawing.Color.Gainsboro;
            this.rowsUpDown.Location = new System.Drawing.Point(702, 434);
            this.rowsUpDown.Name = "rowsUpDown";
            this.rowsUpDown.Size = new System.Drawing.Size(37, 17);
            this.rowsUpDown.TabIndex = 26;
            this.rowsUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rowsUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gainsboro;
            this.label4.Location = new System.Drawing.Point(651, 434);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = " | Slices:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnLocalRender
            // 
            this.btnLocalRender.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLocalRender.BackColor = System.Drawing.SystemColors.Control;
            this.btnLocalRender.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLocalRender.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalRender.Location = new System.Drawing.Point(330, 432);
            this.btnLocalRender.Name = "btnLocalRender";
            this.btnLocalRender.Size = new System.Drawing.Size(80, 19);
            this.btnLocalRender.TabIndex = 22;
            this.btnLocalRender.Text = "Local Render";
            this.btnLocalRender.UseVisualStyleBackColor = false;
            this.btnLocalRender.Click += new System.EventHandler(this.localRender_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(839, 24);
            this.menuStrip1.TabIndex = 32;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renderingToolStripMenuItem,
            this.connectionSettingsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // renderingToolStripMenuItem
            // 
            this.renderingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.antiAliasToolStripMenuItem,
            this.phongHighlightsToolStripMenuItem,
            this.castShadowsToolStripMenuItem,
            this.reflectionsToolStripMenuItem,
            this.refractionToolStripMenuItem});
            this.renderingToolStripMenuItem.Name = "renderingToolStripMenuItem";
            this.renderingToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.renderingToolStripMenuItem.Text = "Rendering";
            // 
            // antiAliasToolStripMenuItem
            // 
            this.antiAliasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noneToolStripMenuItem,
            this.quickToolStripMenuItem,
            this.lowToolStripMenuItem,
            this.mediumToolStripMenuItem,
            this.highToolStripMenuItem,
            this.veryHighToolStripMenuItem});
            this.antiAliasToolStripMenuItem.Name = "antiAliasToolStripMenuItem";
            this.antiAliasToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.antiAliasToolStripMenuItem.Text = "Anti Aliasing";
            // 
            // noneToolStripMenuItem
            // 
            this.noneToolStripMenuItem.CheckOnClick = true;
            this.noneToolStripMenuItem.Name = "noneToolStripMenuItem";
            this.noneToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.noneToolStripMenuItem.Text = "None";
            this.noneToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click);
            // 
            // quickToolStripMenuItem
            // 
            this.quickToolStripMenuItem.CheckOnClick = true;
            this.quickToolStripMenuItem.Name = "quickToolStripMenuItem";
            this.quickToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.quickToolStripMenuItem.Text = "Quick";
            this.quickToolStripMenuItem.Click += new System.EventHandler(this.quickToolStripMenuItem_Click);
            // 
            // lowToolStripMenuItem
            // 
            this.lowToolStripMenuItem.CheckOnClick = true;
            this.lowToolStripMenuItem.Name = "lowToolStripMenuItem";
            this.lowToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.lowToolStripMenuItem.Text = "Low";
            this.lowToolStripMenuItem.Click += new System.EventHandler(this.lowToolStripMenuItem_Click);
            // 
            // mediumToolStripMenuItem
            // 
            this.mediumToolStripMenuItem.Checked = true;
            this.mediumToolStripMenuItem.CheckOnClick = true;
            this.mediumToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mediumToolStripMenuItem.Name = "mediumToolStripMenuItem";
            this.mediumToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.mediumToolStripMenuItem.Text = "Medium";
            this.mediumToolStripMenuItem.Click += new System.EventHandler(this.mediumToolStripMenuItem_Click);
            // 
            // highToolStripMenuItem
            // 
            this.highToolStripMenuItem.CheckOnClick = true;
            this.highToolStripMenuItem.Name = "highToolStripMenuItem";
            this.highToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.highToolStripMenuItem.Text = "High";
            this.highToolStripMenuItem.Click += new System.EventHandler(this.highToolStripMenuItem_Click);
            // 
            // veryHighToolStripMenuItem
            // 
            this.veryHighToolStripMenuItem.CheckOnClick = true;
            this.veryHighToolStripMenuItem.Name = "veryHighToolStripMenuItem";
            this.veryHighToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.veryHighToolStripMenuItem.Text = "Very High";
            this.veryHighToolStripMenuItem.Click += new System.EventHandler(this.veryHighToolStripMenuItem_Click);
            // 
            // phongHighlightsToolStripMenuItem
            // 
            this.phongHighlightsToolStripMenuItem.Checked = true;
            this.phongHighlightsToolStripMenuItem.CheckOnClick = true;
            this.phongHighlightsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.phongHighlightsToolStripMenuItem.Name = "phongHighlightsToolStripMenuItem";
            this.phongHighlightsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.phongHighlightsToolStripMenuItem.Text = "Phong Highlights";
            // 
            // castShadowsToolStripMenuItem
            // 
            this.castShadowsToolStripMenuItem.Checked = true;
            this.castShadowsToolStripMenuItem.CheckOnClick = true;
            this.castShadowsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.castShadowsToolStripMenuItem.Name = "castShadowsToolStripMenuItem";
            this.castShadowsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.castShadowsToolStripMenuItem.Text = "Cast Shadows";
            // 
            // reflectionsToolStripMenuItem
            // 
            this.reflectionsToolStripMenuItem.Checked = true;
            this.reflectionsToolStripMenuItem.CheckOnClick = true;
            this.reflectionsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.reflectionsToolStripMenuItem.Name = "reflectionsToolStripMenuItem";
            this.reflectionsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.reflectionsToolStripMenuItem.Text = "Reflection";
            // 
            // refractionToolStripMenuItem
            // 
            this.refractionToolStripMenuItem.Checked = true;
            this.refractionToolStripMenuItem.CheckOnClick = true;
            this.refractionToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.refractionToolStripMenuItem.Name = "refractionToolStripMenuItem";
            this.refractionToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.refractionToolStripMenuItem.Text = "Refraction";
            // 
            // connectionSettingsToolStripMenuItem
            // 
            this.connectionSettingsToolStripMenuItem.Name = "connectionSettingsToolStripMenuItem";
            this.connectionSettingsToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.connectionSettingsToolStripMenuItem.Text = "Connection Settings...";
            this.connectionSettingsToolStripMenuItem.Click += new System.EventHandler(this.connectionSettingsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutUtilifyRayTracerToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.aboutToolStripMenuItem.Text = "Help";
            // 
            // aboutUtilifyRayTracerToolStripMenuItem
            // 
            this.aboutUtilifyRayTracerToolStripMenuItem.Name = "aboutUtilifyRayTracerToolStripMenuItem";
            this.aboutUtilifyRayTracerToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.aboutUtilifyRayTracerToolStripMenuItem.Text = "About Utilify Ray Tracer";
            this.aboutUtilifyRayTracerToolStripMenuItem.Click += new System.EventHandler(this.aboutUtilifyRayTracerToolStripMenuItem_Click);
            // 
            // pbSceneLocal
            // 
            this.pbSceneLocal.BackColor = System.Drawing.Color.Black;
            this.pbSceneLocal.Location = new System.Drawing.Point(12, 27);
            this.pbSceneLocal.Name = "pbSceneLocal";
            this.pbSceneLocal.Size = new System.Drawing.Size(400, 400);
            this.pbSceneLocal.TabIndex = 33;
            this.pbSceneLocal.TabStop = false;
            // 
            // pbSceneUtilify
            // 
            this.pbSceneUtilify.BackColor = System.Drawing.Color.Black;
            this.pbSceneUtilify.Location = new System.Drawing.Point(427, 27);
            this.pbSceneUtilify.Name = "pbSceneUtilify";
            this.pbSceneUtilify.Size = new System.Drawing.Size(400, 400);
            this.pbSceneUtilify.TabIndex = 34;
            this.pbSceneUtilify.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(21, 460);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Number of Balls: ";
            // 
            // ballsUpDown
            // 
            this.ballsUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ballsUpDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.ballsUpDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ballsUpDown.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ballsUpDown.ForeColor = System.Drawing.Color.Gainsboro;
            this.ballsUpDown.Location = new System.Drawing.Point(112, 459);
            this.ballsUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.ballsUpDown.Name = "ballsUpDown";
            this.ballsUpDown.Size = new System.Drawing.Size(42, 17);
            this.ballsUpDown.TabIndex = 37;
            this.ballsUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ballsUpDown.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.ballsUpDown.ValueChanged += new System.EventHandler(this.ballsUpDown_ValueChanged);
            // 
            // lblStatusLocal
            // 
            this.lblStatusLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.lblStatusLocal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusLocal.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblStatusLocal.Location = new System.Drawing.Point(12, 430);
            this.lblStatusLocal.Name = "lblStatusLocal";
            this.lblStatusLocal.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblStatusLocal.Size = new System.Drawing.Size(400, 23);
            this.lblStatusLocal.TabIndex = 38;
            this.lblStatusLocal.Text = "Local";
            this.lblStatusLocal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStatusUtilify
            // 
            this.lblStatusUtilify.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.lblStatusUtilify.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusUtilify.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblStatusUtilify.Location = new System.Drawing.Point(427, 430);
            this.lblStatusUtilify.Name = "lblStatusUtilify";
            this.lblStatusUtilify.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblStatusUtilify.Size = new System.Drawing.Size(400, 23);
            this.lblStatusUtilify.TabIndex = 39;
            this.lblStatusUtilify.Text = "Utilify";
            this.lblStatusUtilify.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUtilifyRender
            // 
            this.btnUtilifyRender.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUtilifyRender.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUtilifyRender.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUtilifyRender.Location = new System.Drawing.Point(745, 432);
            this.btnUtilifyRender.Name = "btnUtilifyRender";
            this.btnUtilifyRender.Size = new System.Drawing.Size(80, 19);
            this.btnUtilifyRender.TabIndex = 42;
            this.btnUtilifyRender.Text = "Utilify Render";
            this.btnUtilifyRender.UseVisualStyleBackColor = true;
            this.btnUtilifyRender.Click += new System.EventHandler(this.utilifyRender_Click);
            // 
            // lblStatusJobs
            // 
            this.lblStatusJobs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.lblStatusJobs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusJobs.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblStatusJobs.Location = new System.Drawing.Point(427, 456);
            this.lblStatusJobs.Name = "lblStatusJobs";
            this.lblStatusJobs.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblStatusJobs.Size = new System.Drawing.Size(400, 23);
            this.lblStatusJobs.TabIndex = 43;
            this.lblStatusJobs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(12, 456);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(400, 23);
            this.label3.TabIndex = 44;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRenderAll
            // 
            this.btnRenderAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRenderAll.BackColor = System.Drawing.SystemColors.Control;
            this.btnRenderAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRenderAll.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRenderAll.Location = new System.Drawing.Point(380, 485);
            this.btnRenderAll.Name = "btnRenderAll";
            this.btnRenderAll.Size = new System.Drawing.Size(80, 19);
            this.btnRenderAll.TabIndex = 45;
            this.btnRenderAll.Text = "Render All";
            this.btnRenderAll.UseVisualStyleBackColor = false;
            this.btnRenderAll.Click += new System.EventHandler(this.btnRenderAll_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(126)))), ((int)(((byte)(122)))));
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(12, 483);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(815, 23);
            this.label2.TabIndex = 46;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UtilifyRayTracer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 515);
            this.Controls.Add(this.btnRenderAll);
            this.Controls.Add(this.lblStatusJobs);
            this.Controls.Add(this.btnUtilifyRender);
            this.Controls.Add(this.ballsUpDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rowsUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnLocalRender);
            this.Controls.Add(this.lblStatusUtilify);
            this.Controls.Add(this.lblStatusLocal);
            this.Controls.Add(this.pbSceneUtilify);
            this.Controls.Add(this.pbSceneLocal);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "UtilifyRayTracer";
            this.Text = "Utilify Ray Tracer";
            ((System.ComponentModel.ISupportInitialize)(this.rowsUpDown)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSceneLocal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSceneUtilify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ballsUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown rowsUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLocalRender;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renderingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem antiAliasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mediumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem veryHighToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phongHighlightsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem castShadowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reflectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refractionToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbSceneLocal;
        private System.Windows.Forms.PictureBox pbSceneUtilify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ballsUpDown;
        private System.Windows.Forms.Label lblStatusLocal;
        private System.Windows.Forms.Label lblStatusUtilify;
        private System.Windows.Forms.ToolStripMenuItem aboutUtilifyRayTracerToolStripMenuItem;
        private System.Windows.Forms.Button btnUtilifyRender;
        private System.Windows.Forms.Label lblStatusJobs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRenderAll;
        private System.Windows.Forms.Label label2;
    }
}