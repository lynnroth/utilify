using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace Utilify.Platform.Demo.FractalGenerator
{
    public partial class PaletteDialog : Form
    {
        private ColorBlend blend;
        private int paletteSize;
        //private Color[] palette;

        //For Display
        private ColorBlend newBlend;
        private int newPaletteSize;

        //Gradient Graphics
        Graphics gradGraphics;
        LinearGradientBrush gradBrush;

        public PaletteDialog(int paletteSize, ColorBlend blend)
        {
            InitializeComponent();

            this.Blend = blend;
            this.NewBlend = blend;
            this.PaletteSize = paletteSize;
            this.NewPaletteSize = paletteSize;

            Console.WriteLine("NewPaletteSize:" + this.NewPaletteSize);
            Console.WriteLine("PaletteSize:" + this.PaletteSize);

            CustomInitializeComponent();
        }

        private void CustomInitializeComponent()
        {
            pbGradient.Image = new Bitmap(pbGradient.Width, pbGradient.Height);
            gradGraphics = Graphics.FromImage(pbGradient.Image);
            gradBrush = new LinearGradientBrush(pbGradient.ClientRectangle, Color.Black, Color.White, 0.0F);

            InitialiseColorBoxes();
            //OnColourChange();
            //UpdatePalette();
        }

        public void ApplyChanges()
        {
            //Copy display values to apply changes
            this.Blend = this.NewBlend;
            this.PaletteSize = this.NewPaletteSize;
            //Create Palette
            //UpdatePalette();
        }

        public void CancelChanges()
        {
            //Reset display values to the saved values
            this.NewBlend = this.Blend;
            this.NewPaletteSize = this.PaletteSize;
            //Re-initialise things
            InitialiseColorBoxes();
            UpdateGradient();
        }

        private void InitialiseColorBoxes()
        {
            //todoLater: Change to make this dynamic.
            pbColor1.BackColor = this.NewBlend.Colors[0];
            pbColor2.BackColor = this.NewBlend.Colors[1];
            pbColor3.BackColor = this.NewBlend.Colors[2];
            
            //todoLater: Add listeners for backColorChanged event?
        }

        private void OnColourChange()
        {
            //Update Blend
            UpdateBlend();
            //Update Palette
            //UpdatePalette();
            //Redraw Gradient
            UpdateGradient();
        }

        private void UpdateGradient()
        {
            gradBrush.InterpolationColors = this.NewBlend;
            gradGraphics.FillRectangle(gradBrush, pbGradient.ClientRectangle);
            pbGradient.Refresh();
        }

        private void UpdateBlend()
        {
            this.NewBlend.Colors[0] = pbColor1.BackColor;
            this.NewBlend.Colors[1] = pbColor2.BackColor;
            this.NewBlend.Colors[2] = pbColor3.BackColor;
        }
/*
        private void UpdatePalette()
        {
            this.Palette = new Color[this.NewPaletteSize];
            Rectangle rect = new Rectangle(0, 0, this.NewPaletteSize, 1);
            Bitmap map = new Bitmap(this.NewPaletteSize, 1);
            Graphics gr = Graphics.FromImage(map);
            LinearGradientBrush brush = new LinearGradientBrush(rect, Color.Black, Color.White, 0.0F);
            for (int i = 0; i < this.NewBlend.Colors.Length; i++)
            {
                Console.WriteLine(String.Format("(R,G,B):({0},{1},{2})", this.NewBlend.Colors[i].R, this.NewBlend.Colors[i].G, this.NewBlend.Colors[i].B));
            }
            brush.InterpolationColors = this.NewBlend;
            gr.FillRectangle(brush, rect);

            //Console.WriteLine("__Palette__");
            for (int i = 0; i < this.NewPaletteSize; i++)
            {
                this.Palette[this.NewPaletteSize - (i+1)] = map.GetPixel(i, 0);
                //Console.WriteLine(String.Format("(R,G,B):({0},{1},{2})", palette[i].R, palette[i].G, palette[i].B));
            }
        }
*/
        public ColorBlend Blend
        {
            get { return blend; }
            set
            {
                Console.WriteLine("length:" + value.Colors.Length);
                this.blend = new ColorBlend(value.Colors.Length);
                for (int i = 0; i < value.Colors.Length; i++)
                {
                    this.blend.Colors[i] = value.Colors[i];
                    this.blend.Positions[i] = value.Positions[i];
                }
            }
        }

        public int PaletteSize
        {
            get { return paletteSize; }
            private set
            {
                paletteSize = value;
            }
        }

/*
        public Color[] Palette
        {
            get { return palette; }
            private set { palette = value; }
        }
*/

        private ColorBlend NewBlend
        {
            get { return newBlend; }
            set
            {
                this.newBlend = new ColorBlend(value.Colors.Length);
                for (int i = 0; i < value.Colors.Length; i++)
                {
                    this.newBlend.Colors[i] = value.Colors[i];
                    this.newBlend.Positions[i] = value.Positions[i];
                }
            }
        }

        private int NewPaletteSize
        {
            get { return newPaletteSize; }
            set
            {
                newPaletteSize = value;
            }
        }
        
        private void pbColor1_Click(object sender, EventArgs e)
        {
            if (cp.ShowDialog() == DialogResult.OK)
            {
                pbColor1.BackColor = cp.Color;
                OnColourChange();
            }
        }

        private void pbColor2_Click(object sender, EventArgs e)
        {
            if (cp.ShowDialog() == DialogResult.OK)
            {
                pbColor2.BackColor = cp.Color;
                OnColourChange();
            }
        }

        private void pbColor3_Click(object sender, EventArgs e)
        {
            if (cp.ShowDialog() == DialogResult.OK)
            {
                pbColor3.BackColor = cp.Color;
                OnColourChange();
            }
        }
    }
}