using System;
using System.Collections.Generic;
using System.Text;

using Utilify.Platform;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Linux
{
    internal class OsInfoUtil
    {
        //todoLater: Finish implementation to get the correct version

        public static OSInfo GetOsInfo()
        {
            string OsName = OSInfoHelper.GetPlatform().ToString();
            string OsVersion = Environment.OSVersion.Version.ToString();

            OSInfo osInfo = new OSInfo(OsName, OsVersion);

            return osInfo;
        }
    }
}
