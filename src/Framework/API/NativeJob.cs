using System;
using System.Collections.Generic;
using Utilify.Platform;

namespace Utilify.Framework
{
    /// <summary>
    /// Represents a job that runs native executable code (for example, a .exe file in Windows) remotely.
    /// </summary>
    [Serializable]
    public class NativeJob : NativeExecutable, IDependent, IResults
    {
        private string command;
        private string arguments;
        private IList<DependencyInfo> inputFiles;
        private IList<ResultInfo> outputFiles;

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeJob"/> class.
        /// </summary>
        public NativeJob()
        {
            inputFiles = new List<DependencyInfo>();
            outputFiles = new List<ResultInfo>();
        }

        /// <summary>
        /// Executes the job with the specified context.
        /// </summary>
        /// <param name="context">The job context.</param>
        public override void Execute(ExecutionContext context)
        {
            context.Log.AppendLine("Command is: " + command);
            command = Environment.ExpandEnvironmentVariables(command);
            context.Log.AppendLine("Command is: " + command);
            context.Log.AppendLine("Arguments are: " + arguments);

            ExecutionArgs executionArgs =
                new ExecutionArgs(command, arguments, "stdout.txt", "stderr.txt", context.WorkingDirectory);
            // Raise event to start native execution.
            NativeExecute(executionArgs);
        }

        #region Properties

        /// <summary>
        /// Gets or sets the command for the executable.
        /// </summary>
        /// <value>The command.</value>
        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        /// <summary>
        /// Gets or sets the arguments.
        /// </summary>
        /// <value>The arguments.</value>
        public string Arguments
        {
            get { return arguments; }
            set { arguments = value; }
        }

        /// <summary>
        /// Gets the list of input files that are needed to run this job.
        /// </summary>
        /// <value>The input files.</value>
        public IList<DependencyInfo> InputFiles
        {
            get { return inputFiles; }
        }

        /// <summary>
        /// Gets the list of output files that will be produced by this job.
        /// </summary>
        /// <value>The output files.</value>
        public IList<ResultInfo> OutputFiles
        {
            get { return outputFiles; }
        }

        #endregion

        #region IResults Members

        /// <summary>
        /// Gets the list of results expected upon execution of this job.
        /// </summary>
        /// <returns></returns>
        public ResultInfo[] GetExpectedResults()
        {
            return ((List<ResultInfo>)outputFiles).ToArray();
        }

        #endregion

        #region IDependent Members

        /// <summary>
        /// Gets the list of dependencies for this job.
        /// </summary>
        /// <returns></returns>
        public DependencyInfo[] GetDependencies()
        {
            return ((List<DependencyInfo>)inputFiles).ToArray();
        }

        #endregion
    }
}
