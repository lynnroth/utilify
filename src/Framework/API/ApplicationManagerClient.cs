using System;
using System.Collections.Generic;
using Utilify.Platform;

namespace Utilify.Framework
{
    /// <summary>
    /// Provides an alternate API to submit, and manage applications on the Utilify Platform.
    /// The application manager client is designed for use in stateless contexts like a web application.
    /// It does not have long-running internal threads, and does not automatically monitor the status of the application or its jobs.
    /// </summary>
    public sealed class ApplicationManagerClient : IDisposable
    {
        private Logger logger;
        private IApplicationManager manager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationManagerClient"/> class.
        /// </summary>
        public ApplicationManagerClient() : this(ConnectionSettings.Default) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationManagerClient"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public ApplicationManagerClient(ConnectionSettings settings)
        {
            logger = new Logger();
            this.manager = ProxyFactory.CreateProxyInstance<IApplicationManager>(settings);
        }

        /// <summary>
        /// Checks if the service is available. The service is considered available to accept requests if this method does not throw an exception.
        /// </summary>
        public void CheckService()
        {
            VerifyDisposed();

            manager.CheckService();
        }

        /// <summary>
        /// Submits the application.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        /// <returns></returns>
        public string SubmitApplication(string applicationName)
        {
            VerifyDisposed();

            if (applicationName == null)
                throw new ArgumentNullException("applicationName");
            if (applicationName.Trim().Length == 0)
                throw new ArgumentException("Application name cannot be empty", "applicationName");

            return SubmitApplication(applicationName, null, QosInfo.GetDefault());
        }

        /// <summary>
        /// Submits the application.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <returns></returns>
        public string SubmitApplication(string applicationName, IEnumerable<DependencyInfo> dependencies)
        {
            VerifyDisposed();

            if (applicationName == null)
                throw new ArgumentNullException("applicationName");
            if (applicationName.Trim().Length == 0)
                throw new ArgumentException("Application name cannot be empty", "applicationName");

            return SubmitApplication(applicationName, dependencies, QosInfo.GetDefault());
        }

        /// <summary>
        /// Submits the application.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="qos">The qos.</param>
        /// <returns></returns>
        public string SubmitApplication(string applicationName, IEnumerable<DependencyInfo> dependencies, QosInfo qos)
        {
            VerifyDisposed();

            if (applicationName == null)
                throw new ArgumentNullException("applicationName");
            if (applicationName.Trim().Length == 0)
                throw new ArgumentException("Application name cannot be empty", "applicationName");
            if (qos == null)
                throw new ArgumentNullException("qos");

            List<DependencyInfo> deps = new List<DependencyInfo>();
            if (dependencies != null)
            {
                foreach (DependencyInfo dep in dependencies)
                {
                    if (dep != null)
                        deps.Add(dep);
                }
            }
            ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(applicationName, deps.ToArray(), qos);

            string appId = manager.SubmitApplication(appInfo);

            if (appId == null || appId.Trim().Length == 0)
                throw new ApplicationInitializationException("Could not initialize application.");

            return appId;
        }

        /// <summary>
        /// Pauses the application.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        public void PauseApplication(string applicationId)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");
                
            manager.PauseApplication(applicationId);
        }

        /// <summary>
        /// Resumes the application.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        public void ResumeApplication(string applicationId)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            manager.ResumeApplication(applicationId);
        }

        /// <summary>
        /// Cancels the application.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        public void CancelApplication(string applicationId)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            manager.AbortApplication(applicationId);
        }

        /// <summary>
        /// Gets the un resolved dependencies.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="scope">The scope.</param>
        /// <returns></returns>
        public DependencyInfo[] GetUnResolvedDependencies(string id, DependencyScope scope)
        {
            VerifyDisposed();

            if (id == null)
                throw new ArgumentNullException("id");
            if (id.Trim().Length == 0)
                throw new ArgumentException("Application/Job id cannot be empty", "id");

            DependencyInfo[] deps = null;

            deps = manager.GetUnresolvedDependencies(new DependencyQueryInfo(id, scope));

            return deps;
        }

        /// <summary>
        /// Sends the content of the dependency.
        /// </summary>
        /// <param name="dependencyId">The dependency id.</param>
        /// <param name="fileName">Name of the file.</param>
        public void SendDependencyContent(string dependencyId, string fileName)
        {
            VerifyDisposed();

            if (dependencyId == null)
                throw new ArgumentNullException("dependencyId");
            if (dependencyId.Trim().Length == 0)
                throw new ArgumentException("Dependency id cannot be empty", "dependencyId");

            if (fileName == null)
                throw new ArgumentNullException("fileName");
            if (fileName.Trim().Length == 0)
                throw new ArgumentException("Dependency file name cannot be empty", "fileName");

            byte[] data = IOHelper.ReadFile(fileName);
            manager.SendDependency(new DependencyContent(dependencyId, data));
        }

        /// <summary>
        /// Submits the job.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="job">The job.</param>
        /// <returns></returns>
        public string SubmitJob(string applicationId, IExecutable job)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");
            if (job == null)
                throw new ArgumentNullException("job");

            if (!job.GetType().IsSerializable)
                throw new ArgumentException(string.Format("Invalid job class. The type '{0}' has to be serializable.", job.GetType().FullName),
                    "job");

            return SubmitJob(applicationId, job, true);
        }

        /// <summary>
        /// Submits the job.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="job">The job.</param>
        /// <param name="autoDetectDependencies">if set to <c>true</c> [auto detect dependencies].</param>
        /// <returns></returns>
        public string SubmitJob(string applicationId, IExecutable job, bool autoDetectDependencies)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");
            if (job == null)
                throw new ArgumentNullException("job");

            string[] ids = SubmitJobs(applicationId, new IExecutable[] { job }, autoDetectDependencies);
            if (ids != null && ids.Length == 1)
                return ids[0];

            throw new JobSubmissionException("Could not submit job: " + Errors.InvalidServerResponseMessage);
	    }

        /// <summary>
        /// Submits the jobs.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="jobs">The jobs.</param>
        /// <param name="autoDetectDependencies">if set to <c>true</c> [auto detect dependencies].</param>
        /// <returns></returns>
        public string[] SubmitJobs(string applicationId, IEnumerable<IExecutable> jobs, bool autoDetectDependencies)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            if (jobs == null)
                throw new ArgumentNullException("jobs");

            List<string> jobIds = new List<string>();

            foreach (IExecutable job in jobs)
            {
                if (job == null)
                    throw new ArgumentException("Elements of the job list cannot be null.", "jobs");

                if (!job.GetType().IsSerializable)
                    throw new ArgumentException(string.Format("Invalid job class. The type '{0}' has to be serializable.", job.GetType().FullName),
                        "job");

                logger.Debug("Getting declared dependencies for job...");

                // 1. find dependencies
                List<DependencyInfo> deps = new List<DependencyInfo>();
                // if the object implements this interface, then depJob won't be null
                IDependent depJob = job as IDependent;
                if (depJob != null)
                {
                    DependencyInfo[] jobDeps = depJob.GetDependencies();
                    if (jobDeps != null && jobDeps.Length > 0)
                    {
                        for (int i = 0; i < jobDeps.Length; i++)
                        {
                            deps.Add(jobDeps[i]);
                        }
                    }
                }

                logger.Debug("Autodetect dependencies turned on = " + autoDetectDependencies);
                // 1.1 If needed: auto-detect and add module dependencies
                if (autoDetectDependencies)
                {
                    logger.Debug("Auto-detecting dependencies for job...");
                    IList<DependencyInfo> detectedDeps = DependencyResolver.DetectDependencies(job.GetType());
                    foreach (DependencyInfo dep in detectedDeps)
                    {
                        // if the module is found in the app's common dependencies
                        // or
                        // user-declared dependencies for this job :ignore it
                        // otherwise add it to the job's dependency list
                        if (!deps.Contains(dep))
                            deps.Add(dep);
                    }
                }

                logger.Debug("Getting declared result info. for job...");
                // 2 --- find expected results
                List<ResultInfo> res = new List<ResultInfo>();
                IResults resJob = job as IResults;
                if (resJob != null)
                {
                    ResultInfo[] results = resJob.GetExpectedResults();
                    if (results != null && results.Length > 0)
                    {
                        for (int i = 0; i < results.Length; i++)
                        {
                            res.Add(results[i]);
                        }
                    }
                }

                logger.Debug("Serializing job...");
                // 3 --- job instance
                byte[] instance = SerializationHelper.Serialize(job);

                // 4 find job type
                JobType type;
                if (job is NativeExecutable)
                    type = JobType.NativeModule;
                else
                    type = JobType.CodeModule;

                // 5 --- create submissioninfo to send this job off remotely
                JobSubmissionInfo jobInfo = new JobSubmissionInfo(applicationId, type, instance, deps, res);

                logger.Debug("Submitting job to remote manager...");
                string[] jobIdArray = manager.SubmitJobs(new JobSubmissionInfo[] { jobInfo });
                string jobId = null;
                if (jobIdArray != null && jobIdArray.Length == 1)
                {
                    jobId = jobIdArray[0];
                }
                else
                {
                    throw new JobSubmissionException("Could not retrieve job id from the server");
                }
                jobIds.Add(jobId);
            }
            return jobIds.ToArray();
        }

        /// <summary>
        /// Cancels the job.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        public void CancelJob(string jobId)
        {
            VerifyDisposed();

            if (jobId == null)
                throw new ArgumentNullException("jobId");
            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            manager.AbortJob(jobId);
        }

        /// <summary>
        /// Gets the application status.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <returns></returns>
        public ApplicationStatus GetApplicationStatus(string applicationId)
        {
            VerifyDisposed();

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            ApplicationStatus status = ApplicationStatus.UnInitialized;

            ApplicationStatusInfo info = manager.GetApplicationStatus(applicationId);
            if (info != null)
                status = info.Status;
            else
                throw new PlatformException("Could not get application status: " + Errors.InvalidServerResponseMessage);
            
            return status;
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        public JobStatus GetJobStatus(string jobId)
        {
            VerifyDisposed();

            if (jobId == null)
                throw new ArgumentNullException("jobId");
            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            List<string> jobIds = new List<string>();
            jobIds.Add(jobId);
            List<JobStatus> statuses = GetJobStatus(jobIds);

            if (statuses == null || statuses.Count != 1)
                throw new PlatformException("Could not get job status: " + Errors.InvalidServerResponseMessage);
            
            return statuses[0];
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <param name="jobIds">The job ids.</param>
        /// <returns></returns>
        public List<JobStatus> GetJobStatus(IEnumerable<string> jobIds)
        {
            VerifyDisposed();

            if (jobIds == null)
                throw new ArgumentNullException("jobIds");
		
            List<string> ids = new List<string>();
		    foreach (string jobId in jobIds)
            {
                if (jobId == null || jobId.Trim().Length == 0 )
                    throw new ArgumentException("Elements of jobIds list cannot be null or empty");
                ids.Add(jobId);
		    }
            if (ids.Count == 0)
                throw new ArgumentException("Job ids list cannot be empty", "jobIds");

		    JobStatusInfo[] infos = manager.GetJobStatus(ids.ToArray());
		    if (infos == null || infos.Length == 0)
			    throw new PlatformException("Could not get job status: " + Errors.InvalidServerResponseMessage);

            List<JobStatus> statuses = new List<JobStatus>();
		    foreach (JobStatusInfo info in infos) 
            {
			    if (info == null)
                    throw new PlatformException("Could not get job status: " + Errors.InvalidServerResponseMessage);
			    statuses.Add(info.Status);
		    }
            
		    return statuses;
	    }

        /// <summary>
        /// Gets the completed job.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        public IExecutable GetCompletedJob(string jobId)
        {
            VerifyDisposed();

            if (jobId == null)
                throw new ArgumentNullException("jobId");
            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            JobCompletionInfo jobInfo = manager.GetCompletedJob(jobId);
            IExecutable job = null;
            if (jobInfo != null)
            {
                job = (IExecutable)SerializationHelper.Deserialize(jobInfo.GetJobInstance());
            }
            return job;
        }

        /// <summary>
        /// Gets the job results.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        public ResultStatusInfo[] GetJobResults(string jobId)
        {
            VerifyDisposed();

            if (jobId == null)
                throw new ArgumentNullException("jobId");
            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            ResultStatusInfo[] statuses = manager.GetJobResults(jobId);
            if (statuses == null || statuses.Length == 0)
                throw new PlatformException("Could not get job result status: " + Errors.InvalidServerResponseMessage);

            return statuses;
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="resultId">The result id.</param>
        /// <returns></returns>
        public byte[] GetResult(string resultId)
        {
            VerifyDisposed();

            if (resultId == null)
                throw new ArgumentNullException("resultId");
            if (resultId.Trim().Length == 0)
                throw new ArgumentException("Result id cannot be empty", "resultId");

            ResultContent content = manager.GetResult(resultId);
            byte[] data = null;
            if (content != null)
            {
                data = content.GetContent();
            }
            return data;
        }

        #region IDisposable Members
        private bool disposed = false;

        /*
         * This class has an IDisposable field (the proxy), but doesn't use unmanaged resources directly.
         * So, we implement the simple version of IDisposable - the one without a Finalizer. 
         * We don't need to call GC.SuppressFinalize() since we are not implementing a Finalizer.
         */
        /// <summary>
        /// See <see cref="M:System.IDisposable.Dispose">System.IDisposable.Dispose</see>
        /// </summary>
        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                //clean up managed stuff
                IDisposable disposableProxy = manager as IDisposable;
                disposableProxy.Dispose();

                manager = null;
            }

            //we have no base class to call dispose on, so nothing more to do here.

            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
        #endregion

    }
}