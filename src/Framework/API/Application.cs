using System;
using System.Collections.Generic;
using Utilify.Platform;

namespace Utilify.Framework
{
    //todoDoc
    //todoPerf: we could have a generic version of this, that basically does the same thing except, it will initially detect dependencies 
    //common for the job type, and add them to the application itself.
    //we almost dont need the non-generic version then, since we have a dependency on .NET 3.0 anyway!
    //for the generic version, we can set AutoDetect to false by default?
    /// <summary>
    /// Represents a distributed application consisting of a set of independent tasks that can be executed on the Utilify Platform.
    /// </summary>
    public partial class Application : IDisposable
    {
        private static NumberSequenceGenerator appIdSeq = new NumberSequenceGenerator();

        private IApplicationManager manager;
        private IList<DependencyInfo> dependencies;

        private string name;
        private string id;
        private ApplicationStatus status = ApplicationStatus.UnInitialized; //default
        private QosInfo qos = QosInfo.GetDefault(); //the budget part of this not used/tested at the moment.

        private bool autoDetectDependencies = true;

        /// <summary>
        /// Gets or sets a value indicating whether to automatically detect dependencies (.NET assembly references) for the jobs in an application.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the system should auto-detect dependencies; otherwise, <c>false</c>.
        /// </value>
        public bool AutoDetectDependencies
        {
            get { return autoDetectDependencies; }
            set 
            {
                if (this.status != ApplicationStatus.UnInitialized)
                    throw new InvalidOperationException("AutoDetectDependencies can be set only if an application is not already submitted.");
                autoDetectDependencies = value;
            }
        }

        //todoLater: in submit job, app and other places, should check deadline - passed etc. Qos stuff, when , where, how should the client be informed?

        #region public interface

        #region Events

        /// <summary>
        /// Raised when an error occurs during submission or execution of an application or job.
        /// </summary>
        public event EventHandler<ErrorEventArgs> Error;
        /// <summary>
        /// Occurs when the status of a job changes.
        /// </summary>
        public event EventHandler<StatusChangedEventArgs> JobStatusChanged;
        /// <summary>
        /// Occurs when a job is submitted to the Manager.
        /// </summary>
        public event EventHandler<JobSubmittedEventArgs> JobSubmitted;
        /// <summary>
        /// Occurs when a job has completed execution and the results (if any) are available on the client (i.e the machine that submitted this application).
        /// </summary>
        public event EventHandler<JobCompletedEventArgs> JobCompleted;

        #endregion

        #region ctors

        /// <summary>
        /// Creates an instance of the Application.
        /// </summary>
        public Application() : this("Application" + appIdSeq.GetNext(), null, null, ConnectionSettings.Default) {}

        /// <summary>
        /// Creates an instance of the Application with the given name.
        /// </summary>
        /// <param name="name">name of the application</param>
        /// <exception cref="System.ArgumentNullException">name is a null reference</exception>
        /// <exception cref="System.ArgumentException">name is empty</exception>
        public Application(string name) : this(name, null, null, ConnectionSettings.Default) { }


        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="settings">The connection settings.</param>
        public Application(string name, ConnectionSettings settings)
            : this(name, null, null, settings)
        {
        }

        /// <summary>
        /// Creates an instance of the Application with the given name and QoS.
        /// </summary>
        /// <param name="name">name of the application</param>
        /// <param name="qos">QoS settings for the application</param>
        /// <exception cref="System.ArgumentNullException">name is a null reference</exception>
        /// <exception cref="System.ArgumentException">name is empty</exception>
        public Application(string name, QosInfo qos)
            : this(name, qos, null, ConnectionSettings.Default)
        {
        }

        /// <summary>
        /// Creates an instance of the Application with the given name and list of dependencies.
        /// </summary>
        /// <param name="name">name of the application</param>
        /// <param name="dependencies">common dependencies for all jobs in the application</param>
        /// <exception cref="System.ArgumentNullException">name is a null reference</exception>
        /// <exception cref="System.ArgumentException">name is empty</exception>
        public Application(string name, IEnumerable<DependencyInfo> dependencies)
            : this(name, null, dependencies, ConnectionSettings.Default)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="qos">The qos.</param>
        /// <param name="dependencies">The dependencies.</param>
        public Application(string name, QosInfo qos, IEnumerable<DependencyInfo> dependencies)
            : this(name, qos, dependencies, ConnectionSettings.Default)
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="qos">The qos parameters.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="settings">The settings.</param>
        public Application(string name, QosInfo qos, IEnumerable<DependencyInfo> dependencies, ConnectionSettings settings)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Trim().Length == 0)
                throw new ArgumentException("Application name cannot be empty", "name");

            if (settings == null)
                throw new ArgumentNullException("settings");

            this.logger = new Logger();

            this.name = name;
            this.status = ApplicationStatus.UnInitialized;
            this.dependencies = new List<DependencyInfo>();

            this.backOff = new BackOffHelper(1000, 60000); //MAGIC numbers: need to factor out

            if (qos != null)
                this.qos = qos;
            else
                this.qos = QosInfo.GetDefault();

            if (dependencies != null)
            {
                foreach (DependencyInfo dep in dependencies)
                {
                    if (dep == null)
                        throw new ArgumentException("Elements of the dependencies list should not be null", "dependencies");
                    else
                        this.dependencies.Add(dep);
                }
            }

            this.manager = ProxyFactory.CreateProxyInstance<IApplicationManager>(settings);

            // Set Event Handlers for the ClientJobTable.
            jobTable.JobStatusChanged += new EventHandler<JobStatusEventArgs>(jobTable_JobStatusChanged);
        }

        void jobTable_JobStatusChanged(object sender, JobStatusEventArgs e)
        {
            // Wake up threads that were resting so they can do what they need.
            switch (e.Status)
            {
                case JobStatus.UnInitialized:
                case JobStatus.Completing:
                case JobStatus.Completed:                
                    jobsToSubmitWaitHandle.Set();
                    break;
                case JobStatus.Submitted:
                    jobsToMonitorWaitHandle.Set();
                    break;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Id of the application
        /// </summary>
        /// <remarks>If the application is not yet initialized, this returns an empty string.</remarks>
        public string Id
        {
            get { return id; }
        }
        
        /// <summary>
        /// Gets the name of the application
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the status of the application
        /// </summary>
        public ApplicationStatus Status
        {
            get 
            {
                ApplicationStatus s;
                lock (applicationStatusLock) 
                {
                    s = status;
                }
                return s; 
            }
            private set
            {
                lock (applicationStatusLock)
                {
                    status = value;
                }
            }
        }

        #endregion

        /// <summary>
        /// Adds a Job to the Application.
        /// Adding a Job to an Application causes it to be sent to the Manager for scheduling, if the Appication is already started.
        /// If the Application is not already started, the Job is queued locally, and is sent to the Manager when the Application is started.
        /// </summary>
        /// <param name="job">the Job to add</param>
        /// <returns>a remote Job instance which can be used to query the Job properties</returns>
        /// <exception cref="System.ArgumentNullException">Job is a null reference</exception>
        /// <exception cref="System.ArgumentException">The Job object is not serializable</exception>
        public Job AddJob(IExecutable job)
        {
            // todoDiscuss: What if user wants to catch Job submission exceptions?

            VerifyDisposed();

            if (job == null)
                throw new ArgumentNullException("job");

            if (!job.GetType().IsSerializable)
                throw new ArgumentException(string.Format("Invalid Job class. The type '{0}' has to be serializable.", job.GetType().FullName),
                    "job");

            // Create new Job. Initial status is 'UnInitialized'
            Job remoteJob = new Job(job);

            // Buffer jobs instead of sending away immediately
            // ClientJobTable will hold jobs in the UnInitialized state.
            // The JobSubmitter thread will pick up those Jobs and submit them.
            // This may take a bit long - since we may need to process file transfers - so not using the ThreadPool for the JobSubmitter.

            // Event handler on the ClientJobTable can wake up JobSubmitter by setting the WaitHandle.

            // Add Job to ClientJobTable.
            jobTable.Add(remoteJob);

            return remoteJob;
        }

        /// <summary>
        /// Cancels (i.e aborts) a remotely running Application and all of its Jobs.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Job is a null reference</exception>
        /// <exception cref="System.ArgumentException">The Job object is not serializable</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">Application could not be found</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public void Cancel()
        {
            VerifyDisposed();

            ApplicationStatus s = Status;
            if (s == ApplicationStatus.Stopped)
                throw new InvalidOperationException("The Application is already stopped.");
            if (s == ApplicationStatus.UnInitialized)
                throw new InvalidOperationException("The Application is not yet initialized.");

            manager.AbortApplication(this.Id);

            Status = ApplicationStatus.Stopped;

            // Get all Jobs from the ClientJobTable and Cancel them.
            Job[] allJobs = jobTable.Jobs;
            foreach (Job job in allJobs)
            {
                CancelJob(job);
            }
        }

        /// <summary>
        /// Cancels (i.e aborts) a Job.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Job is a null reference</exception>
        /// <exception cref="System.ArgumentException">The Job object is not serializable</exception>
        public void CancelJob(Job job)
        {
            // todoDiscuss: What if user wants to catch cancel job exceptions?

            VerifyDisposed();

            if (job == null)
                throw new ArgumentNullException("job");

            if (!jobTable.Exists(job))
                throw new ArgumentException("Cannot cancel an unknown job");

            //we now make ALL jobs follow the same path: submit to Manager and then cancel!
            //or else there will be inconsistencies, and we lose track of jobs which are in the client's submission queue.

            // If the job has been sent to the manager the job monitor will need to know to cancel it.
            // Tell the job monitor that the job should be cancelled.
            // If it is already cancelled or had a cancel requested, dont set it again.
            if (!job.IsCancelRequested
                && job.Status != JobStatus.Cancelled
                && job.Status != JobStatus.Completed)
                job.IsCancelRequested = true;
        }

        /// <summary>
        /// Retrieves the Dependency information for the Application
        /// </summary>
        /// <returns>an array containing the dependencies</returns>
        private DependencyInfo[] GetDependencies() //Issue 015: perhaps this doesn't need to be a public API
        {
            DependencyInfo[] deps = new DependencyInfo[dependencies.Count];
            if (deps.Length > 0)
                dependencies.CopyTo(deps, 0);
            return deps;
        }

        /// <summary>
        /// Retrieves the Result information for a specified Job.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Job is a null reference</exception>
        /// <exception cref="System.ArgumentException">The Job object is not serializable</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.ResultNotFoundException">one or more of the Results could not be found</exception>
        /// <exception cref="Utilify.Platform.PlatformException">Results were not successfully retrieved</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public ResultStatusInfo[] GetJobResults(Job job) 
        {
            VerifyDisposed();

            if (job == null)
                throw new ArgumentNullException("job");

            if (job.Id == null || job.Id.Trim().Length == 0)
                throw new ArgumentException("Could not get job results. Job is unknown or not yet submitted.");

            ResultStatusInfo[] results = new ResultStatusInfo[0];

            //ideally this method should be called only for jobs which have declared results and 
            //have also completed execution
            //if the job has not completed execution, but has declared results it will simply get the initial list
            IResults resultJob = job.InitialInstance as IResults;
            if (resultJob != null && resultJob.GetExpectedResults().Length > 0)
            {
                //the job has declared some results
                //so ask for results from the manager only if the job is completed / cancelled.
                results = manager.GetJobResults(job.Id);
                if (results == null)
                    throw new PlatformException("Could not get job result status: " + Errors.InvalidServerResponseMessage);
            }

            return results;
        }

        /// <summary>
        /// Retrieves the actual Result content (data) for a specified Result
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Job is a null reference</exception>
        /// <exception cref="System.ArgumentException">The Job object is not serializable</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.ResultNotFoundException">Results could not be found</exception>
        /// <exception cref="Utilify.Platform.PlatformException">Content for Result was not successfully retrieved</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public ResultContent GetResultContent(string resultId)
        {
            VerifyDisposed();

            if (resultId == null)
                throw new ArgumentNullException("resultId");
            if (resultId.Trim().Length == 0)
                throw new ArgumentException("Result id cannot be null or empty", "resultId");

            logger.Debug("Getting Content for Result: " + resultId);
            ResultContent content = manager.GetResult(resultId);
            if (content == null)
            {
                logger.Debug("Could not get result: " + resultId);
                throw new PlatformException("Could not get result: " + Errors.InvalidServerResponseMessage);
            }
            logger.Debug("Got Content for Result: " + resultId + " Content: " + content.GetContent());
            return content;
        }

        /// <summary>
        /// Starts an Application on the Manager.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">The Application is already started,
        /// <see cref="ApplicationStatus.Paused">paused</see> or
        /// <see cref="ApplicationStatus.Stopped">stopped</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationInitializationException">The Application could not be initialized on the remote Manager</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public void Start() 
        {
            VerifyDisposed();

            if (status != ApplicationStatus.UnInitialized)
                throw new InvalidOperationException("The application is already started, paused or stopped.");

            //this method is not meant to be thread-safe, but we should still lock the state when starting it
            //ideally there will be no other threads that will be active at this point, i.e no contention
            //so there shouldn't be much of a penalty here
            lock (applicationStatusLock)
            {
                ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(this.name, this.GetDependencies(), this.qos);

                id = manager.SubmitApplication(appInfo);

                if (!string.IsNullOrEmpty(id))
                    status = ApplicationStatus.Submitted;
                else
                    throw new ApplicationInitializationException("Could not initialize application.");
            }

            logger.Debug("Application Successfully Registered {0}", id);

            //fire off a worker thread to do the dependency stuff for the app
            //this may take a bit long - since we may need to process file transfers - so not using the ThreadPool
            if (this.dependencies.Count > 0)
            {
                StartDependencyResolver();
            }

            StartJobMonitor();
            StartJobSubmitter();
        }

        /// <summary>
        /// Pauses the Application. Adding more Jobs to a paused Application will result in the Jobs being suspended on the Manager 
        /// for later execution, when the Application is resumed.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">
        /// The Application is <see cref="ApplicationStatus.Paused">paused</see>,
        /// <see cref="ApplicationStatus.Stopped">stopped</see>
        /// or not yet initialized.
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">Application could not be found</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public void Pause()
        {
            VerifyDisposed();

            ApplicationStatus s = Status;
            if (s == ApplicationStatus.UnInitialized)
                throw new InvalidOperationException("Cannot pause an application that is not yet initialized.");
            if (s == ApplicationStatus.Paused)
                throw new InvalidOperationException("Cannot pause an application that is already paused.");
            if (s == ApplicationStatus.Stopped)
                throw new InvalidOperationException("Cannot pause an application that is already stopped.");

            manager.PauseApplication(this.Id);

            Status = ApplicationStatus.Paused;
        }

        /// <summary>
        /// Resumes the Application. 
        /// This will allow waiting Jobs in the Application to be eligible for scheduling.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">
        /// The Application is not <see cref="ApplicationStatus.Paused">paused</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">Application could not be found</exception>
        /// <exception cref="System.UnauthorizedAccessException">User is not Authorized to perform this action</exception>
        /// <exception cref="System.Security.Authentication.AuthenticationException">User could not be Authenticated</exception>
        /// <exception cref="Utilify.Platform.CommunicationException">communication error with the Manager</exception>
        /// <exception cref="Utilify.Platform.InternalServerException">unhandled server exception</exception>
        public void Resume()
        {
            VerifyDisposed();

            if (Status != ApplicationStatus.Paused)
                throw new InvalidOperationException("Cannot resume an application that is not paused.");

            manager.ResumeApplication(this.Id);

            //we need to call this here, since we don't know which state the app will be in next.
            //it may be waiting for dependencies, or ready
            ApplicationStatusInfo statusInfo = manager.GetApplicationStatus(this.Id);
            Status = statusInfo.Status;
        }
        #endregion

        #region IDisposable Members
        private bool disposed = false;
        
        /*
         * This class has an IDisposable field (the proxy), but doesn't use unmanaged resources directly.
         * So, we implement the simple version of IDisposable - the one without a Finalizer. 
         * We don't need to call GC.SuppressFinalize() since we are not implementing a Finalizer.
         */
        /// <summary>
        /// See <see cref="System.IDisposable.Dispose">System.IDisposable.Dispose</see>
        /// </summary>
        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                //clean up managed stuff
                IDisposable disposable = manager as IDisposable;
                if (disposable != null)
                    disposable.Dispose();

                disposable = null;
                manager = null;

                StopAllThreads();
            }

            //we have no base class to call dispose on, so nothing more to do here.

            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
        #endregion
    }

    /// <summary>
    /// Provides data for the <see cref="Application.JobStatusChanged"/> event.
    /// </summary>
    public class StatusChangedEventArgs : EventArgs
    {
        private JobStatusInfo status;
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusChangedEventArgs"/> class.
        /// </summary>
        public StatusChangedEventArgs()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusChangedEventArgs"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        public StatusChangedEventArgs(JobStatusInfo status)
            : base()
        {
            this.status = status;
        }

        /// <summary>
        /// Gets or sets the job status information.
        /// </summary>
        /// <value>The status.</value>
        public JobStatusInfo Status
        {
            get { return status; }
            set { status = value; }
        }
    }

    /// <summary>
    /// Provides data for the <see cref="Application.JobSubmitted"/> event.
    /// </summary>
    public class JobSubmittedEventArgs : EventArgs
    {
        private Job job;
        /// <summary>
        /// Initializes a new instance of the <see cref="JobSubmittedEventArgs"/> class.
        /// </summary>
        public JobSubmittedEventArgs()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobSubmittedEventArgs"/> class.
        /// </summary>
        /// <param name="job">The job.</param>
        public JobSubmittedEventArgs(Job job)
            : base()
        {
            this.job = job;
        }

        /// <summary>
        /// Gets or sets the job.
        /// </summary>
        /// <value>The job.</value>
        public Job Job
        {
            get { return job; }
            set { job = value; }
        }
    }

    /// <summary>
    /// Provides data for the <see cref="Application.JobCompleted"/> event.
    /// </summary>
    public class JobCompletedEventArgs : EventArgs
    {
        private Job job;
        /// <summary>
        /// Initializes a new instance of the <see cref="JobCompletedEventArgs"/> class.
        /// </summary>
        public JobCompletedEventArgs()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="job">The job.</param>
        public JobCompletedEventArgs(Job job)
            : base()
        {
            this.job = job;
        }

        /// <summary>
        /// Gets or sets the job.
        /// </summary>
        /// <value>The job.</value>
        public Job Job
        {
            get { return job; }
            set { job = value; }
        }
    }
}
