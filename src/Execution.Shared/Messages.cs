using System;
using System.Runtime.Serialization;
using System.IO;
using System.Text;

namespace Utilify.Platform.Execution.Shared
{
    public interface IMessageData
    {
        MessageType MessageType { get; }
        bool IsValid();
    }

    [Serializable]
    public class EmptyMessageData : IMessageData
    {
        private EmptyMessageData() { } //required for serialization

        public EmptyMessageData(MessageType messageType)
        {
            this.messageType = messageType;
        }

        #region IMessageData Members
        private MessageType messageType;

        public MessageType MessageType
        {
            get
            {
                return messageType;
            }
        }

        bool IMessageData.IsValid()
        {
            return (messageType != MessageType.Unknown);
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (object.ReferenceEquals(obj, this))
                return true;

            EmptyMessageData other = obj as EmptyMessageData;
            if (other == null)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ExecuteJobMessageData : IMessageData
    {
        private string jobId;
        private string applicationId;
        private string jobDirectory;
        private string applicationDirectory;
        private JobType jobType;
        private byte[] initialJobInstance;

        private ExecuteJobMessageData() { } //reqd. for serialization

        public ExecuteJobMessageData(string jobId, string applicationId,
            string jobDirectory, string applicationDirectory, JobType jobType,
            byte[] initialJobInstance)
        {
            if (string.IsNullOrEmpty(jobId))
                throw new ArgumentException("jobId");

            if (string.IsNullOrEmpty(applicationId))
                throw new ArgumentException("applicationId");

            if (string.IsNullOrEmpty(jobDirectory))
                throw new ArgumentException("jobDirectory");

            if (string.IsNullOrEmpty(applicationDirectory))
                throw new ArgumentException("applicationDirectory");

            if (initialJobInstance == null || initialJobInstance.Length == 0)
                throw new ArgumentException("initialJobInstance");
            
            this.jobId = jobId;
            this.applicationId = applicationId;
            this.jobDirectory = jobDirectory;
            this.applicationDirectory = applicationDirectory;
            this.jobType = jobType;
            this.initialJobInstance = initialJobInstance;
        }
        
        public string JobId
        {
            get { return jobId; }
        }
        
        public string ApplicationId
        {
            get { return applicationId; }
        }
        
        public string JobDirectory
        {
            get { return jobDirectory; }
        }
        
        public string ApplicationDirectory
        {
            get { return applicationDirectory; }
        }
        
        public JobType JobType
        {
            get { return jobType; }
        }
        
        public byte[] InitialJobInstance
        {
            get { return initialJobInstance; }
        }

        #region IMessageData Members
        //to allow proper serialization to Json
        
        public MessageType MessageType
        {
            get
            {
                return MessageType.ExecuteJob;
            }
        }

        bool IMessageData.IsValid()
        {
            bool valid = false;
            valid = !(string.IsNullOrEmpty(JobId) ||
                string.IsNullOrEmpty(ApplicationId) ||
                string.IsNullOrEmpty(JobDirectory) ||
                string.IsNullOrEmpty(ApplicationDirectory) ||
                (InitialJobInstance == null));
            return valid;
        }

        #endregion

        public override String ToString()
        {
            StringBuilder strng = new StringBuilder();
            strng.AppendLine(this.JobId);
            strng.AppendLine(this.ApplicationDirectory);
            strng.AppendLine(this.JobDirectory);
            strng.AppendLine(this.ApplicationDirectory);
            strng.AppendLine(this.JobType.ToString());
            return strng.ToString();
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            ExecuteJobMessageData emd = other as ExecuteJobMessageData;
            if (emd == null)
                return false;

            return (emd.JobId == this.JobId) && 
                (emd.ApplicationId == this.ApplicationId) && 
                (emd.JobDirectory == this.JobDirectory) && 
                (emd.ApplicationDirectory == this.ApplicationDirectory) && 
                (emd.JobType == this.JobType) && 
                (Helper.AreEqual(emd.InitialJobInstance, this.InitialJobInstance));
        }

        public override int GetHashCode()
        {
            return this.jobId.GetHashCode() ^ this.applicationId.GetHashCode();
        }
    }

    [Serializable]
    public class AbortJobMessageData : IMessageData
    {
        private string jobId;

        private AbortJobMessageData() { } //reqd. for serialization

        public AbortJobMessageData(string jobId)
        {
            this.jobId = jobId;
        }
        
        public string JobId
        {
            get { return jobId; }
        }

        #region IMessageData Members
        //to allow proper serialization to Json
        
        public MessageType MessageType
        {
            get
            {
                return MessageType.AbortJob;
            }
        }

        bool IMessageData.IsValid()
        {
            bool valid = !string.IsNullOrEmpty(jobId);
            return valid;
        }

        #endregion
    }

    [Serializable]
    public class JobCompletionMessageData : IMessageData
    {
        private JobCompletionInfo[] infos;

        private JobCompletionMessageData() { } //reqd. for serialization

        public JobCompletionMessageData(JobCompletionInfo[] infos)
        {
            if (infos == null)
                throw new ArgumentNullException("infos");
            this.infos = infos;
        }

        #region IMessageData Members
        //to allow proper serialization to Json
        
        public MessageType MessageType
        {
            get
            {
                return MessageType.CompletedJobs;
            }
        }

        bool IMessageData.IsValid()
        {
            bool valid = true; //todoNow: validate the job msg data
            return valid;
        }

        #endregion

        public JobCompletionInfo[] GetJobs()
        {
            return infos;
        }
        
        public JobCompletionInfo[] Infos
        {
            get
            {
                return infos;
            }
            set
            {
                infos = value;
            }
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            JobCompletionMessageData jmd = other as JobCompletionMessageData;
            if (jmd == null)
                return false;

            bool areEqual = false;
            JobCompletionInfo[] otherInfos = jmd.GetJobs();
            if (otherInfos != null && this.infos != null)
            {
                if (infos.Length == otherInfos.Length)
                {
                    bool tempEquals = true;
                    for (int i = 0; i < infos.Length; i++)
                    {
                        tempEquals = tempEquals && infos[i].Equals(otherInfos[i]);

                        if (!tempEquals)
                            break;
                    }
                    areEqual = tempEquals;
                }
            }

            return areEqual;
        }

        public override int GetHashCode()
        {
            //implemented to make FxCop happy. we don't really use this in a hash table.
            int hash = 0;
            hash = ToString().GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("# jobs: {0}", this.infos.Length).AppendLine();
            foreach (JobCompletionInfo info in infos)
            {
                sb.AppendFormat(info.ToString()).AppendLine();
            }
            return sb.ToString();
        }

    }
}
