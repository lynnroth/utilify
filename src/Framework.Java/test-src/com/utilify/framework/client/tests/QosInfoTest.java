package com.utilify.framework.client.tests;

import java.util.Date;

import org.junit.Test;

import com.utilify.framework.PriorityLevel;
import com.utilify.framework.client.QosInfo;

public class QosInfoTest {

	/*
	 * [Category("QosInfo")]
	 */
	
	@Test (expected = IllegalArgumentException.class)
	public void createQosInfoNegativeBudget() {
		long oneHourLater = (new Date()).getTime() + 60;		
		new QosInfo(-1, new Date(oneHourLater), new Date(), new Date(), PriorityLevel.NORMAL);
	}

	@Test (expected = IllegalArgumentException.class)
	public void createQosInfoNegativeDeadline() {
		long oneHourAgo = (new Date()).getTime() - 60;		
		new QosInfo(-1, new Date(oneHourAgo), new Date(), new Date(), PriorityLevel.NORMAL);
	}

	@Test
	public void createQosInfoDefault() {
		QosInfo.getDefault();
		//no assertions, Java dates are already Utc 
	}

	@Test
	public void QosInfoCtor() {
		long oneHourLater = (new Date()).getTime() + 60;
		long oneDayLater = (new Date()).getTime() + (60 * 24);
		// correct instantiation
		new QosInfo(1, new Date(oneDayLater), new Date(), new Date(oneHourLater), PriorityLevel.NORMAL);
		//no assertions, Java dates are already Utc 
	}
}