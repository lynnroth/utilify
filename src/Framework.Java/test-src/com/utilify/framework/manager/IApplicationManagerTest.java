package com.utilify.framework.manager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.sun.xml.ws.Closeable;
import com.utilify.framework.ApplicationProperties;
import com.utilify.framework.ApplicationStatus;
import com.utilify.framework.DependencyScope;
import com.utilify.framework.DependencyType;
import com.utilify.framework.EntityNotFoundException;
import com.utilify.framework.InvalidArgumentException;
import com.utilify.framework.InvalidOperationException;
import com.utilify.framework.JobStatus;
import com.utilify.framework.JobType;
import com.utilify.framework.ResultRetrievalMode;
import com.utilify.framework.client.ApplicationStatusInfo;
import com.utilify.framework.client.ApplicationSubmissionInfo;
import com.utilify.framework.client.ArrayOfJobSubmissionInfo;
import com.utilify.framework.client.DependencyContent;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.DependencyQueryInfo;
import com.utilify.framework.client.JobStatusInfo;
import com.utilify.framework.client.JobSubmissionInfo;
import com.utilify.framework.client.ResultContent;
import com.utilify.framework.client.ResultInfo;
import com.utilify.framework.client.ResultStatusInfo;
import com.utilify.framework.client.tests.TestJob;
import com.utilify.framework.tests.TestHelper;

public class IApplicationManagerTest {

	private IApplicationManager manager;

	@Before
	public void setUp() {
        ApplicationProperties props = new ApplicationProperties();
        ApplicationManager proxy = new ApplicationManager(props.getManagerUrl());
		manager = proxy.getApplicationManagerDefaultEndPoint();
	}

	@After
	public void tearDown() {
		manager = null;
		if (manager != null){
			Closeable closeable = (Closeable)manager;
			if (closeable != null)
				closeable.close();
			manager = null;
		}
	}

	/*
	 * Expected behaviour: - Should throw if we get invalid data : such as null,
	 * unfilled/meaningless data
	 */
	// [Category("SubmitApplication")]
	@Test(expected = InvalidArgumentException.class)
	public void submitApplicationNullValidation() {
		manager.submitApplication(null);
	}

	// #1. we know that this service will be used by non.NET clients.
	// and we know that the ApplicationSubmissionInfo has its own set of tests
	// for checking invalid data.
	// but if the data is still somehow malformed, we need to test if the
	// SubmitApp holds up
	// so should we try here, to set invalid values to the appInfo object thru'
	// reflection, and see if it works
	// todoLater: do we need to try this sort of testing because of #1 above?

	@Test
	public void submitApplication() {
		ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				"myApp1", "me", null);
		String id = manager.submitApplication(appInfo);

		Assert.assertTrue(
				"Id of submitted application should not be null / empty",
				(id != null && id.trim().length() != 0));
		// we'll test the app status in the GetAppStatus tests
	}

	/*
	 * - Should send dependency status if valid app id is given - else throw an
	 * exception - If a dependency is already present, then send a status for
	 * that dependency. (including the details), else send back an empty list :
	 * test this for both app and job - After SendDependency,
	 * GetUnresolvedDependencies should return an empty list - (since the
	 * dependencies would be sent).
	 */

	// [Category("GetUnresolvedDependencies - Common")]
	@Test(expected = IllegalArgumentException.class)
	public void getUnresolvedDependenciesWithNullQuery() {
		manager.getUnresolvedDependencies(null);
	}

	// [Category("GetUnresolvedDependencies - Application")]
	@Test (expected = EntityNotFoundException.class)
	public void getUnresolvedDependenciesWithNonExistentAppId() {
		// use some random guid which (hopefully) won't be in the db
		String id = UUID.randomUUID().toString();
		manager.getUnresolvedDependencies(new DependencyQueryInfo(id, DependencyScope.APPLICATION));
	}

	 //[Category("GetUnresolvedDependencies - Application")]

	@Test
	public void getUnresolvedDependenciesForAppWithNoDependencies() {
		ArrayList<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
		DependencyInfo[] deps = dependencies.toArray(new DependencyInfo[0]);

		ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("myApp",
				"me", deps);

		String appId = manager.submitApplication(app);
		DependencyInfo[] status = manager.getUnresolvedDependencies(new DependencyQueryInfo(appId,
				DependencyScope.APPLICATION)).toArray();
		Assert.assertNotNull("A non-null dependency status info array should be returned", status);
		Assert.assertEquals("An empty dependency status info array should be returned for an app with no dependencies",
				0, status.length);
	}
	
	//[Category("GetUnresolvedDependencies - Application")]
	@Test	
	public void getUnresolvedDependenciesForAppWithUrlDeps() throws IOException, MalformedURLException {
		List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
		for (int i = 0; i < 5; i++) {
			DependencyInfo dep = new DependencyInfo("d" + i,
					"http://myhost/myurl" + i, DependencyType.REMOTE_URL);
			dependencies.add(dep);
		}
		
		DependencyInfo[] deps = dependencies.toArray(new DependencyInfo[0]);
		
		ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("myApp", "me", deps);
	
		String appId = manager.submitApplication(app);
		DependencyInfo[] status =
			manager.getUnresolvedDependencies(new DependencyQueryInfo(appId,
					DependencyScope.APPLICATION)).toArray();
		
		Assert.assertNotNull("A non-null dependency status info array should be returned", status);
		
		Assert.assertEquals("An empty dependency status info array should be returned for an url dependencies of an app",
				0, status.length);
	}
	
	//[Category("GetUnresolvedDependencies - Application")]
	@Test
	public void getUnresolvedDependenciesForAppWithSomeFileDeps() throws IOException, MalformedURLException {
		DependencyInfo dep1 = DependencyInfo.fromClass(this.getClass());
		// just some random dependency - doesnt matter what - for this test :
		// (as long as it is not a standard assembly that we know about)
		DependencyInfo dep2 = new DependencyInfo("randomFile", TestHelper
				.getRandomTempFile(), DependencyType.DATA_FILE);
		DependencyInfo[] deps = new DependencyInfo[] { dep1, dep2 };

		ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("myApp",
				"me", deps);

		String appId = manager.submitApplication(app);
		DependencyInfo[] status = manager.getUnresolvedDependencies(
				new DependencyQueryInfo(appId, DependencyScope.APPLICATION))
				.toArray();
		Assert.assertNotNull(
				"A non-null dependency status info array should be returned",
				status);
		Assert
		.assertEquals(
				"A non-empty dependency status info array should be returned for an url dependencies of an app",
				2, status.length);
	}


	 //[Category("GetUnresolvedDependencies - Job")]

	 @Test (expected = EntityNotFoundException.class)
	 //[ExpectedException(typeof(JobNotFoundException))]
	 public void getUnresolvedDependenciesWithNonExistentJobId() {
		 //use some random guid which (hopefully) won't be in the db
		 manager.getUnresolvedDependencies(new DependencyQueryInfo(UUID.randomUUID().toString(), DependencyScope.JOB));
	 }
	 
	 //[Category("GetUnresolvedDependencies - Job")]
	 
	 @Test
	 public void getUnresolvedDependenciesForJobWithNoDependencies() {
		 List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		 
		 // submit app
		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("myApp", "me"); // no deps for app either

		 String appId = manager.submitApplication(app);

		 // submit job
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()),
				 deps);
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();

		 Assert.assertNotNull("Since we submitted valid jobs, the ids array returned should not be null",
				 jobIds);
		 Assert.assertEquals("Since we submitted ONE job, we should get ONE id back",
				 1, jobIds.size());

		 // the REAL test
		 DependencyInfo[] status = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(jobIds.get(0), DependencyScope.JOB)).toArray();
		 
		 Assert.assertNotNull("A non-null dependency status info array should be returned",
				 status);
		 Assert.assertEquals("An empty dependency status info array should be returned for an job with no dependencies",
				 0, status.length);
	 }

	 //[Category("GetUnresolvedDependencies - Job")]
	  
	 @Test
	 public void getUnresolvedDependenciesForJobWithUrlDeps() throws IOException, MalformedURLException {
		 List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		 for (int i = 0; i < 5; i++) {
			 DependencyInfo dep = new DependencyInfo("d" + i, "http://myhost/myurl" + i, DependencyType.REMOTE_URL);
			 deps.add(dep);
		 }
	
		 ApplicationSubmissionInfo app =
		 new ApplicationSubmissionInfo("myApp", "me"); //no deps for app
		 //submit app
		 String appId = manager.submitApplication(app);
		
		 //submit job
		 JobSubmissionInfo job = new JobSubmissionInfo(appId, JobType.CODE_MODULE,
		 TestHelper.getJobInstance(new TestJob()), deps);
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job })).getStrings();
		
		 Assert.assertNotNull("Since we submitted valid jobs, the ids array returned should not be null",
				 jobIds);
		 Assert.assertEquals("Since we submitted ONE job, we should get ONE id back",
				 1, jobIds.size());
		
		 //the REAL test
		 DependencyInfo[] status = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(jobIds.get(0), DependencyScope.JOB)).toArray();
		 Assert.assertNotNull("A non-null dependency status info array should be returned",
				 status);
		 Assert.assertEquals("An empty dependency status info array should be returned for an url dependencies of an app",
				 0, status.length);
	 }
	
	 //[Category("GetUnresolvedDependencies - Job")]
	 @Test
	 public void getUnresolvedDependenciesForJobWithSomeFileDeps() throws IOException, MalformedURLException {
		 DependencyInfo dep1 = DependencyInfo.fromClass(this.getClass());
		 // just some random dependency - doesnt matter what - for this test :
		 // (as long as it is not a standard assembly that we know about)
		 DependencyInfo dep2 = new DependencyInfo("randomFile", TestHelper.getRandomTempFile(),
				 DependencyType.DATA_FILE);
		 ArrayList<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		 deps.add(dep1);
		 deps.add(dep2);

		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("myApp", "me"); // no deps for app

		 String appId = manager.submitApplication(app);
		 // submit job
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()), deps);
		 
		 List<String> jobIds = manager.submitJobs(new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();

		 Assert.assertNotNull("Since we submitted valid jobs, the ids array returned should not be null",
				 jobIds);
		 Assert.assertEquals("Since we submitted ONE job, we should get ONE id back",
				 1, jobIds.size());

		 // the REAL test
		 DependencyInfo[] status = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(jobIds.get(0), DependencyScope.JOB))
				 .toArray();
		 Assert.assertNotNull("A non-null dependency status info array should be returned",
				 status);
		 Assert.assertEquals("A non-empty dependency status info array should be returned for an url dependencies of an app",
				 2, status.length);
	 }
	

	 // /*
	 // * Expected behaviour:
	 // * - Should store the dependencies if the given ids match.
	 // * - Should throw if invalid data is given (such as null etc)
	 // * - Should throw if app/job with given ids have already started/finished
	 // * - Once this method is called for an app/job, the
	 // GetUnresolvedDependenciesInfo can be checked to retrieve updated status
	 // * of dependencies.
	 // */
	 //[Category("SendDependency")] 

	 @Test(expected = IllegalArgumentException.class)
	 public void sendDependencyWithNullInput() {
		 manager.sendDependency(null);
	 }
	
	 @Test(expected = EntityNotFoundException.class)
	 public void sendDependencyInvalidDependencyId() {
		 // we need to go through the whole submission process,
		 // since we need to get a proper application id
		 
		 // we send an empty dependency list
		 DependencyInfo[] dependencies = new DependencyInfo[0];
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me", dependencies);
		 String id = manager.submitApplication(appInfo);

		 // this random guid id dependency would not exist!
		 DependencyContent content = new DependencyContent(UUID.randomUUID().toString(), TestHelper.getJobInstance(new TestJob()));
		 manager.sendDependency(content);
	 }
	 
	 @Test(expected = IllegalArgumentException.class)
	 public void sendDependencyInvalidFormatDependencyId() {
		 // we need to go through the whole submission process,
		 // since we need to get a proper application id
		 DependencyInfo[] dependencies = new DependencyInfo[0]; // we send an empty dependency list
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me", dependencies);
		 String id = manager.submitApplication(appInfo);

		 // this badly formatted guid id for the dependency should throw the exception
		 DependencyContent content = new DependencyContent(
				 "someBadlyFormatterDepId",
				 TestHelper.getJobInstance(new TestJob()));
		 manager.sendDependency(content);
	 }
	 
	 @Test
	 public void sendDependencyForJob() throws IOException, MalformedURLException {
		 // we need to go through the whole submission process,
		 // since we need to get a proper dependency id
		 // so that the check for its validity passes.
		 // This is to make sure only ONE argument in each test is invalid, and
		 // that produces the expected exception

		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me", null);
		 String appId = manager.submitApplication(appInfo);

		 // dependencies that will be added to the job
		 List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
		 DependencyInfo dep = new DependencyInfo("aDep", TestHelper
				 .getRandomTempFile(), DependencyType.DATA_FILE);
		 dependencies.add(dep);

		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()),
				 dependencies);

		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();
		 // just to be sure : check
		 Assert.assertNotNull("Ids array returned from submit jobs cannot be null", jobIds);

		 // now get the dependency status: to get the dependency Id
		 DependencyInfo[] depStatus = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(jobIds.get(0), DependencyScope.JOB))
				 .toArray();

		 // just to be sure
		 Assert.assertNotNull("Since we aded a dependency, the status should not be null",
				 depStatus);
		 Assert.assertEquals("Since we aded ONE dependency, the status should have exactly ONE element",
				 1, depStatus.length);
		 String depId = depStatus[0].getId();

		 // the REAL tests:
		 DependencyContent content = new DependencyContent(depId,
				 TestHelper.getJobInstance(new TestJob()));

		 manager.sendDependency(content);

		 // now test if GetDeps gives a zero length array
		 DependencyInfo[] newStatuses = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(jobIds.get(0), DependencyScope.JOB))
				 .toArray();
		 Assert.assertNotNull("The dep status should not be null", newStatuses);
		 Assert.assertEquals("Since we sent all the dependencies, the status should have ZERO elements",
				 0, newStatuses.length);
	 }
	
	 @Test
	 public void sendDependencyForApp() throws IOException, MalformedURLException {
		 // we need to go through the whole submission process,
		 // since we need to get a proper dependency id
		 // so that the check for its validity passes.
		 // This is to make sure only ONE argument in each test is invalid, and
		 // that produces the expected exception

		 List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
		 DependencyInfo dep = new DependencyInfo("aDep", TestHelper
				 .getRandomTempFile(), DependencyType.DATA_FILE);
		 dependencies.add(dep);

		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me", dependencies.toArray(new DependencyInfo[0]));
		 String appId = manager.submitApplication(appInfo);

		 // now get the dependency status: to get the dependency Id
		 DependencyInfo[] depStatus = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(appId.toString(),
						 DependencyScope.APPLICATION)).toArray();

		 // just to be sure
		 Assert.assertNotNull("Since we aded a dependency, the status should not be null",
				 depStatus);
		 Assert.assertEquals("Since we aded ONE dependency, the status should have exactly ONE element",
				 1, depStatus.length);
		 String depId = depStatus[0].getId();

		 // the REAL tests:
		 DependencyContent content = new DependencyContent(depId, TestHelper.getJobInstance(new TestJob()));

		 manager.sendDependency(content);

		 // now test if GetDeps gives a zero length array
		 DependencyInfo[] newStatuses = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(appId, DependencyScope.APPLICATION))
				 .toArray();
		 Assert.assertNotNull("The dep status should not be null", newStatuses);
		 Assert.assertEquals("Since we sent all the dependencies, the status should have ZERO elements",
				 0, newStatuses.length);
	 }
	 
	 @Test(expected = InvalidOperationException.class)
	 public void sendDependencyTwice() throws IOException, MalformedURLException {
		 List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
		 DependencyInfo dep = new DependencyInfo("aDep", TestHelper
				 .getRandomTempFile(), DependencyType.DATA_FILE);
		 dependencies.add(dep);

		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me", dependencies.toArray(new DependencyInfo[0]));
		 String appId = manager.submitApplication(appInfo);

		 // now get the dependency status: to get the dependency Id
		 DependencyInfo[] depStatus = manager.getUnresolvedDependencies(
				 new DependencyQueryInfo(appId.toString(),
						 DependencyScope.APPLICATION)).toArray();

		 // just to be sure
		 Assert.assertNotNull("Since we aded a dependency, the status should not be null",
				 depStatus);
		 Assert.assertEquals("Since we aded ONE dependency, the status should have exactly ONE element",
				 1, depStatus.length);
		 String depId = depStatus[0].getId();

		 DependencyContent content = new DependencyContent(depId, TestHelper.getJobInstance(new TestJob()));

		 manager.sendDependency(content);

		 // the REAL test:
		 // call it again. since the dependency has already been sent, we should
		 // get an illegal state exception
		 manager.sendDependency(content);
	 }

	 // [Category("SubmitJobs")]
	 @Test(expected = IllegalArgumentException.class)
	 public void submitJobsNullInput() {
		 manager.submitJobs(null);
	 }

	 @Test (expected = IllegalArgumentException.class)
	 public void submitJobsEmptyArray() {
		 manager.submitJobs(new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] {}));
	 }
	
	 @Test(expected = IllegalArgumentException.class)
	 public void submitJobsArrayWithNullElements() {
		 manager.submitJobs(new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { null, null }));
	 }
	
	 @Test(expected = EntityNotFoundException.class)
	 public void submitJobsWithInvalidApplication() {
		 // some random id - doesnt matter what the exact value is, all we need
		 // is that it shouldn't be in the submitted apps
		 String appId = UUID.randomUUID().toString();
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()));
		 manager.submitJobs(new ArrayOfJobSubmissionInfo(
				 new JobSubmissionInfo[] { job }));
	 }
	
	 @Test
	 public void submitJobsSingle() {
		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("aName",
		 "me");
		 String appId = manager.submitApplication(app);
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()));
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();

		 Assert.assertNotNull("Since we submitted valid jobs, the ids array returned should not be null",
				 jobIds);
		 Assert.assertEquals("The ids array returned should contain ONE job id",
				 1, jobIds.size());

		 Assert.assertTrue("The job id returned should not be null or empty",
				 (jobIds.get(0) != null && jobIds.get(0).trim().length() > 0));
		 // we'll test job status-es in the GetJobStatus tests
	 }
	
	 @Test
	 public void submitJobsMultiple() {
		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("aName",
		 "me");
		 String appId = manager.submitApplication(app);

		 int numJobs = 3;
		 List<JobSubmissionInfo> jobs = new ArrayList<JobSubmissionInfo>();
		 for (int i = 0; i < numJobs; i++) {
			 JobSubmissionInfo job = new JobSubmissionInfo(appId,
					 JobType.CODE_MODULE, TestHelper
					 .getJobInstance(new TestJob()));
			 jobs.add(job);
		 }

		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(jobs
						 .toArray(new JobSubmissionInfo[0]))).getStrings();
		 // the REAL tests:
		 Assert.assertNotNull("Since we submitted valid jobs, the ids array returned should not be null",
				 jobIds);
		 Assert.assertEquals("The ids array returned should contain " + numJobs + " job id(s)",
				 numJobs, jobIds.size());

		 for (int i = 0; i < numJobs; i++) {
			 Assert.assertTrue("The job id returned should not be null or empty. i = " + i,
					 (jobIds.get(i) != null && jobIds.get(i).trim().length() > 0));
		 }
		 // we'll test the statuses in the GetJobStatus tests
	 }

	 //[Category("GetJobStatus")]

	 @Test (expected = IllegalArgumentException.class)
	 public void getJobStatusNullIds() {
		 manager.getJobStatus(null);
	 }
	
	 @Test (expected = IllegalArgumentException.class)
	 public void getJobStatusEmptyListIds() {
		 ArrayOfstring strings = new ArrayOfstring();
		 manager.getJobStatus(strings);
	 }
	
	 @Test (expected = IllegalArgumentException.class)
	 public void getJobStatusEmptyElementsInList(){
		 ArrayOfstring strings = new ArrayOfstring();
		 strings.getStrings().add(null);
		 strings.getStrings().add(" ");
		 manager.getJobStatus(strings);
	 }
	
	 @Test (expected = EntityNotFoundException.class)
	 public void getJobStatusNonExistentJobIds(){
		 ArrayOfstring strings = new ArrayOfstring();
		 strings.getStrings().add(UUID.randomUUID().toString());
		 manager.getJobStatus(strings);
	 }
	
	 @Test
	 public void getJobStatusSingleNoDeps() {
		 // assuming other methods work as expected!
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "anApp", "me");
		 String appId = manager.submitApplication(appInfo);

		 JobSubmissionInfo jobInfo = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()));
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(
						 new JobSubmissionInfo[] { jobInfo })).getStrings();

		 ArrayOfstring strings = new ArrayOfstring();
		 strings.getStrings().addAll(jobIds);
		 // the REAL test:
		 JobStatusInfo[] statuses = manager.getJobStatus(strings).toArray();
		 Assert.assertNotNull("The returned statuses list should not be null",
				 statuses);
		 Assert.assertEquals("The returned statuses list should have exactly ONE element",
				 1, statuses.length);
		 Assert.assertEquals("The status of a newly submitted job with no dependencies should be SUBMITTED",
				 JobStatus.SUBMITTED, statuses[0].getStatus());
	 }

	 @Test
	 public void getJobStatusMultipleNoDeps() {
		 // assuming other methods work as expected!
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "anApp", "me");
		 String appId = manager.submitApplication(appInfo);

		 List<JobSubmissionInfo> jobInfos = new ArrayList<JobSubmissionInfo>();
		 int numJobs = 3;
		 for (int i = 0; i < numJobs; i++) {
			 JobSubmissionInfo jobInfo = new JobSubmissionInfo(appId,
					 JobType.CODE_MODULE, TestHelper
					 .getJobInstance(new TestJob()));
			 jobInfos.add(jobInfo);
		 }

		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(jobInfos
						 .toArray(new JobSubmissionInfo[0]))).getStrings();
		 ArrayOfstring strings = new ArrayOfstring();
		 strings.getStrings().addAll(jobIds);
		 // the REAL test:
		 JobStatusInfo[] statuses = manager.getJobStatus(strings).toArray();
		 Assert.assertNotNull("The returned statuses list should not be null",
				 statuses);
		 Assert.assertEquals("The returned statuses list should have exactly " + numJobs + " element(s)",
				 numJobs, statuses.length);
		 Assert.assertEquals("The status of a newly submitted job with no dependencies should be SUBMITTED",
				 JobStatus.SUBMITTED, statuses[0].getStatus());
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getApplicationStatusNull() {
		 manager.getApplicationStatus(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getApplicationStatusEmptyId() {
		 manager.getApplicationStatus(" ");
	 }

	 @Test(expected = EntityNotFoundException.class)
	 public void getApplicationStatusNonExistentId() {
		 manager.getApplicationStatus(UUID.randomUUID().toString());
	 }
	
	 @Test
	 public void getApplicationStatus() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");
		 String appId = manager.submitApplication(appInfo);
		 ApplicationStatusInfo status = manager.getApplicationStatus(appId);
		 Assert.assertNotNull("Application status should not be null", status);

		 boolean correctStatus = (status.getStatus() == ApplicationStatus.SUBMITTED || status
				 .getStatus() == ApplicationStatus.READY);

		 Assert.assertTrue("Application status for a submitted application should be 'submitted' or 'ready'",
				 correctStatus);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void pauseApplicationNullId() {
		 manager.pauseApplication(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void pauseApplicationEmptyId() {
		 manager.pauseApplication(" ");
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void pauseApplicationInvalidFormatId() {
		 manager.pauseApplication("someRandomId");
	 }

	 @Test(expected = EntityNotFoundException.class)
	 public void pauseApplicationNonExistentId() {
		 manager.pauseApplication(UUID.randomUUID().toString());
	 }

	 @Test(expected = InvalidOperationException.class)
	 public void pauseApplicationStopped() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");

		 String appId = manager.submitApplication(appInfo);
		 manager.abortApplication(appId); // should make the status : stopped
		 manager.pauseApplication(appId); // should throw exception
	 }
	
	 @Test(expected = InvalidOperationException.class)
	 public void pauseApplicationAlreadyPaused() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");

		 String appId = manager.submitApplication(appInfo);

		 manager.pauseApplication(appId); // should pause the app
		 manager.pauseApplication(appId); // should throw exception
	 }

	 @Test
	 public void pauseApplication() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");
		 String appId = manager.submitApplication(appInfo);
		 manager.pauseApplication(appId);

		 // the status should now be Paused
		 ApplicationStatusInfo status = manager.getApplicationStatus(appId);
		 Assert.assertNotNull("The status object should not be null", status);
		 Assert.assertEquals("The status should be 'paused'",
				 ApplicationStatus.PAUSED, status.getStatus());
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void resumeApplicationNullId() {
		 manager.resumeApplication(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void resumeApplicationEmptyId() {
		 manager.resumeApplication(" ");
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void resumeApplicationInvalidFormatId() {
		 manager.resumeApplication("someRandomId");
	 }
	
	 @Test(expected = EntityNotFoundException.class)
	 public void resumeApplicationNonExistentId() {
		 manager.resumeApplication(UUID.randomUUID().toString());
	 }

	 @Test(expected = InvalidOperationException.class)
	 public void resumeApplicationStopped() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");

		 String appId = manager.submitApplication(appInfo);

		 manager.abortApplication(appId); // should make the status : stopped
		 manager.resumeApplication(appId); // should throw exception
	 }
	
	 @Test(expected = InvalidOperationException.class)
	 public void resumeApplicationAlreadyReadyOrWaiting() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");

		 String appId = manager.submitApplication(appInfo);

		 manager.resumeApplication(appId); // should throw exception
	 }

	 @Test
	 public void resumeApplication() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");
		 String appId = manager.submitApplication(appInfo);

		 ApplicationStatusInfo oldStatus = manager.getApplicationStatus(appId);
		 Assert.assertNotNull("The status object should not be null", oldStatus);

		 manager.pauseApplication(appId); // assume pause works...

		 // now test resume
		 manager.resumeApplication(appId);
		 // the status should now be the same as the old status
		 ApplicationStatusInfo status = manager.getApplicationStatus(appId);
		 Assert.assertNotNull("The status object should not be null", status);
		 Assert.assertEquals("The status should be " + oldStatus.getStatus(),
				 oldStatus.getStatus(), status.getStatus());
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void abortApplicationNullId() {
		 manager.abortApplication(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void abortApplicationEmptyId() {
		 manager.abortApplication(" ");
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void abortApplicationInvalidFormatId() {
		 manager.abortApplication("someRandomId");
	 } 
	
	 @Test(expected = EntityNotFoundException.class)
	 public void abortApplicationNonExistentId() {
		 manager.abortApplication(UUID.randomUUID().toString());
	 }

	 @Test(expected = InvalidOperationException.class)
	 public void abortApplicationStopped() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");

		 String appId = manager.submitApplication(appInfo);

		 manager.abortApplication(appId);
		 manager.abortApplication(appId); // should throw an exception
	 }
	
	 @Test
	 public void abortApplication() {
		 ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(
				 "aName", "me");
		 String appId = manager.submitApplication(appInfo);

		 manager.abortApplication(appId); // should make the status : stopped

		 // the status should now be Paused
		 ApplicationStatusInfo status = manager.getApplicationStatus(appId);
		 Assert.assertNotNull("The status object should not be null", status);
		 Assert.assertEquals("The status should be 'stopped'",
				 ApplicationStatus.STOPPED, status.getStatus());
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getJobResultsNullJobId() {
		 manager.getJobResults(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getJobResultsEmptyJobId() {
		 manager.getJobResults(" ");
	 }

	 @Test(expected = EntityNotFoundException.class)
	 public void getJobResultsNonExistentJobId() {
		 manager.getJobResults(UUID.randomUUID().toString());
	 }
	 
	 @Test
	 public void getJobResults() {
		 // submit an app
		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("aName", "me");
		 String appId = manager.submitApplication(app);

		 // create some result infos
		 List<ResultInfo> results = new ArrayList<ResultInfo>();
		 for (int i = 0; i < 3; i++) {
			 ResultInfo res = new ResultInfo(TestHelper.getRandomTempFile(),
					 ResultRetrievalMode.BACK_TO_ORIGIN);
			 results.add(res);
		 }

		 // submit a job for the app with some results
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()),
				 new ArrayList<DependencyInfo>(), results);
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();

		 // assuming everything so far works correctly:

		 ResultStatusInfo[] resStatuses = manager.getJobResults(jobIds.get(0))
		 .toArray();
		 Assert.assertNotNull(
				 "Since we added results, the list should not ne null",
				 resStatuses);
		 Assert.assertEquals("We should get back as many results as we added",
				 results.size(), resStatuses.length);

	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getResultNullId() {
		 manager.getResult(null);
	 }

	 @Test(expected = IllegalArgumentException.class)
	 public void getResultEmptyId() {
		 manager.getResult(" ");
	 }

	 @Test(expected = EntityNotFoundException.class)
	 public void getResultNonExistentResult() {
		 manager.getResult(UUID.randomUUID().toString());
	 }

	 @Test
	 public void getResult() {
		 // submit an app
		 ApplicationSubmissionInfo app = new ApplicationSubmissionInfo("aName", "me");
		 String appId = manager.submitApplication(app);

		 // create a result info
		 List<ResultInfo> results = new ArrayList<ResultInfo>();
		 ResultInfo res = new ResultInfo(TestHelper.getRandomTempFile(),
				 ResultRetrievalMode.BACK_TO_ORIGIN);
		 results.add(res);

		 // submit a job for the app with a result
		 JobSubmissionInfo job = new JobSubmissionInfo(appId,
				 JobType.CODE_MODULE, TestHelper.getJobInstance(new TestJob()),
				 new ArrayList<DependencyInfo>(), results);
		 List<String> jobIds = manager.submitJobs(
				 new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { job }))
				 .getStrings();

		 ResultStatusInfo[] resStatuses = manager.getJobResults(jobIds.get(0)).toArray();

		 // assuming everything so far works correctly:
		 String resultId = resStatuses[0].getResultId();

		 ResultContent content = manager.getResult(resultId);

		 Assert.assertNotNull("Since we added results, the content should not be null",
				 content);
		 Assert.assertEquals("We should get back the content for the result we requested",
				 resultId, content.getResultId());
	 }
}
