
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ArrayOfJobSubmissionInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobInfo" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfJobSubmissionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "jobInfo"
})
@XmlRootElement(name = "SubmitJobs")
public class SubmitJobs {

    @XmlElement(nillable = true)
    protected ArrayOfJobSubmissionInfo jobInfo;

    /**
     * Gets the value of the jobInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfJobSubmissionInfo }
     *     
     */
    public ArrayOfJobSubmissionInfo getJobInfo() {
        return jobInfo;
    }

    /**
     * Sets the value of the jobInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfJobSubmissionInfo }
     *     
     */
    public void setJobInfo(ArrayOfJobSubmissionInfo value) {
        this.jobInfo = value;
    }

}
