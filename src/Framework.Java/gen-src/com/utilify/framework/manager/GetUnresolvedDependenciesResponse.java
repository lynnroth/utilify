
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ArrayOfDependencyInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUnresolvedDependenciesResult" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfDependencyStatusInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUnresolvedDependenciesResult"
})
@XmlRootElement(name = "GetUnresolvedDependenciesResponse")
public class GetUnresolvedDependenciesResponse {

    @XmlElement(name = "GetUnresolvedDependenciesResult", nillable = true)
    protected ArrayOfDependencyInfo getUnresolvedDependenciesResult;

    /**
     * Gets the value of the getUnresolvedDependenciesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDependencyInfo }
     *     
     */
    public ArrayOfDependencyInfo getGetUnresolvedDependenciesResult() {
        return getUnresolvedDependenciesResult;
    }

    /**
     * Sets the value of the getUnresolvedDependenciesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDependencyInfo }
     *     
     */
    public void setGetUnresolvedDependenciesResult(ArrayOfDependencyInfo value) {
        this.getUnresolvedDependenciesResult = value;
    }

}
