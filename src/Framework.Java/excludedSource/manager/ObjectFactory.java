
package com.utilify.schemas._2007._09.manager;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.utilify.framework.EntityNotFoundFault;
import com.utilify.framework.InvalidOperationFault;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.utilify.schemas._2007._09.manager package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EntityNotFoundFault_QNAME = new QName("http://schemas.utilify.com/2007/09/Manager", "EntityNotFoundFault");
    private final static QName _InvalidOperationFault_QNAME = new QName("http://schemas.utilify.com/2007/09/Manager", "InvalidOperationFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.utilify.schemas._2007._09.manager
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitApplicationResponse }
     * 
     */
    public SubmitApplicationResponse createSubmitApplicationResponse() {
        return new SubmitApplicationResponse();
    }

    /**
     * Create an instance of {@link ResumeApplication }
     * 
     */
    public ResumeApplication createResumeApplication() {
        return new ResumeApplication();
    }

    /**
     * Create an instance of {@link AbortApplication }
     * 
     */
    public AbortApplication createAbortApplication() {
        return new AbortApplication();
    }

    /**
     * Create an instance of {@link GetApplicationStatus }
     * 
     */
    public GetApplicationStatus createGetApplicationStatus() {
        return new GetApplicationStatus();
    }

    /**
     * Create an instance of {@link GetUnresolvedDependencies }
     * 
     */
    public GetUnresolvedDependencies createGetUnresolvedDependencies() {
        return new GetUnresolvedDependencies();
    }

    /**
     * Create an instance of {@link GetJobResultsResponse }
     * 
     */
    public GetJobResultsResponse createGetJobResultsResponse() {
        return new GetJobResultsResponse();
    }

    /**
     * Create an instance of {@link GetUnresolvedDependenciesResponse }
     * 
     */
    public GetUnresolvedDependenciesResponse createGetUnresolvedDependenciesResponse() {
        return new GetUnresolvedDependenciesResponse();
    }

    /**
     * Create an instance of {@link SubmitJobsResponse }
     * 
     */
    public SubmitJobsResponse createSubmitJobsResponse() {
        return new SubmitJobsResponse();
    }

    /**
     * Create an instance of {@link SendDependency }
     * 
     */
    public SendDependency createSendDependency() {
        return new SendDependency();
    }

    /**
     * Create an instance of {@link GetJobResults }
     * 
     */
    public GetJobResults createGetJobResults() {
        return new GetJobResults();
    }

    /**
     * Create an instance of {@link ResumeApplicationResponse }
     * 
     */
    public ResumeApplicationResponse createResumeApplicationResponse() {
        return new ResumeApplicationResponse();
    }

    /**
     * Create an instance of {@link GetJobStatusResponse }
     * 
     */
    public GetJobStatusResponse createGetJobStatusResponse() {
        return new GetJobStatusResponse();
    }

    /**
     * Create an instance of {@link AbortApplicationResponse }
     * 
     */
    public AbortApplicationResponse createAbortApplicationResponse() {
        return new AbortApplicationResponse();
    }

    /**
     * Create an instance of {@link GetResultResponse }
     * 
     */
    public GetResultResponse createGetResultResponse() {
        return new GetResultResponse();
    }

    /**
     * Create an instance of {@link GetJobStatus }
     * 
     */
    public GetJobStatus createGetJobStatus() {
        return new GetJobStatus();
    }

    /**
     * Create an instance of {@link SubmitJobs }
     * 
     */
    public SubmitJobs createSubmitJobs() {
        return new SubmitJobs();
    }

    /**
     * Create an instance of {@link SubmitApplication }
     * 
     */
    public SubmitApplication createSubmitApplication() {
        return new SubmitApplication();
    }

    /**
     * Create an instance of {@link GetApplicationStatusResponse }
     * 
     */
    public GetApplicationStatusResponse createGetApplicationStatusResponse() {
        return new GetApplicationStatusResponse();
    }

    /**
     * Create an instance of {@link GetResult }
     * 
     */
    public GetResult createGetResult() {
        return new GetResult();
    }

    /**
     * Create an instance of {@link SendDependencyResponse }
     * 
     */
    public SendDependencyResponse createSendDependencyResponse() {
        return new SendDependencyResponse();
    }

    /**
     * Create an instance of {@link PauseApplication }
     * 
     */
    public PauseApplication createPauseApplication() {
        return new PauseApplication();
    }

    /**
     * Create an instance of {@link PauseApplicationResponse }
     * 
     */
    public PauseApplicationResponse createPauseApplicationResponse() {
        return new PauseApplicationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntityNotFoundFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Manager", name = "EntityNotFoundFault")
    public JAXBElement<EntityNotFoundFault> createEntityNotFoundFault(EntityNotFoundFault value) {
        return new JAXBElement<EntityNotFoundFault>(_EntityNotFoundFault_QNAME, EntityNotFoundFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidOperationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Manager", name = "InvalidOperationFault")
    public JAXBElement<InvalidOperationFault> createInvalidOperationFault(InvalidOperationFault value) {
        return new JAXBElement<InvalidOperationFault>(_InvalidOperationFault_QNAME, InvalidOperationFault.class, null, value);
    }

}
