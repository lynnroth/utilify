
package com.utilify.framework;

import javax.xml.ws.WebFault;

/**
 * Thrown when a method has been invoked at an inappropriate time.
 */
@WebFault(name = InvalidOperationFault.FaultName, targetNamespace = Namespaces.Framework)
public class InvalidOperationException extends IllegalStateException {

	private static final long serialVersionUID = 3877674890658841898L;
	
	/*
     * Java type that goes as soapenv:Fault detail element. 
     */
    private InvalidOperationFault faultInfo;

    //needed for JAXWS to figure out if this is the wrapper class for a declared fault
    /**
     * Gets the SOAP fault that caused this exception
     * @return the associated {@link InvalidOperationFault}
     */
    public InvalidOperationFault getFaultInfo(){
    	return faultInfo;
    }
    
    private InvalidOperationException(){
    	super();
    }
    
    /**
     * Constructs an <code>InvalidOperationException</code> with the specified detail message and fault.
     * @param message - the detail message.
     * @param faultInfo - the fault that is the cause of this exception.
     */ //ctor needed for JAXWS to figure out if this is the wrapper class for a declared fault
    public InvalidOperationException(String message, InvalidOperationFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

}
