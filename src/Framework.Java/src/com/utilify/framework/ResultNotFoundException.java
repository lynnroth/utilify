
package com.utilify.framework;

/**
 * Thrown when a result with the specified Id is not found.
 */
public class ResultNotFoundException extends FrameworkException {
	
	private static final long serialVersionUID = -572242194853444505L;
	private String resultId = null;

	/**
     * Constructs a ResultNotFoundException with the specified detail message and result Id.
     * @param message - the detail message.
     * @param resultId - the Id of the resul that could not be found.
	 */
	public ResultNotFoundException(String message, String resultId) {
		super(String.format("%1$s. Result id : '%2$s'", message));
		this.resultId = resultId;
	}

	/**
	 * Constructs a ResultNotFoundException with no detail message.
	 */
	public ResultNotFoundException() {
		super();
	}

	/**
	 * Constructs a ResultNotFoundException with the specified detail message.
	 * @param message - the detail message.
	 */
	public ResultNotFoundException(String message) {
		super(message);
	}

	/**
	 * Constructs a ResultNotFoundException with the specified detail message
	 * and cause.
	 * @param message - the detail message.
	 * @param cause - the cause of this exception.
	 */
	public ResultNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Gets the result Id value that could not be found.
	 * @return the result Id value.
	 */
	public String getResultId() {
		return resultId;
	}
}