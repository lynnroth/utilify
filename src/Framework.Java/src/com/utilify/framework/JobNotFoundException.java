
package com.utilify.framework;

/**
 * Thrown when a job with the specified Id is not found.
 */
public class JobNotFoundException extends FrameworkException {

	private static final long serialVersionUID = 5379228171134101994L;
	private String jobId = null;

    /**
     * Constructs a JobNotFoundException with the specified detail message and job Id.
     * @param message - the detail message.
     * @param jobId - the Id of the job that could not be found.
     */
	public JobNotFoundException(String message, String jobId) {
		super(String.format("%1$s. Job id : '%2$s'", message, jobId));
		this.jobId = jobId;
	}

    /**
     * Constructs a JobNotFoundException with no detail message.
     */
	public JobNotFoundException() {
		super();
	}

    /**
     * Constructs a JobNotFoundException with the specified detail message.
     * @param message - the detail message.
     */
	public JobNotFoundException(String message) {
		super(message);
	}

    /**
     * Constructs a JobNotFoundException with the specified detail message
     * and cause.
     * @param message - the detail message.
     * @param cause - the cause of the exception.
     */
	public JobNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Gets the job id value that could not be found
     * @return the job Id
     */
	public String getJobId() {
		return jobId;
	}
}