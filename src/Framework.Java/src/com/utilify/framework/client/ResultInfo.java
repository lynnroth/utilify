
package com.utilify.framework.client;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;
import com.utilify.framework.ResultRetrievalMode;

/*
 * <p>Java class for ResultInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetrievalMode" type="{http://schemas.utilify.com/2007/09/Framework}ResultRetrievalMode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents information about a result. Results are files or resources produced by the execution of a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultInfo", propOrder = {
    "filename",
    "id",
    "retrievalMode"
})
public class ResultInfo implements Serializable {

    @XmlElement(name = "FileName", required = true, nillable = true)
    private String filename;
    @XmlElement(name = "Id", nillable = true)
    private String id;
    @XmlElement(name = "RetrievalMode", required = true)
    private ResultRetrievalMode retrievalMode;

    private ResultInfo(){}
    
    /**
     * Creates an instance of the <code>ResultInfo</code> class with the specified filename and mode of retrieval.
     * @param fileName
     * 	the name of the result file.
     * @param retrievalMode
     * 	the mode of retrieval of the result file.
     * @throws IllegalArgumentException
     * 	if <code>fileName</code> is <code>null</code> or empty, or <code>retrievalMode</code> is <code>null</code>.
     */
    public ResultInfo(String fileName, ResultRetrievalMode retrievalMode) {
    	setFilename(fileName);
    	setRetrievalMode(retrievalMode);
	}

	/**
     * Gets the filename of this result.
     * @return 
     *	the result file name.
     */
    public String getFilename() {
        return filename;
    }

    private void setFilename(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": filename");
        this.filename = value;
    }

    /*
     * Gets the Id of this result.
     * @return
     * 	the result Id.
     */
    @SuppressWarnings("unused") //used by JAXWS - on the server side
	private String getId() {
        return id;
    }

    @SuppressWarnings("unused") //used only on the server side
	private void setId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": id");
        this.id = value;
    }

    /**
     * Gets the the mode of retrieval of this result.
     * @return
     * 	the mode of retrieval.
     */
    public ResultRetrievalMode getRetrievalMode() {
        return retrievalMode;
    }

    private void setRetrievalMode(ResultRetrievalMode value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": retrievalMode");
        this.retrievalMode = value;
    }
}
