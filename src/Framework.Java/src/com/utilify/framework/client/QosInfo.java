
package com.utilify.framework.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;
import com.utilify.framework.Helper;
import com.utilify.framework.PriorityLevel;

/*
 * <p>Java class for QosInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QosInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Budget" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Deadline" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EarliestStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="LatestStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Priority" type="{http://schemas.utilify.com/2007/09/Framework}PriorityLevel"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the quality of service requirements provided to the Manager when an application is submitted.
 * The qos is always associated with an application.
 * The qos assigned to an application can be updated subsequently to allow more or less flexibility while scheduling jobs.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QosInfo", propOrder = {
    "budget",
    "deadline",
    "earliestStartTime",
    "latestStartTime",
    "priority"
})
public class QosInfo {

    @XmlElement(name = "Budget")
    private int budget;
    @XmlElement(name = "Deadline", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date deadline;
    @XmlElement(name = "EarliestStartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date earliestStartTime;
    @XmlElement(name = "LatestStartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date latestStartTime;
    @XmlElement(name = "Priority", required = true)
    private PriorityLevel priority;
    
    private QosInfo(){}
    
    /**
     * Creates an instance of the <code>QosInfo</code> class with the specified budget, deadline, start time restrictions and priority.
     * @param budget
     * 	the maximum allowed budget (which is an indirect measure of priority) to be spent during execution of jobs for the associated application.  
     * @param deadline
     * @param earliestStartTime
     * @param latestStartTime
     * @param priority
     * @throws IllegalArgumentException
     * 	if
     * <li><code>budget</code> is a negative number, or</li>
     * <li><code>deadline</code>, <code>earliestStartTime</code>, <code>latestStartTime</code> or <code>priority</code> is
     * <code>null</code>, or</li>
     * <li><code>deadline</code> is earlier than the current time.</li>
     */
    public QosInfo(int budget, Date deadline, Date earliestStartTime, Date latestStartTime, PriorityLevel priority) {
		setBudget(budget);
		setDeadline(deadline);
		setEarliestStartTime(earliestStartTime);
		setLatestStartTime(latestStartTime);
		setPriority(priority);
	}

	/**
     * Gets the total budget allowed for execution of jobs.
	 * @return
	 * 	the budget allowed.
     */
    public int getBudget() {
        return budget;
    }

    private void setBudget(int value) {
    	if (value < 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " zero: budget");
        this.budget = value;
    }

    /**
     * Gets the value of the deadline property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getDeadline() {
    	return this.deadline;
    }

    private void setDeadline(Date value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": deadline");
    	long now = new Date().getTime();
    	if (value.getTime() <  now)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " current time: deadline");

    	this.deadline = value;
    }

    /**
     * Gets the value of the earliestStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getEarliestStartTime() {
        return earliestStartTime;
    }

    private void setEarliestStartTime(Date value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": earliestStartTime");
    	this.earliestStartTime = value;
    }

    /**
     * Gets the value of the latestStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getLatestStartTime() {
    	return latestStartTime;
    }

    private void setLatestStartTime(Date value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": latestStartTime");
    	this.latestStartTime = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link PriorityLevel }
     *     
     */
    public PriorityLevel getPriority() {
        return priority;
    }

    private void setPriority(PriorityLevel value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": priority");
        this.priority = value;
    }

    /**
     * Gets the default qos requirements - which specify a zero budget,
     * a deadline of  31/Dec/9999 23:59:59 UTC, 
     * an earliest and latest start time which is the current time,
     * and priority of {@link PriorityLevel#NORMAL}.
     * @return
     * 	an instance of <code>QosInfo</code>.
     */
    public static QosInfo getDefault(){
    	return new QosInfo(0, Helper.getMaxUtcDate(), new Date(), Helper.getMaxUtcDate(), PriorityLevel.NORMAL);
    }
}
