package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.utilify.framework.JobStatus;

/**
 * A <code>Job</code> is the client-side representation of a job submitted for execution to the Manager.
 */
public final class Job {

	private String id;
	private Executable job;
	private JobStatus status;
	private Executable completedJob;
	
    private Date startTime;
    private Date finishTime;
    
    private ArrayList<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
    
    private Object completedJobLock = new Object();
	
	/**
	 * Creates an instance of the <code>Job</code> class with the specified executable job instance.
	 * @param job
	 * 	an instance of {@link Executable} that represents the code to execute remotely.
	 */
	public Job(Executable job) {
		this.job = job;
		this.id = "";
		this.status = JobStatus.UN_INITIALIZED;
	}

	//all getters and setters of this class are thread-safe and synchronized 
	//- since they are always used on different threads.
	/**
	 * Gets the orginially submitted instance of the executable job. 
	 * @return
	 * 	the submitted job instance.
	 */
	public Executable getInitialInstance(){
		Executable copy = null;
		synchronized (this.job) {
			copy = job;
		}
		return copy;
	}

	/**
	 * Gets the Id of the job.
	 * If the job does not yet have an Id, (possibly because it is still being submitted),
	 * it returns <code>null</code>.
	 * @return
	 * 	the Id of this job.
	 */
	public String getId() {
		String copy = null;
		synchronized (this.id) {
			copy = this.id;
		}
		return copy;
	}
	
	void setId(String value){
		synchronized (this.id) {
			this.id = value;
		}
	}

	/**
	 * Gets an instance of the completed job.
	 * If the job has not yet finished executing, it returns <code>null</code>.
	 * @return
	 * 	the instance of the completed job.
	 */
	public Executable getCompletedInstance() {
		Executable copy = null;
		synchronized (completedJobLock) {
			copy = completedJob;
		}
		return copy;
	}

	void setCompletedInstance(Executable completedJob) {
		synchronized (completedJobLock) {
			this.completedJob = completedJob;
		}
	}
	
	/**
	 * Gets the status of the job.
	 * If the job is in the process of submission, it returns <code>null</code>.
	 * @return
	 * 	the job status.
	 */
	public JobStatus getStatus() {
		JobStatus copy = null;
		synchronized (this.status) {
			copy =  status;
		}
		return copy;
	}

	private void setStatus(JobStatus status) {
		synchronized (this.status) {
			this.status = status;	
		}
	}
	
	private boolean isCancelRequested = false;
	/**
	 * @return isCancelRequested
	 */
	boolean isCancelRequested() {
		boolean isReq = false;
		synchronized (this) {
			isReq = isCancelRequested;
		}
		return isReq;
	}
	/**
	 * @param isCancelRequested
	 */
	void setCancelRequested(boolean isCancelRequested) {
		synchronized (this) {
			this.isCancelRequested = isCancelRequested;
		}
	}

	/**
	 * Blocks the current thread till this job completes remote execution.
	 * @throws InterruptedException
	 * 	if the thread is interrupted while it is blocked.
	 */
	public void waitForCompletion() throws InterruptedException {
		while (true){
			JobStatus s = getStatus();
			if (s == JobStatus.COMPLETED
					|| s == JobStatus.CANCELLED)
				break;
			Thread.sleep(1000);
		}
	}

	/**
	 * Gets the start time for the job
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}
	
	void setStartTime(Date value){
		startTime = value;
	}
	
	/**
	 * Gets the finish time for the job
	 * @return the finish time
	 */
	public Date getFinishTime(){
		return finishTime;
	}
	
	void setFinishTime(Date value){
		finishTime = value;
	}

	/**
	 * @return the dependencies
	 */
	public ArrayList<DependencyInfo> getDependencies() {
		return dependencies;
	}

	/**
	 * @param dependencies the dependencies to set
	 */
	void setDependencies(Collection<DependencyInfo> dependencies) {
		if (dependencies != null){
			this.dependencies.clear();
			this.dependencies.addAll(dependencies);
		}
	}
}
