package com.utilify.framework.client;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.JobStatus;
import com.utilify.framework.JobStatusEvent;
import com.utilify.framework.JobStatusListener;


public class ClientJobTable
{    
    // NOTE : Multiple lists are used here to improve performance and prevent having to iterate through multiple lists
    // Need to ensure that they are kept in sync.

    List<Job> newJobs = new ArrayList<Job>();

    // ONLY used for Jobs that should be monitored!
    // Dictionary mapping JobIds to Jobs 
    Map<String, Job> idToActiveJobs =
        new HashMap<String, Job>();

    // ONLY used for Jobs that are Complete or Cancelled!
    // Dictionary mapping JobIds to Jobs 
    Map<String, Job> idToInactiveJobs =
        new HashMap<String, Job>();

    Logger logger = Logger.getLogger(this.getClass().getName());

    private List<JobTableStatusListener> statusListeners = new ArrayList<JobTableStatusListener>();
    
    /// <summary>
    /// Creates an empty ClientJobTable.
    /// </summary>
    protected ClientJobTable() { }

    // Use reflection to change the Job's status
    private void changeJobStatus(Job job, JobStatus status)
    {
    	try {
    		Method m = Job.class.getDeclaredMethod("setStatus", new Class[] {JobStatus.class});
    		m.setAccessible(true);
    		m.invoke(job, status);
    		m.setAccessible(false);
    	} catch (Exception e) {
    		logger.severe("Failed to change the Job status using reflection. " + e.getMessage());
    	}
    }

    /// <summary>
    /// Adds a Job to the ClientJobTable.
    /// </summary>
    /// <remarks>
    /// The 'Add' methods are the only way a Job can be introduced to the ClientJobTable.
    /// </remarks>
    /// <param name="job"></param>
    protected void add(Job job)
    {
        synchronized(this)  {
            add(job, JobStatus.UN_INITIALIZED);
        }
    }

    private void add(Job job, JobStatus status)
    {
        if (job == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + " job");

        if (job.getId() != null && !job.getId().trim().equals(""))
            throw new IllegalArgumentException("Cannot Add Job. Job already has an Id, so it must already be submitted.");

        synchronized(this)
        {
            if (newJobs.contains(job))
                throw new IllegalArgumentException("Cannot Add Job. Job already exists.");
            if (job.getStatus() != JobStatus.UN_INITIALIZED)
                throw new IllegalArgumentException("Cannot Add Job with Status = " + job.getStatus());

            // Should be atomic
            newJobs.add(job);

            // Raise the Event.
            raiseJobTableStatusChanged(status);
        }
    }

    /// <summary>
    /// Changes the Status of a Job.
    /// </summary>
    /// <param name="job"></param>
    /// <param name="newStatus"></param>
    protected void changeStatus(Job job, JobStatus newStatus)
    {
        if (job == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + " job");

        logger.fine("Changing Job Status from " + job.getStatus() + " to " + newStatus);

        synchronized(this)
        {
            // Check for valid state transition.
            JobStatus oldStatus = job.getStatus();
            if (!isValidStatusChange(oldStatus, newStatus))
                throw new IllegalArgumentException(String.format("Cannot update Job Status. Job cannot transition from {0} to {1}.", oldStatus, newStatus));

            if (newStatus == JobStatus.SUBMITTED) {
            	
            	logger.fine("Moving job " + job.getId() + " to Active group");
                if (!newJobs.contains(job))
                    throw new IllegalArgumentException("Cannot update Job Status. Job does not exist.");
                // Add to list of active jobs.
                idToActiveJobs.put(job.getId(), job);
                // Remove from newJobs.
                newJobs.remove(job);
            } else if (newStatus == JobStatus.COMPLETED || newStatus == JobStatus.CANCELLED) {
            	
            	logger.fine("Moving job " + job.getId() + " to InActive group");
                if ((job.getId() == null || job.getId().trim().equals("")) || !idToActiveJobs.containsKey(job.getId()))
                    throw new IllegalArgumentException("Cannot update Job Status. Job does not exist.");
                // Add to Inactive list
                idToInactiveJobs.put(job.getId(), job);
                // Remove Job from list so its not monitored again.
                idToActiveJobs.remove(job.getId());
            }

            // Change protected Job status
            // Need to set Status inside the Job too (shouldnt change the status anywhere outside here!).
            // For now, lets make private and use reflection to prevent others from being tempted to change the status.
            // Note: No need to change status for Adding Job.
            changeJobStatus(job, newStatus);

            // Raise the Event.
            raiseJobTableStatusChanged(newStatus);
        }
    }

    /// <summary>
    /// Checks whether the given Status change is a valid one.
    /// </summary>
    /// <param name="oldStatus"></param>
    /// <param name="newStatus"></param>
    /// <returns></returns>
    private boolean isValidStatusChange(JobStatus oldStatus, JobStatus newStatus)
    {
        // If Job has been Cancelled don't allow any other state change.
        //if (oldStatus == JobStatus.Cancelled)
        //    return false;
        //else
            return true;
    }

    /// <summary>
    /// Returns all the Ids for Jobs based on StatusGroup
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    protected List<String> getJobIds(StatusGroup status)
    {
        synchronized(this)
        {
            List<String> jobs = new ArrayList<String>();
            if (status == StatusGroup.ACTIVE) {
            	jobs.addAll(idToActiveJobs.keySet());
            } else if (status == StatusGroup.INACTIVE) {
                jobs.addAll(idToInactiveJobs.keySet());            	
            }
            return jobs;//(String[]) jobs.toArray();
        }
    }

    /// <summary>
    /// Returns all the Jobs based on StatusGroup
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    protected Job[] getJobs(StatusGroup status)
    {
    	logger.fine("Getting Jobs from " + status + " status group.");
        synchronized(this)
        {
            List<Job> jobs = new ArrayList<Job>();
            if (status == StatusGroup.ACTIVE) {
            	jobs.addAll(idToActiveJobs.values());
            } else if (status == StatusGroup.INACTIVE) {
                jobs.addAll(idToInactiveJobs.values());            	
            } else if (status == StatusGroup.NEW) {
            	jobs.addAll(newJobs);
            }
            return jobs.toArray(new Job[] {});
        }
    }

    /// <summary>
    /// Returns the job with the given Id and status, if it exists.
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    protected Job getJob(String jobId)
    {
        Job job = null;
        synchronized(this)
        {
            if (idToActiveJobs.containsKey(jobId))
                job = idToActiveJobs.get(jobId);
            else if (idToInactiveJobs.containsKey(jobId))
                job = idToInactiveJobs.get(jobId);
            return job;
        }
    }

    /// <summary>
    /// Gets all Jobs.
    /// </summary>
    protected Job[] getJobs()
    {
        synchronized(this)
        {
            List<Job> jobs = new ArrayList<Job>(newJobs);
            jobs.addAll(idToActiveJobs.values());
            jobs.addAll(idToInactiveJobs.values());
            return jobs.toArray(new Job[] {});
        }
    }

    /// <summary>
    /// Returns the count of Jobs with a given Status.
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    protected int getCount(StatusGroup status)
    {
        synchronized(this)
        {
            int count = 0;
            if (status == StatusGroup.ACTIVE) {
                count = idToActiveJobs.size();
            } else if (status == StatusGroup.INACTIVE) {
                count = idToInactiveJobs.size();
            } else if (status == StatusGroup.NEW) {
                count = newJobs.size();
            }
            return count;
        }
    }

    /// <summary>
    /// Gets the total count of Jobs for all Statuses.
    /// </summary>
    protected int getCount()
    {
        synchronized(this)
        {
            int count = idToActiveJobs.size();
            count += idToInactiveJobs.size();
            count += newJobs.size();
            return count;
        }
    }

    protected void addStatusListener(JobTableStatusListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	synchronized (statusListeners) {
    		statusListeners.add(listener);
		}
    }

    protected void removeStatusListener(JobTableStatusListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	synchronized (statusListeners) {
    		statusListeners.remove(listener);
		}
    }    
    
    private void raiseJobTableStatusChanged(JobStatus status)
    {
        JobTableStatusEvent event = new JobTableStatusEvent(status);
        List<JobTableStatusListener> copy = new ArrayList<JobTableStatusListener>();
    	synchronized (statusListeners) {
    		copy.addAll(statusListeners);
    	}
		for (JobTableStatusListener listener : copy) {
			try {
				listener.onJobTableStatusChanged(event);
			} catch (Exception ex) {
				logger.warning("Error raising job table status changed event. Continuing..." + ex.toString());
			}
		}
    }
}


