
package com.utilify.framework.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;

/*
 * Represents a job that has completed execution.
 * <p>Java class for JobStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobCompletionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplicationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JobInstance" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="ExecutionLog" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExecutionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents information about a completed job. A job is said to be completed if it has had a chance to
 * run to completion (with or without failure) on a remote machine.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobCompletionInfo", propOrder = {
	"jobId",
	"applicationId",
    "jobInstance",
    "executionLog",
    "executionError",
    "startTime",
    "finishTime"
})
public class JobCompletionInfo {
    
	@XmlElement(name = "JobId", required = true, nillable = true)
	private String jobId;
	@XmlElement(name = "ApplicationId", required = true, nillable = true)
	private String applicationId;
	@XmlElement(name = "JobInstance", required = true, nillable = true)
	private byte[] jobInstance;
    @XmlElement(name = "ExecutionLog", required = true, nillable = true)
    private String executionLog;
    @XmlElement(name = "ExecutionError", required = true, nillable = true)
    private String executionError;

    @XmlElement(name = "StartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date startTime;

    @XmlElement(name = "FinishTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date finishTime;

    //private TimeSpan cpuTime; //Issue 018: set this property on the executor

    private JobCompletionInfo() { }

    /**
     * @param jobId
     * @param applicationId
     * @param finalJobInstance
     * @param log
     * @param error
     */
    public JobCompletionInfo(String jobId, String applicationId, 
    		byte[] finalJobInstance, String log, String error) {
    	setJobId(jobId);
    	setApplicationId(applicationId);
    	setJobInstance(finalJobInstance);
    	setExecutionLog(log);
    	setExecutionError(error);
	}
    
    /**
     * 
     * @param jobId
     * @param applicationId
     * @param finalJobInstance
     * @param log
     * @param error
     * @param startTime
     * @param finishTime
     */
    public JobCompletionInfo(String jobId, String applicationId, 
    		byte[] finalJobInstance, String log, String error, Date startTime, Date finishTime) {
    	this(jobId, applicationId, finalJobInstance, log, error);
    	setStartTime(startTime);
    	setFinishTime(finishTime);
	}

	/**
     * Gets the job id.
     * @return
     * 	the Id of this job.
     */
    public String getJobId(){
        return jobId;
    }
    
    @SuppressWarnings("unused") //used by JAXWS
	private void setJobId(String value){
        if (value == null || value.trim().length() == 0)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");

        jobId = value;
    }

    /**
     * Gets the Id of the application associated with this job
     * @return
     * 	the application Id.
     */
    public String getApplicationId() {
        return applicationId;
    }
    
    @SuppressWarnings("unused") //used by JAXWS
    private void setApplicationId(String value) {
        if (value == null || value.trim().length() == 0)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");

        applicationId = value;
    }

    /**
     * Gets the serialised instance of this job.
     * @return
     * 	serialised instance of this job.
     */
    public byte[] getJobInstance()
    {
        return jobInstance;
    }
    
    @SuppressWarnings("unused") //used by JAXWS
    private void setJobInstance(byte[] value) {
        if (value == null || value.length == 0 )
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobInstance");
        
        jobInstance = value;        
    }

    /**
     * Gets the execution log for this job.
     * @return
     * 	the execution log.
     */
    public String getExecutionLog() {
        return executionLog;
    }
    
    @SuppressWarnings("unused") //used by JAXWS
    private void setExecutionLog(String value){
    	if (value == null)
    		executionLog = "";
    	else
    		executionLog = value;
    }

    /**
     * Gets the execution error(s), if any, for this job.
     * @return
     * 	the execution error(s).
     */
    public String getExecutionError(){
        return executionError;
    }
    
    @SuppressWarnings("unused") //used by JAXWS
	private void setExecutionError(String value){
    	if (value == null)
    		executionError = "";
    	else
    		executionError = value;
    }

	/**
	 * @return the finishTime
	 */
	public Date getFinishTime() {
		return finishTime;
	}

	/**
	 * @param finishTime the finishTime to set
	 */
	private void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	private void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

}