
package com.utilify.framework;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/*
 * <p>Java class for ArgumentFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArgumentFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsValueNull" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ParameterName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents a SOAP fault that occurs when an invalid argument is passed to the server. 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = ArgumentFault.FaultName, propOrder = {
    "isValueNull",
    "message",
    "parameterName"
})
public class ArgumentFault {

	/**
	 * The name of the fault (as represented in the XML Schema)
	 */
	public static final String FaultName = "ArgumentFault";
	
    @XmlElement(name = "IsValueNull")
    private boolean isValueNull;
    @XmlElement(name = "Message", required = true, nillable = true)
    private String message;
    @XmlElement(name = "ParameterName", required = true, nillable = true)
    private String parameterName;

    /**
     * Gets a value specifying whether the parameter that caused the <code>ArgumentFault</code> was a null reference. 
     * @return <code>true</code> if the value of the parameter passed in was a null reference, <code>false</code> otherwise
     */
    public boolean isValueNull() {
        return isValueNull;
    }

    /**
     * Sets a value specifying whether the parameter that caused the <code>ArgumentFault</code> was a null reference. 
     */
    @SuppressWarnings("unused") //used by JAXWS
    private void setValueNull(boolean value) {
        this.isValueNull = value;
    }

    /**
     * Gets the fault message.
     * @return the fault message  
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the fault message.
     * @param value - the fault message   
     */
    @SuppressWarnings("unused") //used by JAXWS
    private void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the name of parameter.
     * @return paramater name
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * Sets the name  of the parameter.
     * @param value - the parameter name
     */
    @SuppressWarnings("unused") //used by JAXWS
	private void setParameterName(String value) {
        this.parameterName = value;
    }

}
