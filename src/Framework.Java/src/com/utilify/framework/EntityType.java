
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for EntityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Application"/>
 *     &lt;enumeration value="Job"/>
 *     &lt;enumeration value="Dependency"/>
 *     &lt;enumeration value="Result"/>
 *     &lt;enumeration value="Executor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
/**
 * Represents the type of an entity. Entities can be of the following types:<br>
 * <li>Application</li><br>
 * An application is a logical grouping of work units that are executed in the distributed system.
 * <li>Job</li><br>
 * A job is the smallest atomic work unit that can be individually addressed, scheduled, and executed on a machine in the distributed system.
 * <li>Dependency</li><br>
 * A dependency is a resource (typically a file) that is needed for executing an application or a job.
 * <li>Result</li><br>
 * A result is a resource (typically a file) that is produced by the execution of a job.
 * <li>Executor</li><br>
 * An executor is a machine that executes a job.
 */
@XmlType(name = "EntityType")
@XmlEnum
public enum EntityType {

    /**
     * EntityType for an Application.
     */
    @XmlEnumValue("Application")
    APPLICATION("Application"),
    /**
     * EntityType for an Job.
     */
    @XmlEnumValue("Job")
    JOB("Job"),
    /**
     * EntityType for an Dependency.
     */
    @XmlEnumValue("Dependency")
    DEPENDENCY("Dependency"),
    /**
     * EntityType for an Result.
     */
    @XmlEnumValue("Result")
    RESULT("Result"),
    /**
     * EntityType for an Executor.
     */
    @XmlEnumValue("Executor")
    EXECUTOR("Executor");
    private final String value;

    EntityType(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>EntityType</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>EntityType</code> object.
     * @param v - the value from which to create the <code>EntityType</code>.
     * @return the <code>EntityType</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable type values.
     */
    public static EntityType fromValue(String v) {
        for (EntityType c: EntityType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
