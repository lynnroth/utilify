
package com.utilify.framework;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A utility class used internally in the framework.
 */
public final class Helper {

	private static Logger logger = Logger.getLogger(Helper.class.getName());
	
	enum CompressedFileFormat {
		/** Zip file format */
		ZIP,
		/** Jar file format */
		JAR
	}
	
	/**
	 * Returns a value specifying if the specified object (is of a class that) 
	 * implements {@link java.io.Serializable} or {@link java.io.Externalizable}   
	 * @param obj - the object that may implement <code>Serializable</code> or <code>Externalizable</code>.
	 * @return true if the object's class implements <code>Serializable</code> or <code>Externalizable</code>.
	 */
	public static boolean isMarkedSerializable(Object obj) {
		final String serializable = java.io.Serializable.class.getName();
		final String externalizable = java.io.Externalizable.class.getName();
		
		Class c = obj.getClass();
		//walk back up hierarchy to check interfaces
		while (c != null) {
			//Get all interfaces for this class into an array
			Class[] interfaces = c.getInterfaces();
			//is the Serializable interface one of them?
			for (int i = 0; i < interfaces.length; i++) {
				String interfaceName = interfaces[i].getName();
				if (interfaceName.equals(serializable) || interfaceName.equals(externalizable)) {
					return true;
				}
			}
			c = c.getSuperclass();
		}
		return false;
	}

	/*
	 * Gets a copy of the given list.
	 * If the list object is null, an empty array is returned - NOT null.
	 * @param <T>
	 * @param list
	 * @return
	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T[] getCopy(Collection<T> list, Class<T> componentType){
//        List<T> copy = null;
//
//        if (list != null)
//            copy = new ArrayList<T>(list);
//        else
//            copy = new ArrayList<T>();
//
//        T[] toArray = (T[])Array.newInstance(componentType, copy.size());;
//        copy.toArray(toArray); //due to the stupid 'erasure' implementation of Java's generics 
//		return toArray;
//    }

    /**
     * Reads the file at the specified path and returns its raw contents.
     * @param fullPath - the path of the file to read.
     * @return the binary contents of the file read.
     * @throws IOException if there is an I/O error reading the file.
     * @throws IllegalArgumentException if <code>fullPath</code> is null or empty, or points to a directory.
     */
    public static byte[] readFile(String fullPath) throws IOException {
        if (fullPath == null || fullPath.trim().length() == 0)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": fullPath");
        File file = new File(fullPath);
        return readFile(file);
    }
    
	/**
     * Reads the file at the specified path and returns its raw contents.
     * @param file - the file to read.
     * @return the binary contents of the file read.
     * @throws IOException if there is an I/O error reading the file.
     * @throws IllegalArgumentException if <code>file</code> is null or points to a directory.
	 */
	public static byte[] readFile(File file) throws IOException {
		if (file == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": file");
		if (file.isDirectory())
			throw new IllegalArgumentException("The given path cannot point to a directory.");
		
        byte[] contents = null;
        FileInputStream fs = null;
        BufferedInputStream bis = null;
        try{
	        contents = new byte[(int)file.length()]; 
	        fs = new FileInputStream(file);
	        bis = new BufferedInputStream(fs);
	        boolean eof = false;
	        while (!eof){
	            int retval = bis.read(contents);
	            eof = (retval == -1);
	        }
        }finally{
        	if (bis != null)
        		bis.close();
        	if (fs != null)
        		fs.close();
        }
        return contents;
	}
	
    /**
     * Gets the minimum Utc date accepted by the server
     * @return a <code>Date</code> object 
     */
	public static Date getMaxUtcDate() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		//cal.set(year, month, date, hourOfDay, minute, second)
		cal.set(9999, Calendar.DECEMBER, 31, 23, 59, 59);
		return cal.getTime();
	}
	
	/**
	 * Gets the maximum Utc date accepted by the server
	 * @return a <code>Date</code> object
	 */
	public static Date getMinUtcDate() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		//cal.set(year, month, date, hourOfDay, minute, second)
		cal.set(0, Calendar.JANUARY, 1, 0, 0, 0);
		return cal.getTime();
	}

    /**
     * Serializes the specified object and returns the serialized data.
     * @param obj - the object to serialize.
     * @return the binary data of the serialized object.
     * @throws IOException if there is an I/O error serializing the object.
     * @throws IllegalArgumentException if <code>obj</code> is a null reference.
     */
    public static byte[] serialize(Object obj) throws IOException {
        // todoLater: abstract this out to use whatever serializer : to perhaps
		// conditionally compress / encrypt etc
        //for now we're just using a binary one
        ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
        ObjectOutputStream out = null;
        byte[] instance = null;
        try {
        	out = new ObjectOutputStream(bos) ;
        	out.writeObject(obj);
        	instance = bos.toByteArray();
        } finally {
        	try{
        		bos.close();
        		if (out != null)
        			out.close();
        	}catch (IOException ix){
        		logger.fine("Error closing object output stream: " + ix.getMessage());
        	}
        }
        
    	return instance;
    }
    
	/**
	 * Deserializes the specified data and returns the deserialized object.
	 * @param instance - the serialized data.
	 * @return the object instance represented by the specified data.
	 * @throws IOException if there is an I/O error deserializing the object.
	 * @throws IllegalArgumentException if <code>instance</code> is a null reference.
	 */
	public static Object deserialize(byte[] instance) throws IOException{
    	if (instance == null || instance.length == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": instance");
    		
    	Object obj = null;
    	ByteArrayInputStream bis = new ByteArrayInputStream(instance);
    	ObjectInputStream in = null;
    	try {
    		in = new ObjectInputStream(bis);
    		obj = in.readObject();
    	} catch (ClassNotFoundException e) {
    		logger.warning("Error deserializing object input stream: " + getFullStackTrace(e));
		} finally {
    		try{
        		bis.close();
        		if (in != null)
        			in.close();
        	}catch (IOException ix){
        		logger.fine("Error closing object output stream: " + ix);
        	}    		
    	}
    	
    	return obj;
    }
	
	/**
	 * Gets the full stack trace as a string from an exception
	 * @param ex
	 * @return string representing the exception message and stack trace
	 */
	public static String getFullStackTrace(Exception ex){
		if (ex == null)
			throw new IllegalArgumentException("Exception cannot be null");
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String msg = sw.toString();
		pw.close();
		return msg;
	}

    /**
     * Jars up a directory and returns the jar file.
     * @param directory - the directory containing classes or data files.
     * @return the <code>File</code> that points to the newly created jar file.
     * @throws IOException if there was an I/O error reading a file.
     */
	public static File jarDirectory(File directory) throws IOException {
		if (directory == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": directory");
		if (!directory.exists())
			throw new FileNotFoundException(directory.getCanonicalPath());
		
		return compressDirectory(directory, CompressedFileFormat.JAR);
	}
	
	/**
	 * Jars up a directory and returns the jar file. 
	 * @param files - the files to jar.
	 * @param jarFileName - the name of the jar file.
	 * @throws IOException if there was an I/O error reading a file.
	 * @return the <code>File</code> that points to the newly created jar file.
	 */
	public static File jarFiles(Map<File, String> fileRootDirectories, String jarFileName) throws IOException{
		
		File jarFile = new File(jarFileName);
		if (jarFile.exists())
			jarFile.delete();
		
		FileOutputStream fos = new FileOutputStream(jarFile);
		ZipOutputStream zos = new JarOutputStream(fos, new Manifest());
		
		try {
			Set<File> files = fileRootDirectories.keySet();
			for (File file : files) {
				compressFile(fileRootDirectories.get(file), file, zos, CompressedFileFormat.JAR);	
			}
		} finally {
			try {
				zos.flush();
				zos.close();
				fos.close();
			} catch (IOException e) {
				logger.fine("Error closing compressed stream." + e.getMessage());
			}
		}
		
		return jarFile;
	}

    /**
     * Zips up a directory and returns the zip file.
     * @param directory - the directory containing classes or data files.
     * @return the <code>File</code> that points to the newly created zip file.
     * @throws IOException if there was an I/O error reading a file.
     */
	public static File zipDirectory(File directory) throws IOException {
		if (directory == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": directory");
		if (!directory.exists())
			throw new FileNotFoundException(directory.getCanonicalPath());
		
		return compressDirectory(directory, CompressedFileFormat.ZIP);
	}
	
	private static File compressDirectory(File directory, CompressedFileFormat format) throws IOException {

		String ext;
		if (format == CompressedFileFormat.JAR)
			ext = ".jar";
		else
			ext = ".zip";
		
		File zipFile = new File(directory.getName() + ext);
		if (zipFile.exists())
			zipFile.delete();
		
		ZipOutputStream zos = null;
		FileOutputStream fos = new FileOutputStream(zipFile);
		
		if (format == CompressedFileFormat.JAR)
			zos = new JarOutputStream(fos, new Manifest());
		else
			zos = new ZipOutputStream(fos);
		
		String root = directory.getCanonicalPath();
		try {
			compressDirectory(root, directory, zos, format);
		} finally {
			try {
				zos.flush();
				zos.close();
				fos.close();
			} catch (IOException e) {
				logger.fine("Error closing compressed stream." + e.getMessage());
			}
		}
		
		return zipFile;		
	}

	private static void compressDirectory(String root, File directoryToZip, ZipOutputStream zos, CompressedFileFormat format) throws IOException {
		String[] dirList = directoryToZip.list(); 
		
		//list the dirs and files and go through them recursively
		for (int i = 0; i < dirList.length; i++){
			File file = new File(directoryToZip, dirList[i]); 
			if (file.isDirectory()){
				compressDirectory(root, file, zos, format);
			} else {
				compressFile(root, file, zos, format);
			}
		}
	}

	/**
	 * @param root
	 * @param zos
	 * @param format
	 * @param readBuffer
	 * @param file
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static void compressFile(String root, File file, ZipOutputStream zos, CompressedFileFormat format) throws IOException {
		byte[] readBuffer = new byte[8192]; //8 KB buffer 

//		String fullPath = file.getCanonicalPath().replace(File.separatorChar, '/');				
//		String filename = fullPath.substring(root.length() + 1);
		String fullPath = file.getCanonicalPath();				
		String filename = fullPath.substring(root.length());
//		 need to change the separator char for the Jar file entry.
		filename = filename.replace(File.separatorChar, '/');
		
		System.out.println("Adding file (root): '" + root + "'");
		System.out.println("Adding file (full  path): '" + fullPath + "'");
		System.out.println("Adding file (entry path): '" + filename + "'");
		ZipEntry anEntry;
		if (format == CompressedFileFormat.JAR)
			anEntry = new JarEntry(filename);
		else
			anEntry = new ZipEntry(filename);
		
		zos.putNextEntry(anEntry); 
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		try {
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis);
			//now write the content of the file to the ZipOutputStream 
			int bytesIn = 0;
			while(bytesIn != -1){
				bytesIn = bis.read(readBuffer);
				if (bytesIn > 0)
					zos.write(readBuffer, 0, bytesIn);
			}
			zos.closeEntry();
		} finally {
			if (bis != null)
				bis.close();
			if (fis != null)
				fis.close();
		}
	}
	
    /**
     * @param thread
     * @param timeoutMillis
     */
    public static void waitAndAbort(Thread thread, int timeoutMillis) {
    	if (thread != null && thread.isAlive()) {
    		try {
    			thread.join(timeoutMillis);
    		} catch (InterruptedException e) {
    			logger.fine("Thread " + thread.getName() + " interrupted.");
    		}
    		thread.interrupt(); //todoLater: check for best practice on stopping threads
    	}
    }

	/* todoLater
	    ApplicationManageClient sending dependency Contents in .NET 
		Dependencies: link to users (later on)
	 */
}
