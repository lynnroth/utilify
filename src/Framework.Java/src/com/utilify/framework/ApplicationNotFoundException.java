
package com.utilify.framework;

/**
 * Thrown when an application with the specified Id is not found.
 */
public class ApplicationNotFoundException extends FrameworkException {

	private static final long serialVersionUID = 7814379245328029808L;
	private String applicationId = null;

    /**
     * Constructs an ApplicationNotFoundException with the specified detail message and application Id.
     * @param message - the detail message.
     * @param applicationId - the Id of the application that could not be found.
     */
    public ApplicationNotFoundException(String message, String applicationId){
    	super (String.format("%1$s. Application id : '%2$s'", message, applicationId));
        this.applicationId = applicationId;
    }
    
    /**
     * Constructs an ApplicationNotFoundException with no detail message.
     */
    public ApplicationNotFoundException(){
    	super();
    }
    /**
     * Constructs an ApplicationNotFoundException with the specified detail message.
     * @param message - the detail message.
     */
    public ApplicationNotFoundException(String message){
    	super(message);
    }
    /**
     * Constructs an ApplicationNotFoundException with the specified detail message
     * and cause.
     * @param message - the detail message.
     * @param cause - the cause of the exception.
     */
    public ApplicationNotFoundException(String message, Throwable cause){
    	super(message, cause);
    }
    
    /**
     * Gets the application Id value that could not be found
     * @return the application Id
     */
    public String getApplicationId(){
    	return applicationId;
    }
}