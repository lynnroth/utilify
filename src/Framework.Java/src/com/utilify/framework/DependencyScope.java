
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for DependencyScope.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DependencyScope">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Job"/>
 *     &lt;enumeration value="Application"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
/**
 * Represents the scope of a dependency. Dependencies are scoped to an application or a job. This means that 
 * the dependency is needed for execution of an application (that is, the dependency is common across all its jobs),
 * or a job.
 * <p>The possible values for scope are:<br>
 * <li>Application</li><br>
 * The dependency is scoped to an application.
 * <li>Job</li><br>
 * The dependency is scoped to a single job.
 */
@XmlType(name = "DependencyScope")
@XmlEnum
public enum DependencyScope {

    /**
     * Dependency scope for a job.
     */
    @XmlEnumValue("Job")
    JOB("Job"),
    /**
     * Dependency scope for an application.
     */
    @XmlEnumValue("Application")
    APPLICATION("Application");
    private final String value;

    DependencyScope(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>DependencyScope</code>
     * @return the value of the scope (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>DependencyScope</code> object.
     * @param v - the value from which to create the <code>DependencyScope</code>.
     * @return the <code>DependencyScope</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable scope values.
     */
    public static DependencyScope fromValue(String v) {
        for (DependencyScope c: DependencyScope.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
