﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IdentityModel.Policy;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;

using Utilify.Platform.Configuration;
using Utilify.Platform.Manager.Security;
using System.Configuration;

namespace Utilify.Platform.Manager
{
    internal class ManagerServiceHost : ServiceHost
    {
        private static Logger logger = new Logger();
        
        private Type contractType;
        //private Type serviceType;

        //can't use the base .ctor (serviceType, baseAddresses) because we want to do extra validation BEFORE ApplyConfiguration,
        //in which we need the contractType
        internal ManagerServiceHost(Type serviceType, Type contractType, params Uri[] baseAddresses) : base()
        {
            //we still need to check the serviceType for validation purposes, and to initialize the actual service itself.
            ValidationHelper.CheckNull(serviceType, "serviceType");
            ValidationHelper.CheckNull(contractType, "contractType");

            if (!contractType.IsAssignableFrom(serviceType))
                ThrowHelper.ArgumentException(Errors.ServiceShouldImplContract, "serviceType");

            //this.serviceType = serviceType; //don't need this anymore: since we're using contractType everywhere in this class.
            this.contractType = contractType;

            //need to call this ourselves since we are not using the base .ctor (serviceType, baseAddresses)
            this.InitializeDescription(serviceType, new UriSchemeKeyedCollection(baseAddresses));

        }

        internal bool ApplyServiceModelConfig { get; set; }

        //this is called before the .ctor completes
        protected override void ApplyConfiguration()
        {
            //conditionally apply service model / utilify config
            if (ApplyServiceModelConfig)
            {
                base.ApplyConfiguration();
                ValidateServiceModelConfig();
            }
            else
            {
                ApplyCustomConfiguration();
            }
        }

        private void ApplyCustomConfiguration()
        {
            ManagerSectionGroup group = ConfigurationHelper.GetSectionGroup(true);

            //we follow the naming convention with the 'I' prefix for interface naming.
            //for configuration, we expect the service names to follow a convention:
            //  - the simple name of the interface without the 'I' in front
            //so: remove the 'I' in front and look it up! ;)
            logger.Debug("group.Services is null ? " + (group.Services == null) + ", contractType is null ? " + (contractType == null));

            ServiceElement serviceEl = group.Services.LookupService(this.ServiceName);

            if (serviceEl == null)
                throw new ConfigurationErrorsException("Invalid configuration: Could not find service: " + this.ServiceName);

            //apply utilify config :
            
            //0. base Uri and close timeout:
            Uri baseUri = GetEndPointAddress(serviceEl);
            this.CloseTimeout = new TimeSpan(0); //when calling close, close immediately

            //1. Add required endpoints
            //add an endpoint each authentication type
            foreach (AuthenticationModeElement mode in serviceEl.AuthenticationModes)
            {
                string epAddress = ProxyFactory.GetRelativeAddress(mode.Type);
                epAddress = new Uri(baseUri, epAddress).ToString();
                logger.Debug("Adding endpoint for service " + contractType + " with " + mode.Type + " auth @ " + epAddress);

                AddServiceEndpoint(contractType,
                    CreateBinding(serviceEl.IsInteropEndPoint, mode.Type),
                    epAddress);

                if (mode.Type == CredentialType.Username)
                {
                    this.Credentials.UserNameAuthentication.UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom;
                    this.Credentials.UserNameAuthentication.CustomUserNamePasswordValidator = GetUserValidator();

                    //Set custom validation mode and server certificate for custom username auth
                    SetServerCertificate(group.ServiceCertificate);
                }
            }

            //2. Set service name
            this.Description.Name = contractType.Name.Substring(1); //can't use  serviceType.FullName; because it will be obfuscated. Instead, we use the interface contract name without the 'I' ;)

            //3. For Debug builds, enable exception propagation
#if DEBUG
            ServiceDebugBehavior sdbg = this.Description.Behaviors.Find<ServiceDebugBehavior>();
            if (sdbg == null)
            {
                sdbg = new ServiceDebugBehavior();
                sdbg.IncludeExceptionDetailInFaults = true;
                this.Description.Behaviors.Add(sdbg);
            }
            else
            {
                sdbg.IncludeExceptionDetailInFaults = true; //make sure we have this for debug
            }
#else
            this.Description.Behaviors.RemoveAll<ServiceDebugBehavior>();
#endif

            //6. remove the metadata behaviors
            this.Description.Behaviors.RemoveAll<ServiceMetadataBehavior>();

            //7. add custom authz policy to service host behaviour
            this.Authorization.ExternalAuthorizationPolicies = GetExternalAuthzPolicies();

            //8. Add some throttling behaviour
            ServiceThrottlingBehavior throttle = this.Description.Behaviors.Find<ServiceThrottlingBehavior>();
            if (throttle == null)
            {
                throttle = new ServiceThrottlingBehavior();
                this.Description.Behaviors.Add(throttle);
            }

            bool isThrottlePresent = serviceEl.ServiceThrottle.ElementInformation.IsPresent;

            throttle.MaxConcurrentCalls = isThrottlePresent ?
                serviceEl.ServiceThrottle.MaxConcurrentCalls : 100;
            throttle.MaxConcurrentSessions = isThrottlePresent ? 
                serviceEl.ServiceThrottle.MaxConcurrentSessions : 100000;
            throttle.MaxConcurrentInstances = isThrottlePresent ? 
                serviceEl.ServiceThrottle.MaxConcurrentInstances : Int32.MaxValue;
            
        }

        private static ReadOnlyCollection<IAuthorizationPolicy> externalAuthzPolicies;
        private static ReadOnlyCollection<IAuthorizationPolicy> GetExternalAuthzPolicies()
        {
            if (externalAuthzPolicies == null)
            {
                // Add a custom authorization policy to the service authorization behavior.
                List<IAuthorizationPolicy> extPolicies = new List<IAuthorizationPolicy>();
                extPolicies.Add(new ManagerAuthorizationPolicy());
                externalAuthzPolicies = extPolicies.AsReadOnly();
            }
            return externalAuthzPolicies;
        }

        private static UserValidator userValidator;
        private static UserValidator GetUserValidator()
        {
            if (userValidator == null)
            {
                userValidator = new UserValidator();
            }
            return userValidator;
        }

        private System.ServiceModel.Channels.Binding CreateBinding(bool isInteropEndPoint, CredentialType credentialType)
        {
            return ProxyFactory.CreateBinding(contractType, isInteropEndPoint, credentialType);
        }


        private void SetServerCertificate(ServiceCertificateSection certSection)
        {
            if (certSection.UseCertificateStore)
            {
                this.Credentials.ServiceCertificate.SetCertificate(certSection.StoreLocation,
                    certSection.StoreName,
                    certSection.FindType,
                    certSection.FindValue);
            }
            else
            {
                X509Certificate2 cert = null;
                if (!string.IsNullOrEmpty(certSection.CertificateAccessPassword))
                    cert = new X509Certificate2(certSection.CertificateFilePath,
                        certSection.CertificateAccessPassword);
                else
                    cert = new X509Certificate2(certSection.CertificateFilePath, (string)null, X509KeyStorageFlags.MachineKeySet);

                this.Credentials.ServiceCertificate.Certificate = cert;
            }
        }

        private string ServiceName
        {
            get
            {
                return this.contractType.Name.Substring(1);
            }
        }

        private Uri GetEndPointAddress(bool isInteropEndPoint, string host, int port)
        {
            string proto = isInteropEndPoint ? Uri.UriSchemeHttp : Uri.UriSchemeNetTcp; //
            string serviceName = this.ServiceName;

            UriBuilder ub = new UriBuilder(proto, host, port,
                string.Concat(ProxyFactory.CommonServicePath, "/", serviceName, "/"));

            logger.Debug("Endpoint: " + ub.Uri.ToString());

            return ub.Uri;

        }

        private Uri GetEndPointAddress(ServiceElement serviceEl)
        {
            string host = Dns.GetHostEntry(Dns.GetHostName()).HostName;
            int port = serviceEl.Port;

            return GetEndPointAddress(serviceEl.IsInteropEndPoint, host, port);
        }

        private void ValidateServiceModelConfig()
        {
            //check:
            /*
             * - Authz policies
             * - user name validator
             * - streamed mode checks
             */

            //1. verify authz policy
            ReadOnlyCollection<IAuthorizationPolicy> authzPolicies = this.Authorization.ExternalAuthorizationPolicies;
            bool foundAuthzPolicy = false;
            for (int i = 0; i < authzPolicies.Count; i++)
            {
                string id = authzPolicies[0].Id ?? string.Empty;
                if (id.StartsWith(ManagerAuthorizationPolicy.IdPrefix))
                {
                    foundAuthzPolicy = true;
                    break;
                }
            }
            if (!foundAuthzPolicy)
                ThrowHelper.ConfigurationException(Errors.AuthzPolicyMissing);

            //2. verify user validator
            if (this.Credentials.UserNameAuthentication.UserNamePasswordValidationMode == UserNamePasswordValidationMode.Custom)
            {
                if (this.Credentials.UserNameAuthentication.CustomUserNamePasswordValidator.GetType() != typeof(UserValidator))
                {
                    ThrowHelper.ConfigurationException(Errors.InvalidUserValidatorType);
                }
            }

            //3. verify streamed mode
            bool isStreamedContract = IsStreamableContract();
            if (isStreamedContract)
            {
                for (int i = 0; i < this.Description.Endpoints.Count; i++)
                {
                    var ep = this.Description.Endpoints[i];
                    if (ep.ListenUri.Scheme == Uri.UriSchemeNetTcp)
                    {
                        NetTcpBinding tcp = ep.Binding as NetTcpBinding;
                        if (tcp.TransferMode != TransferMode.Streamed)
                            ThrowHelper.ConfigurationException(Errors.InvalidUserValidatorType);
                    }
                    //todo: what about other binding types? how do we check?
                }
            }
        }

        //todo: could use a TypeExtensions class and put this method there.
        private bool IsStreamableContract()
        {
            bool isStreamed = false;
            object[] attrs = contractType.GetCustomAttributes(typeof(StreamedBehaviorAttribute), false);
            if (attrs != null && attrs.Length == 1)
            {
                isStreamed = true;
            }
            return isStreamed;
        }
    }
}
