using System;
using System.Collections.Generic;
using System.Text;

using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Executor : ITransformable<SystemInfo>, ITransformable<PerformanceInfo>
    {
        /// <summary>
        /// Default ping interval for executors in milliseconds
        /// </summary>
        public const long DefaultPingInterval = 8000;
        private Guid id = Guid.Empty; //persistence Id : assigned at server
        private IList<Processor> processors;
        private IList<Disk> disks;
        private IList<Memory> memory;
        private OperatingSystem operatingSystem;
        private string uri;
        private string username;

        //should all these properties below be in a seperate class and/or db table?
        private bool dedicated;
        private long pingInterval = DefaultPingInterval; //this tells the manager when to expect the next ping
        private DateTime lastPingTime; //this is used only on the manager side to determine when the executor has last pinged.
        private long version = DataStore.UnsavedVersionValue;

        private JobStatistics jobStatistics;

        protected Executor() { }

        public Executor(SystemInfo info, string username) : this()
        {
            if (info == null)
                throw new ArgumentNullException("info");
            if (String.IsNullOrEmpty(username))
                throw new ArgumentException("username cannot be null or empty", "username");

            this.Username = username;

            this.Id = Helper.GetGuidSafe(info.Id);
            // Processors
            this.processors = new Processor[info.Processors.Length];
            for (int i=0;i<info.Processors.Length;i++) {
                this.processors[i] = new Processor(info.Processors[i]);
            }
            // Disks
            this.disks = new Disk[info.Disks.Length];
            for (int i = 0; i < info.Disks.Length; i++)
            {
                this.disks[i] = new Disk(info.Disks[i]);
            }
            // Memory (treating as an array field internally so hibernate mapping works)
            this.memory = new Memory[1];
            this.memory[0] = new Memory(info.Memory);
            // Other...
            this.OperatingSystem = new OperatingSystem(info.OS);
            this.Uri = info.Uri;
            this.Dedicated = info.Dedicated;
            this.PingInterval = info.PingInterval;
            this.LastPingTime = info.LastPingTime;

        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        /// <summary>
        /// updates the performace data fields for this executor
        /// </summary>
        /// <param name="info"></param>
        protected internal virtual void UpdatePerformance(PerformanceInfo info)
        {
            // Double check that the Id is the same.
            if (info == null)
                throw new ArgumentNullException("info");

            if (String.IsNullOrEmpty(info.SystemId))
                throw new ArgumentNullException("info.SystemId");

            if (!info.SystemId.Equals(this.Id.ToString()))
                throw new ArgumentException("Specified Id does not match the existing Id for this Executor", "info.SystemId");

            if (this.processors != null)
            {
                // loop through and make sure we're updating data for the right components
                foreach (Processor proc in this.processors)
                {
                    foreach (ProcessorPerformanceInfo updatedProcPerfInfo in info.Processors)
                    {
                        if (proc.Name.Equals(updatedProcPerfInfo.Name))
                        {
                            proc.UpdatePerformance(updatedProcPerfInfo);
                            break;
                        }
                    }
                }
            }
            if (this.disks != null)
            {
                foreach (Disk disk in this.disks)
                {
                    foreach (DiskPerformanceInfo updatedDiskPerfInfo in info.Disks)
                    {
                        if (disk.Root.Equals(updatedDiskPerfInfo.Root))
                        {
                            disk.UpdatePerformance(updatedDiskPerfInfo);
                            break;
                        }
                    }
                }
            }
            if (this.memory != null && this.memory.Count > 0)
                this.memory[0].UpdatePerformance(info.Memory);
        }

        /// <summary>
        /// updates the limit values for this executor
        /// </summary>
        /// <param name="info"></param>
        protected internal virtual void UpdateLimits(LimitInfo info)
        {
            // Double check that the Id is the same.
            if (info == null)
                throw new ArgumentNullException("info");

            if (String.IsNullOrEmpty(info.SystemId))
                throw new ArgumentNullException("info.SystemId");

            if (!info.SystemId.Equals(this.Id.ToString()))
                throw new ArgumentException("Specified Id does not match the existing Id for this Executor", "info.SystemId");

            if (this.processors != null)
            {
                // loop through and make sure we're updating data for the right components
                foreach (Processor proc in this.processors)
                {
                    foreach(ProcessorLimitInfo updatedProcLimit in info.Processors)
                    {
                        if (proc.Name.Equals(updatedProcLimit.Name))
                        {
                            proc.UpdateLimits(updatedProcLimit);
                            break;
                        }
                    }
                }
            }
            if (this.disks != null)
            {
                foreach (Disk disk in this.disks)
                {
                    foreach (DiskLimitInfo updatedDiskLimit in info.Disks)
                    {
                        if (disk.Root.Equals(updatedDiskLimit.Root))
                        {
                            disk.UpdateLimits(updatedDiskLimit);
                            break;
                        }
                    }
                }
            }

            if (this.memory != null && this.memory.Count > 0)
                this.memory[0].UpdateLimits(info.Memory);
        }

        /// <summary>
        /// updates the system data fields for this executor. Adds/Removes items if needed. Keeps available performance info.
        /// </summary>
        /// <param name="info"></param>
        protected internal virtual void UpdateSystem(SystemInfo info)
        {
            // Double check that the Id is the same.
            if (info == null)
                throw new ArgumentNullException("info");

            if (info.Id == null || info.Id.Trim().Length == 0)
                throw new ArgumentException("info.Id");

            if (!info.Id.Equals(this.Id.ToString()))
                throw new ArgumentException("Specified Id does not match the existing Id for this Executor", "info.Id");

            UpdateCollection<Processor, ProcessorSystemInfo>(this.processors, info.Processors);
            UpdateCollection<Disk, DiskSystemInfo>(this.disks, info.Disks);
            UpdateCollection<Memory, MemorySystemInfo>(this.memory, new MemorySystemInfo[] {info.Memory});
        }

        private void UpdateCollection<T, TInfo>(IList<T> existing, IEnumerable<TInfo> updated) 
            where T : IUpdatable<TInfo>, new()
        {
            if (updated == null)
            {
                if (existing != null)
                {
                    //remove all
                    while (existing.Count > 0)
                        existing.RemoveAt(0);
                }
                return; //nothing more to do
            }
            else 
            {
                if (existing != null)
                {
                    //this is the interesting part:
                    //unfortunately due to
                    //NHibernate's leaky abstraction: http://hi.baidu.com/ekou/blog/item/aa89e80e8df9ece436d122b4.html
                    //we can't replace things like this anymore, since there're proxies underlying the collection!
                    //this.collection = updatedCollection.ToArray();
                    //which means we need to do a couple of loops the each way round to see if any items added/updatd/deleted
                    foreach (TInfo updatedItem in updated)
                    {
                        bool found = false;
                        foreach (T existingItem in existing)
                        {
                            if (existingItem != null && updatedItem != null)
                            {
                                if (existingItem.CanUpdate(updatedItem))
                                {
                                    found = true;
                                    existingItem.Update(updatedItem);
                                    break; //go next
                                }
                            }
                        }

                        if (!found && updatedItem != null)
                        {
                            T newItem = new T();
                            newItem.Update(updatedItem);
                            existing.Add(newItem);
                        }
                    }

                    //now do deleted stuff
                    List<T> itemsToRemove = new List<T>();
                    foreach (T existingItem in existing)
                    {
                        bool found = false;
                        foreach (TInfo updatedItem in updated)
                        {
                            if (existingItem.CanUpdate(updatedItem))
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                            itemsToRemove.Add(existingItem);
                    }

                    //remove them
                    foreach (T item in itemsToRemove)
                    {
                        existing.Remove(item);
                    }
                }
                else
                {
                    //create a new collection
                    existing = new List<T>();
                    foreach (TInfo info in updated)
                    {
                        T domainObject = new T();
                        domainObject.Update(info);
                        existing.Add(domainObject);
                    }
                }
            }
        }
        
        /// <summary>
        /// Gets the id of the system
        /// </summary>
        public virtual Guid Id
        {
            get { return id; }
            protected set { id = value; }
        }

        /// <summary>
        /// Clears the Id in case we want to save as a new object
        /// </summary>
        protected internal virtual void ClearId()
        {
            this.id = Guid.Empty;
        }

        /// <summary>
        /// Gets the Username of the system
        /// </summary>
        public virtual string Username
        {
            get { return username; }
            protected set { username = value; }
        }

        /// <summary>
        /// Gets the processor information of the system
        /// </summary>
        public virtual Processor[] Processors
        {
            get { return Helper.GetCopy<Processor>(processors); }
        }

        /// <summary>
        /// Gets the hard disk information of the system
        /// </summary>
        public virtual Disk[] Disks
        {
            get { return Helper.GetCopy<Disk>(disks); }
        }

        /// <summary>
        /// Gets the memory information of the system
        /// </summary>
        public virtual Memory Memory
        {
            get
            {
                if (memory != null && memory.Count > 0)
                    return memory[0];
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the operation system information of the system
        /// </summary>
        public virtual OperatingSystem OperatingSystem
        {
            get { return operatingSystem; }
            protected set
            {
                this.operatingSystem = value;
            }
        }

        /// <summary>
        /// Gets the uri of the system
        /// </summary>
        public virtual string Uri
        {
            get { return uri; }
            protected set
            {
                this.uri = value;
            }
        }

        /// <summary>
        /// Gets a value indicating if this system is a dedicated node
        /// </summary>
        public virtual bool Dedicated //todoDoc provide more info / links to show what is meant by dedicated
        {
            get { return dedicated; }
            protected set
            {
                this.dedicated = value;
            }
        }

        /// <summary>
        /// Gets the interval (in milliseconds) between consecutive 'ping's of this Executor. This value tells the Manager when to expect the next 'ping' / contact. 
        /// </summary>
        public virtual long PingInterval
        {
            get { return pingInterval; }
            protected set
            {
                this.pingInterval = value;
            }
        }

        /// <summary>
        /// Gets the date when this Executor last contacted the manager.
        /// </summary>
        public virtual DateTime LastPingTime
        {
            get { return lastPingTime; }
            protected internal set { lastPingTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the job statistics for the executor
        /// </summary>
        public virtual JobStatistics JobStatistics
        {
            get { return jobStatistics; }
            protected set { jobStatistics = value; }
        }

        #region DateTime persistence helpers (using ticks)

        protected virtual long LastPingTimeTicks
        {
            get { return lastPingTime.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("lastPingTime", string.Format("Last ping time time cannot be less than {0}", DateTime.MinValue));
                lastPingTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        #endregion

        #region ITransformable<SystemInfo> Members

        SystemInfo ITransformable<SystemInfo>.Transform()
        {
            ProcessorSystemInfo[] processorInfos = DataStore.Transform<Processor, ProcessorSystemInfo>(this.Processors).ToArray();
            DiskSystemInfo[] diskInfos = DataStore.Transform<Disk, DiskSystemInfo>(this.Disks).ToArray();

            MemorySystemInfo memInfo = null;
            ITransformable<MemorySystemInfo> memTransferable = null;
            if (this.memory != null && this.memory.Count > 0)
            {
                memTransferable = this.memory[0] as ITransformable<MemorySystemInfo>;
            }
            if (memTransferable != null)
                memInfo = memTransferable.Transform();
            else
                memInfo = new MemorySystemInfo(0); //get the default.

            OSInfo osInfo = (this.OperatingSystem as ITransformable<OSInfo>).Transform();

            SystemInfo info = new SystemInfo(this.Id.ToString(),
                processorInfos, diskInfos, memInfo, osInfo, this.Uri,
                this.Dedicated, this.PingInterval);
            info.LastPingTime = this.LastPingTime;
            return info;
        }

        #endregion

        #region ITransformable<PerformanceInfo> Members

        PerformanceInfo ITransformable<PerformanceInfo>.Transform()
        {
            ProcessorPerformanceInfo[] processorInfos = DataStore.Transform<Processor, ProcessorPerformanceInfo>(this.Processors).ToArray();
            DiskPerformanceInfo[] diskInfos = DataStore.Transform<Disk, DiskPerformanceInfo>(this.Disks).ToArray();
            
            MemoryPerformanceInfo memInfo = null;
            ITransformable<MemoryPerformanceInfo> memTransferable = null;
            if (this.memory != null && this.memory.Count > 0)
            {
                memTransferable = this.memory[0] as ITransformable<MemoryPerformanceInfo>;
            }
            if (memTransferable != null)
                memInfo = memTransferable.Transform();
            else
                memInfo = new MemoryPerformanceInfo(0, 0); //get the default.

            PerformanceInfo info = new PerformanceInfo(this.Id.ToString(),
                processorInfos, diskInfos, memInfo);
            info.LastPingTime = this.LastPingTime;
            return info;
        }

        #endregion

    }
}
