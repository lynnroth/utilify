using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using Utilify.Platform.Contract;
using System.Diagnostics.CodeAnalysis;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Memory : IUpdatable<MemorySystemInfo>, ITransformable<MemoryPerformanceInfo>, ITransformable<MemoryLimitInfo>
    {
        private long id; // persistence identifier
        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        private Guid? executorId;

        private long sizeTotal; //(in bytes)
        private long sizeTotalFree; //(in bytes)
        // These values could either measure the memory just for the executor process, or for the overall system.
        private long sizeUsageCurrent; //(in bytes)
        private long sizeUsageLimit; //(in bytes)
        private long version = DataStore.UnsavedVersionValue;

        public Memory() { }

        public Memory(MemorySystemInfo info) : this()
        {
            LoadFrom(info);
        }

        private void LoadFrom(MemorySystemInfo info)
        {
            this.SizeTotal = info.SizeTotal;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        protected internal virtual void UpdateSystem(MemorySystemInfo info)
        {
            this.SizeTotal = info.SizeTotal;
        }

        protected internal virtual void UpdatePerformance(MemoryPerformanceInfo info)
        {
            this.SizeTotalFree = info.SizeTotalFree;
            this.SizeUsageCurrent = info.SizeUsageCurrent;
        }

        protected internal virtual void UpdateLimits(MemoryLimitInfo info)
        {
            this.SizeUsageLimit = info.SizeUsageLimit;
        }

        public virtual Guid? ExecutorId
        {
            get { return executorId; }
            protected set { executorId = value; }
        }

        /// <summary>
        /// Gets the total size of the memory module in bytes.
        /// </summary>
        public virtual long SizeTotal
        {
            get { return sizeTotal; }
            protected set
            {
                this.sizeTotal = value;
            }
        }

        /// <summary>
        /// Gets the size of the free memory of the memory module in bytes.
        /// </summary>
        public virtual long SizeTotalFree
        {
            get { return sizeTotalFree; }
            protected set
            {
                this.sizeTotalFree = value;
            }
        }

        /// <summary>
        /// Gets the size limit of usable memory for this memory module in bytes.
        /// </summary>
        public virtual long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
            protected set
            {
                this.sizeUsageCurrent = value;
            }
        }

        /// <summary>
        /// Gets the size limit of usable memory for this memory module in bytes.
        /// </summary>
        public virtual long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
            protected set
            {
                this.sizeUsageLimit = value;
            }
        }

        #region ITransformable<MemorySystemInfo> Members

        MemorySystemInfo ITransformable<MemorySystemInfo>.Transform()
        {
            return new MemorySystemInfo(this.SizeTotal);
        }

        #endregion

        #region ITransformable<MemoryPerformanceInfo> Members

        MemoryPerformanceInfo ITransformable<MemoryPerformanceInfo>.Transform()
        {
            return new MemoryPerformanceInfo(this.SizeTotalFree, this.SizeUsageCurrent);
        }

        #endregion

        #region ITransformable<MemoryLimitInfo> Members

        MemoryLimitInfo ITransformable<MemoryLimitInfo>.Transform()
        {
            return new MemoryLimitInfo(this.SizeUsageLimit);
        }

        #endregion

        #region IUpdatable<MemorySystemInfo> Members

        bool IUpdatable<MemorySystemInfo>.CanUpdate(MemorySystemInfo info) //todoLater: need some way to identify the memory infos (note: only one memory info per executor so not that important)
        {
            return (info != null); 
        }

        void IUpdatable<MemorySystemInfo>.Update(MemorySystemInfo info)
        {
            if ((this as IUpdatable<MemorySystemInfo>).CanUpdate(info))
            {
                LoadFrom(info);
            }
        }

        #endregion
    }
}
