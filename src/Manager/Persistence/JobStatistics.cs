using System;
using System.Collections.Generic;
using System.Text;


namespace Utilify.Platform.Manager.Persistence
{
    //todoDoc, todoTest: (JobStatistics)

    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public struct JobStatistics : IEnumerable<KeyValuePair<JobStatus, long>>
    {
        private bool allowNegativeNumbers;
        private Dictionary<JobStatus, long> values;
        
        public JobStatistics(bool allowNegativeNumbers)
        {
            this.allowNegativeNumbers = allowNegativeNumbers;
            this.activeJobs = 0;
            this.values = new Dictionary<JobStatus, long>();
        }

        private void EnsureInit()
        {
            if (values != null)
                return;

            this.values = new Dictionary<JobStatus, long>();
            //initialise the dictionary with all the values in the enum
            Array possibleValuesForKey = Enum.GetValues(typeof(JobStatus));
            if (possibleValuesForKey != null)
            {
                for (int i = 0; i < possibleValuesForKey.Length; i++)
                {
                    JobStatus key = (JobStatus)possibleValuesForKey.GetValue(i);
                    values[key] = 0;
                }
            }
        }

        /// <summary>
        /// Gets the value for the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long this[JobStatus key]
        {
            get
            {
                EnsureInit();

                return values[key];
            }
            private set
            {
                EnsureInit();

                if (!allowNegativeNumbers && value < 0)
                    throw new ArgumentOutOfRangeException("value", value, "Value cannot be negative");

                values[key] = value;
            }
        }

        #region IEnumerable<KeyValuePair<JobStatus,long>> Members

        IEnumerator<KeyValuePair<JobStatus, long>> IEnumerable<KeyValuePair<JobStatus, long>>.GetEnumerator()
        {
            EnsureInit();

            IEnumerable<KeyValuePair<JobStatus, long>> ienum = values as IEnumerable<KeyValuePair<JobStatus, long>>;
            return ienum.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            EnsureInit();

            System.Collections.IEnumerable ienum = values as System.Collections.IEnumerable;
            return ienum.GetEnumerator();
        }

        #endregion

        #region Members for supporting persistence mapping

        //need a seperate field, since we don't have an 'active' status: it is really a status-group.
        private long activeJobs; //mapped directly as a field.
        /// <summary>
        /// Gets the active jobs is the sum of Scheduled, executing, completing jobs on an executor
        /// </summary>
        public long ActiveJobs
        {
            get { return activeJobs; }
            private set { activeJobs = value; }
        }

        public long SubmittedJobs
        {
            get { return this[JobStatus.Submitted]; }
            private set { this[JobStatus.Submitted] = value; }
        }
        public long ReadyJobs
        {
            get { return this[JobStatus.Ready]; }
            private set { this[JobStatus.Ready] = value; }
        }
        public long ScheduledJobs
        {
            get { return this[JobStatus.Scheduled]; }
            private set { this[JobStatus.Scheduled] = value; }
        }
        public long ExecutingJobs
        {
            get { return this[JobStatus.Executing]; }
            private set { this[JobStatus.Executing] = value; }
        }
        public long CompletingJobs
        {
            get { return this[JobStatus.Completing]; }
            private set { this[JobStatus.Completing] = value; }
        }
        public long CompletedJobs
        {
            get { return this[JobStatus.Completed]; }
            private set { this[JobStatus.Completed] = value; }
        }
        public long CancelledJobs
        {
            get { return this[JobStatus.Cancelled]; }
            private set { this[JobStatus.Cancelled] = value; }
        }

        #endregion
    }
}
