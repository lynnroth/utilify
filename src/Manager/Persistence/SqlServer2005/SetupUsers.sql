Use UtilifyPlatform;

Begin Transaction

Delete From Usr;
Delete From Grp_Permission;
Delete From Grp;
Delete From Permission;

Set Identity_Insert Grp On;
Insert Grp (grpId, [name]) Values (1, 'Administrators');
Insert Grp (grpId, [name]) Values (2, 'Users');
Insert Grp (grpId, [name]) Values (3, 'Executors');
Set Identity_Insert Grp Off;

-- admin, admin
Insert Usr ([username], [password], grpId, windowsUsername) Values ('admin', '19a2854144b63a8f7617a6f225019b12', 1, Host_Name() + '\Administrator');
-- user, user
Insert Usr ([username], [password], grpId) Values ('user', '9ce4b5879f3fcb5a9842547bebe191e1', 2);
-- executor, executor
Insert Usr ([username], [password], grpId) Values ('executor', '63c2867ae3b5ea1dccf158f6b084a9ec', 3);

Insert Permission(permissionId, [name], [description]) Values (1, 'ExecuteJob', 'Execute a job');
Insert Permission(permissionId, [name], [description]) Values (2, 'ManageApplications', 'Submit, monitor and manage own application');
Insert Permission(permissionId, [name], [description]) Values (3, 'ManageAllApplications', 'Submit, monitor and manage any application');
Insert Permission(permissionId, [name], [description]) Values (4, 'ManageUsers', 'Manager users');
Insert Permission(permissionId, [name], [description]) Values (5, 'ManageExecutors', 'Manager executors');

--set up admin permissions
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (1, 1, 0)
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (1, 3, 1)
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (1, 4, 2)
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (1, 5, 3)

--set up user permissions
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (2, 2, 0)

--set up executor permissions
Insert Grp_Permission (grpId, permissionId, arrayIndexColumn) Values (3, 1, 0)

DBCC CHECKIDENT ('Grp', RESEED, 2)

Commit Transaction

--select * from Usr
--select * from Grp
--select * from Grp_Permission
--select * from Permission