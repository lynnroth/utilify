using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using NHibernate;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    //we manage multiple threads updating an application's status : 
    //using NHibernate's automatic versioning using a 'version' column

    internal static class ApplicationDao
    {
        private static Logger logger = new Logger();

        #region Application related methods

        internal static Guid SaveApplication(Application application)
        {
            if (application == null)
                throw new ArgumentNullException("application");

            DataStore ds = DataStore.GetInstance();

            ISession session = null;
            ITransaction tr = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.RepeatableRead);

                if (application.Version == DataStore.UnsavedVersionValue) //this is a new app : attach deps
                {
                    //the dependencies need to be checked/saved first. since they are top-level objects too.
                    IList<Dependency> dependencies = application.Dependencies;
                    if (dependencies != null)
                    {
                        for (int i = 0; i < dependencies.Count; i++)
                        {
                            Dependency dep = dependencies[i];

                            //logger.Debug("Before loading dep = {0} ", dependencies[i]);

                            //do this loading only for dependencies which don't already have an id
                            //- meaning they are probably not already saved
                            if (dep != null && dep.Id == Guid.Empty)
                                dependencies[i] = LoadDependencyIfSaved(session, dep);

                            //logger.Debug("After loading dep = {0} ", dependencies[i]);
                        }
                    }
                }

                session.SaveOrUpdate(application);
                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early dispose in case of exception
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error saving application: " + application, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session); //this does the null-check and ignores it
                session = null;
            }

            return application.Id;
        }

        internal static bool ApplicationExists(Guid applicationId)
        {
            //let's not throw an exception : we know this won't exist : so just say it isn't there!
            if (applicationId == Guid.Empty)
                return false;

            DataStore ds = DataStore.GetInstance();
            bool exists = ds.ObjectExists(typeof(Application), applicationId);

            return exists;
        }

        //Issue 022: change the arrays in the persistent classes to use ILists, so that they can be optionally lazy loaded
        //and then this method below becomes un-necessary - we just have a GetStatusInfo in the Application class like we have in the Job class.
        //special case: have to work with the client class, so we can do lesser # of queries to get the app status
        internal static Utilify.Platform.ApplicationStatusInfo GetApplicationStatus(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            if (!ApplicationExists(applicationId))
                throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId.ToString());

            Utilify.Platform.ApplicationStatusInfo status = null;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);
                IQuery query = session.CreateQuery("Select app.Id, app.CreationTimeTicks, app.status From Application app Where app.Id = :id");
                query.SetGuid("id", applicationId);
                IList results = query.List();
                //results is an object[]
                if (results != null && results.Count == 1)
                {
                    object[] result = (object[])results[0];

                    long totalJobs = session.CreateQuery("Select count(*) From Job job Where job.Application.Id = :id")
                                            .SetGuid("id", applicationId)
                                            .UniqueResult<long>();

                    long completedJobs = session.CreateQuery("Select count(*) From Job job Where job.Application.Id = :id And job.status = :status")
                                                .SetGuid("id", applicationId)
                                                .SetParameter("status", JobStatus.Completed)
                                                .UniqueResult<long>();

                    //we expect 3 elements in the result array: id, creationTime, status
                    long creationTimeTicks = (long)result[1];
                    DateTime creationTime = new DateTime(creationTimeTicks, DateTimeKind.Utc);
                    status = new Utilify.Platform.ApplicationStatusInfo(((Guid)result[0]).ToString(), creationTime,
                        (ApplicationStatus)result[2], completedJobs, totalJobs);
                }
                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting status for application: " + applicationId, hx);
            }
            finally
            {
                //double check
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return status;
        }

        internal static Application GetApplication(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            //let us not do the ApplicationExists checks here - let the caller do that.
            //if the app does not exist, we should get back a null reference
            DataStore ds = DataStore.GetInstance();
            Application app = ds.Get<Application>(applicationId);
            return app;
        }

        //UpdateQos is not used anywhere in the product code at the moment (kna, 30 Sep 08)
        //Issue 023: Simplify the Qos mapping. make it a struct. delay load it. and load app, update qos and save it back
        //Issue 024: Implement this as a feature throughout the system.
        //Issue 025: Need to restrict what can be updated: only budget (increased), and deadline (increased).
        internal static void UpdateApplicationStatus(Guid applicationId, ApplicationStatus newStatus) //todoTest
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application id cannot be empty", "applicationId");

            //logger.Debug("Thread '{0}' entered UpdateApplicationStatus", Thread.CurrentThread.Name);

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.RepeatableRead);
                //logger.Debug("Thread '{0}' began transaction in UpdateApplicationStatus", Thread.CurrentThread.Name);

                Application app = session.Get<Application>(applicationId);

                if (app == null)
                    throw new ApplicationNotFoundException("Could not find application " + applicationId.ToString(), applicationId.ToString());

                app.Status = newStatus;

                //logger.Debug("Thread '{0}' about to saveOrUpdate", Thread.CurrentThread.Name);

                session.SaveOrUpdate(app);

                tr.Commit();
                //logger.Debug("Thread '{0}' committed after saveOrUpdate", Thread.CurrentThread.Name);
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating status for application: " + applicationId, ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

        }

        /// <summary>
        /// Gets the entire list of exectors - and their static properties.
        /// </summary>
        /// <returns></returns>
        internal static List<ApplicationSubmissionInfo> GetApplications()
        {
            DataStore ds = DataStore.GetInstance();
            return ds.GetListAs<Application, ApplicationSubmissionInfo>(IsolationLevel.ReadCommitted);
        }

        #endregion

        #region Job related methods
        //saving 1 job at a time seems to be 5 times faster than saving a bunch of jobs! why?
        internal static Guid SaveNewJob(Guid applicationId, Job job)
        {
            if (job == null)
                throw new ArgumentNullException("job");

            List<Job> jobs = new List<Job>();
            jobs.Add(job);
            return SaveNewJobs(applicationId, jobs)[0];
        }

        internal static List<Guid> SaveNewJobs(Guid applicationId, List<Job> jobs)
        {
            if (jobs == null)
                throw new ArgumentNullException("jobs");

            foreach (Job job in jobs)
            {
                if (job == null)
                    throw new ArgumentException("Elemebnts of the job list cannot be null", "jobs");
            }

            if (applicationId == Guid.Empty)
                throw new ArgumentException("The application id for the job cannot be empty", "applicationId");

            List<Guid> ids = new List<Guid>();
            if (jobs.Count == 0)
                return ids;

            DataStore ds = DataStore.GetInstance();

            ISession session = null;
            ITransaction tr = null;
            try
            {
                //since the app is not used for anything else other that its id,
                //we won't actually get an ObjectNotFoundException here:
                //we're likely to get a key constraint violation when the commit happens.
                //so we better just check whether the app exists:
                //we can take the hit for the extra query here, since SaveNewJobs is not a high-frequency call.
                 if (!ds.ObjectExists(typeof(Application), applicationId))
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId.ToString());

                session = ds.OpenSession();
                tr = session.BeginTransaction();

                Application app = session.Load<Application>(applicationId);
                
                //to avoid blocking here for too long, on a huge list of jobs,
                //and to allow other threads to access the database in the meanwhile,
                //we need to break it down into smaller transactions of say 'n' records each.
                int transactionRowSize = 20; //magic!
                int currentJob = 0;
                foreach (Job job in jobs)
                {
                    currentJob++;
                    //logger.Debug("Saving new Job: " + currentJob);
                    //attach the application object to the job
                    job.Application = app;
                    //the dependencies need to be checked/saved first. since they are top-level objects too.
                    IList<Dependency> dependencies = job.Dependencies;
                    if (dependencies != null)
                    {
                        for (int i = 0; i < dependencies.Count; i++)
                        {
                            Dependency dep = dependencies[i];

                            //logger.Debug("Before loading dep = {0} ", dependencies[i]);

                            //do this loading only for dependencies which don't already have an id
                            //- meaning they are probably not already saved
                            if (dep != null && dep.Id == Guid.Empty)
                                dependencies[i] = LoadDependencyIfSaved(session, dep);

                            //logger.Debug("After loading dep = {0} ", dependencies[i]);
                        }
                    }
                    //no need to do the same for the Results
                    session.SaveOrUpdate(job);

                    if (currentJob >= transactionRowSize)
                    {
                        //commit and start another transaction
                        currentJob = 0; //reset
                        tr.Commit();
                        //logger.Debug("Committing and starting a new transaction...");
                        //Thread.Sleep(100); //more magic : sleep for a small bit to allow others to do their stuff
                        ds.CloseTransaction(tr);
                        tr = session.BeginTransaction();
                    }

                    ids.Add(job.Id);
                }

                tr.Commit();
            }            
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error saving jobs for app: " + applicationId, hx);
                
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session); //this does the null-check and ignores it
                session = null;
            }

            return ids;
        }

        internal static bool JobExists(Guid jobId)
        {
            //instead of throwing an exception, let's just return false
            if (jobId == Guid.Empty)
                return false;

            DataStore ds = DataStore.GetInstance();
            bool exists = ds.ObjectExists(typeof(Job), jobId);

            return exists;
        }

        internal static Job GetJob(Guid jobId, FetchStrategy strategy)
        {
            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            Job job = null;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = (strategy == FetchStrategy.Eager) ?
                    "From Job job Left Join Fetch job.jobInstance Where job.Id = :id" :
                    "From Job job Where job.Id = :id";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", jobId);
                job = query.UniqueResult<Job>();

                if (strategy == FetchStrategy.Eager)
                {
                    //initialize other stuff, like the dependency collection, results, execution host
                    NHibernateUtil.Initialize(job.ExecutionHost);
                    job.GetJobException();
                    job.GetResults();
                    job.GetDependencies();
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

            return job;
        }

        internal static Job GetJob(Guid jobId)
        {
            return GetJob(jobId, FetchStrategy.Default);
        }

        internal static List<Job> GetJobs(IEnumerable<Guid> jobGuids, FetchStrategy strategy)
        {
            if (jobGuids == null)
                throw new ArgumentNullException("jobGuids");

            List<Job> jobs = new List<Job>();
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = (strategy == FetchStrategy.Eager) ?
                    "From Job job Left Join Fetch job.jobInstance Where job.Id = :id" :
                    "From Job job Where job.Id = :id";

                //todoImprove (ApplicationDao.GetJobs(...))
                IQuery query = session.CreateQuery(hql);
                foreach (Guid id in jobGuids)
                {
                    //logger.Debug("Querying job with id: {0}", id);
                    query.SetGuid("id", id);
                    IList<Job> results = query.List<Job>();
                    if (results != null && results.Count > 0 && results[0] != null)
                        jobs.Add(results[0]); //we should only really get one object here
                    
                    NHibernateUtil.Initialize(results[0].ExecutionHost);
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return jobs;
        }

        internal static IList<JobSubmissionInfo> GetJobsForExecutor(Guid executorId)
        {
            if (executorId == Guid.Empty)
                throw new ArgumentException("Executor Id cannot be empty", "executorId");

            IList<JobSubmissionInfo> infos;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                //we can use 'fetch' on only one proxied object/collection
                string hql = @"Select mapping.Job From JobExecutionMapping mapping 
                                    Left Join Fetch mapping.Job.jobInstance 
                                Where mapping.Executor.id = :id And mapping.Job.status = :jobStatus";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", executorId);
                query.SetParameter("jobStatus", JobStatus.Scheduled);
                IList<Job> jobs = query.List<Job>();

                infos = DataStore.Transform<Job, JobSubmissionInfo>(jobs);
                //we'll have to initialise the proxies for the other ones here
                tr.Commit(); //let's commit even though we are only reading - to signal the end of the transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

            if (infos == null)
                infos = new List<JobSubmissionInfo>();

            return infos;
        }

        internal static IList<Guid> GetExecutingJobIds(Guid executorId)
        {
            if (executorId == Guid.Empty)
                throw new ArgumentException("Executor Id cannot be empty", "executorId");

            IList<Guid> jobIds;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = @"Select mapping.Job.Id From JobExecutionMapping mapping 
                                    Where mapping.Job.status = :executing Or 
                                        mapping.Job.status = :completing And 
                                        mapping.Executor.Id = :id ";

                IQuery query = session.CreateQuery(hql);
                query.SetEnum("executing", JobStatus.Executing);
                query.SetEnum("completing", JobStatus.Completing);
                query.SetGuid("id", executorId);
                jobIds = query.List<Guid>();

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return jobIds;
        }

        internal static IList<Job> AbortJobs(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application Id cannot be empty", "applicationId");

            IList<Job> jobs;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                string hql =
                    @"From Job job Where job.Application.id = :id 
                         And job.status != :completed And job.status != :cancelled ";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", applicationId);
                query.SetEnum("completed", JobStatus.Completed);
                query.SetEnum("cancelled", JobStatus.Cancelled);
                jobs = query.List<Job>();
                foreach (Job job in jobs)
                {
                    job.Status = JobStatus.Cancelled;
                    session.SaveOrUpdate(job);
                }
                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

            return jobs;
        }

        internal static IList<Job> GetJobsForApplication(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application Id cannot be empty", "applicationId");

            IList<Job> jobs;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = "From Job job Left Join Fetch job.ExecutionHost Where job.Application.id = :id";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", applicationId);
                jobs = query.List<Job>();

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return jobs;
        }

        internal static IList<Job> GetJobsForApplicationByStatus(Guid applicationId, JobStatus status)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application Id cannot be empty", "applicationId");

            IList<Job> jobs;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql =
                    "From Job job Where job.status = :status And job.Application.id = :id ";

                IQuery query = session.CreateQuery(hql);
                query.SetEnum("status", status);
                query.SetGuid("id", applicationId);
                jobs = query.List<Job>();

                tr.Commit(); //commit to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return jobs;
        }

        internal static void UpdateJobStatus(Guid jobId, JobStatus newStatus) //todoTest
        {
            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                Job job = session.Get<Job>(jobId);
                if (job == null)
                    throw new JobNotFoundException("Could not find job " + jobId.ToString(), jobId.ToString());

                job.Status = newStatus;

                session.SaveOrUpdate(job);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating status for job: " + jobId, ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
        }

        internal static void SetJobStarted(Guid jobId) //todoTest
        {

            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.RepeatableRead);

                Job job = session.Get<Job>(jobId);
                if (job == null)
                    throw new JobNotFoundException("Could not find job " + jobId.ToString(), jobId.ToString());

                // Update Job Start time and Status.
                job.Status = JobStatus.Executing;
                job.StartTime = DateTime.UtcNow;

                session.SaveOrUpdate(job);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating status for job: " + jobId, ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

        }

        internal static void SetJobCompleting(Guid jobId, JobCompletionInfo info) //todoTest
        {
            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                Job job = session.Get<Job>(jobId);
                if (job == null)
                    throw new JobNotFoundException("Could not find job " + jobId.ToString(), jobId.ToString());

                job.Status = JobStatus.Completing;
                job.FinishTime = DateTime.UtcNow;
                job.CpuTime = info.CpuTime;
                job.ExecutionLog = info.ExecutionLog;
                job.ExecutionError = info.ExecutionError;
                job.UpdateJobInstance(info.GetJobInstance());

                var ex = info.GetJobException();
                if (ex != null)
                    job.UpdateJobException(ex);

                session.SaveOrUpdate(job);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating status for job: " + jobId, ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;

            }

        }

        internal static void AbortJob(Guid jobId) //todoTest
        {

            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.RepeatableRead);

                Job job = session.Get<Job>(jobId);
                if (job == null)
                    throw new JobNotFoundException("Could not find job " + jobId.ToString(), jobId.ToString());

                job.Status = JobStatus.Cancelled;

                session.SaveOrUpdate(job);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating status for job: " + jobId, ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

        }

        #endregion

        #region Dependency related methods

        /// <summary>
        /// Checks if the given dependency object is already saved, and loads it from the db, if so.
        /// If not, the given object is return as is.
        /// We pass in the ISession instance, to avoid the extra overheads of creating/disposing connections,
        /// since we sometimes call this method in a loop.
        /// It is very IMP here that we DONT touch/load into memory the binary content of the dependency.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="templateDep"></param>
        /// <returns></returns>
        internal static Dependency LoadDependencyIfSaved(ISession session, Dependency templateDep)
        {
            if (templateDep == null)
                return null; //nothing we can do

            if (templateDep.Id != Guid.Empty)
                return templateDep; //no need to load it : this instance is already persistent

            IQuery query = session.CreateQuery(
                @"From Dependency dep 
                    Where dep.Name = :name 
                    And dep.FileName = :filename 
                    And dep.Hash = :hash 
                    And dep.Size = :size 
                    And dep.Type = :type 
                    And dep.ModifiedTicks = :modified"
            );
            query.SetString("name", templateDep.Name);
            query.SetString("filename", templateDep.FileName);
            query.SetString("hash", templateDep.Hash);
            query.SetInt64("size", templateDep.Size);
            query.SetParameter("type", templateDep.Type);
            query.SetInt64("modified", templateDep.Modified.Ticks);

            query.SetCacheable(true);
            IList<Dependency> results = query.List<Dependency>();

            Dependency saved = null;
            if (results != null && results.Count == 1)
                saved = results[0];
            
            logger.Debug("Searched for dep {0}, found {1} results. Loaded = {2}", 
                templateDep, results.Count, (saved != null));

            if (saved == null)
                return templateDep; //if we did not find it in the db, return what we got
            else
                return saved; //if we found it in the db, return the db instance
        }

        //Issue 026: all these "Exists" checks are done twice : once in AppManager, again in AppDao
        //is this needed? can we trust that the callers will always pass in an id which has been tested for existence?

        //since we don't want to pull out the entire contents of the dependency each time,
        //we'll lazy load the dependency contents (i.e binary data)
        internal static List<Dependency> GetUnresolvedDependencies(Guid id, DependencyScope scope)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("The application or job id cannot be empty", "id");

            //left joining on app/job may return 1 null element with such a query if there is a left join.
            //changed to avoid left joins on dependency: we only want dependencies if they exist
            string hql = @"Select dep From {0} as parent join 
                                parent.dependencies as dep 
                            Where dep.content is null 
                            And parent.Id = :appOrJobId 
                            And dep.Type <> :dependencyType ";

            if (scope == DependencyScope.Application)
            {
                if (!ApplicationExists(id))
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, id.ToString());

                hql = string.Format(hql, "Application");
            }
            else if (scope == DependencyScope.Job)
            {
                if (!JobExists(id))
                    throw new JobNotFoundException(Messages.JobNotFound, id.ToString());

                hql = string.Format(hql, "Job");
            }

            List<Dependency> dependencies = new List<Dependency>();
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff
                IQuery query = session.CreateQuery(hql);
                query.SetGuid("appOrJobId", id);
                query.SetInt32("dependencyType", (int)DependencyType.RemoteUrl);
                IList<Dependency> results = query.List<Dependency>();
                if (results != null)
                    dependencies.AddRange(results);

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting unresolved dependencies", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            return dependencies;
        }

        //special case: its just more convenient to use the client-side DTO here, instead of too many params
        //1. Dep contents are lazy-fetched - which should be ok, since we actually 'set' them here :)
        //2. Issue 020: relevance and security of content to be ensured by linking that appropriate users request read/write of dependencies.
        internal static void SaveDependencyContent(Utilify.Platform.DependencyContent content)
        {
            if (content == null)
                throw new ArgumentNullException("content");

            //we expect the caller to make sure the dependency actually exists!
            byte[] obj = content.GetContent();
            if (obj == null || obj.Length == 0)
                throw new ArgumentException("Content cannot be empty");

            Guid depId = Helper.GetGuidSafe(content.DependencyId);
            if (depId == Guid.Empty)
                throw new ArgumentException(
                    string.Format(Messages.NullEmptyOrInvalidFormat, "dependency id", content.DependencyId), "content.DependencyId"
                    );

            DataStore ds = DataStore.GetInstance();
            Dependency dep = ds.Get<Dependency>(depId); //no need to eager load here, since we are SETTING the contents not getting them
            if (dep.IsResolved)
                throw new InvalidOperationException("The dependency " + dep.Id + " has already been resolved. Updating resolved dependencies is not permitted.");

            //logger.Debug("Pulled dep from db with id : {0}. Dep = {1}", depId, dep);
            dep.SetContent(content.GetContent());
            ds.Save(dep);
        }

        internal static Dependency GetDependency(Guid dependencyId, FetchStrategy strategy)
        {
            if (dependencyId == Guid.Empty)
                throw new ArgumentException("Dependency id cannot be empty", "dependencyId");

            Dependency dep = null;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = (strategy == FetchStrategy.Eager) ?
                    "From Dependency dep Left Join Fetch dep.content Where dep.id = :id" :
                    "From Dependency dep Where dep.id = :id";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", dependencyId);
                dep = query.UniqueResult<Dependency>();

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return dep;
        }

        #endregion

        #region Result related methods

        internal static List<Result> GetResults(Guid jobId)
        {
            if (jobId == Guid.Empty)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            if (!JobExists(jobId))
                throw new JobNotFoundException(Messages.JobNotFound, jobId.ToString());

            List<Result> results = new List<Result>();

            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);
                string hql = "From Result result Where result.JobId = :jobId";
                IQuery query = session.CreateQuery(hql);
                query.SetGuid("jobId", jobId);
                IList<Result> list = query.List<Result>();
                if (list != null)
                    results.AddRange(list);

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting results for job: " + jobId, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            return results;
        }

        internal static bool ResultExists(Guid resultId)
        {
            //let's not throw an exception : we know this won't exist : so just say it isn't there!
            if (resultId == Guid.Empty)
                return false;

            DataStore ds = DataStore.GetInstance();
            bool exists = ds.ObjectExists(typeof(Result), resultId);
            return exists;
        }

        internal static Result GetResult(Guid resultId, FetchStrategy strategy)
        {
            if (resultId == Guid.Empty)
                throw new ArgumentException("Result id cannot be empty", "resultId");

            Result res = null;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = (strategy == FetchStrategy.Eager) ?
                    "From Result res Left Join Fetch res.content Where res.id = :id" :
                    "From Result res Where res.id = :id";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", resultId);
                res = query.UniqueResult<Result>();
                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting result", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return res;
        }

        internal static void SaveResultContent(Utilify.Platform.ResultContent content) //todoTest
        {
            if (content == null)
                throw new ArgumentNullException("content");

            byte[] obj = content.GetContent();
            if (obj == null)// || obj.Length == 0) //Zero length results files are now allowed
                throw new ArgumentException("Content cannot be empty");

            Guid resId = Helper.GetGuidSafe(content.ResultId);
            if (resId == Guid.Empty)
                throw new ArgumentException(
                    string.Format(Messages.NullEmptyOrInvalidFormat, "result id", content.ResultId), "content.ResultId"
                    );
            if (!ApplicationDao.ResultExists(resId))
                throw new ResultNotFoundException(Messages.ResultNotFound, content.ResultId);

            DataStore ds = DataStore.GetInstance();
            Result res = ds.Get<Result>(resId); //no need to eager load here, since we are SETTING the contents not getting them
            
            //we need to make results behave the same way regardless of whether they are stored in db / file
            //for now let us just allow overwriting the result
            //if (res.HasContent)
            //    throw new InvalidOperationException("The result " + res.Id + " has already been sent. Overwriting existing result content is not permitted.");

            res.SetContent(content.GetContent());
            ds.Save(res);

        }

        #endregion

        internal static List<ApplicationSubmissionInfo> GetApplications(string username)
        {
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            List<ApplicationSubmissionInfo> applications = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = "From Application app Where app.Username = :username ";

                IQuery query = session.CreateQuery(hql);
                query.SetString("username", username);
                IList<Application> results = query.List<Application>();

                applications = 
                    DataStore.Transform<Application, ApplicationSubmissionInfo>(results);

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting applications for user: " + username, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            if (applications == null)
                applications = new List<ApplicationSubmissionInfo>();

            return applications;
        }

        /// <summary>
        /// Unmaps cancelled jobs for an executor and returns the list of job ids that need to be killed on the Executor.
        /// </summary>
        /// <param name="executorId"></param>
        internal static List<string> UnmapCancelledJobs(Guid executorId) //todoTest 
        {
            if (executorId == Guid.Empty)
                throw new ArgumentException("executorId");

            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();

            List<string> jobIdsToCancel = new List<string>();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                IQuery qry = session.CreateQuery("Select mapping.Job.Id From JobExecutionMapping mapping " +
                    " Where mapping.Job.status In (:statuses)" +
                    " And mapping.Executor.Id = :executorId");
                //we need to pull out both Cancelled and Reset jobs
                //This is because jobs can be reset manually via the dashboard, to force the job to be mapped to 
                //another Executor.
                //The mapping is left there for the old Executor to pick up cancelled jobs to abort then on its side.
                //This method, UnmapCancelled jobs then deletes those mappings.
                qry.SetParameterList("statuses",
                    new JobStatus[] { JobStatus.Cancelled, JobStatus.Ready }
                );
                qry.SetGuid("executorId", executorId);

                List<Guid> jobIds = new List<Guid>();
                jobIds.AddRange(qry.List<Guid>());

                if (jobIds.Count > 0)
                {
                    foreach (Guid id in jobIds)
                    {
                        jobIdsToCancel.Add(id.ToString());
                    }

                    ArrayList deleteParams = new ArrayList();
                    deleteParams.AddRange(jobIds);
                    deleteParams.Add(executorId);
                    NHibernate.Type.IType[] types = new NHibernate.Type.IType[deleteParams.Count];
                    for (int i = 0; i < types.Length; i++)
                    {
                        types[i] = NHibernateUtil.Guid;
                    }
                    string deleteHql = "From JobExecutionMapping mapping " +
                        " Where mapping.Job.Id In (?)" +
                        " And mapping.Executor.Id = ?";
                    session.Delete(deleteHql, deleteParams.ToArray(), types);
                }

                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error upmapping jobs for executor: " + executorId, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return jobIdsToCancel;
        }

        // Updated: This is now called when the Job is finished at an Executor (done, aborted, reset, not picked up)
        /// <summary>
        /// Unmaps the job mapping : i.e deletes the mapping for a job, and updates the status.
        /// </summary>
        internal static void UnmapJob(Guid jobId, JobStatus status) //todoTest, check for null values etc
        {

            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();

            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                Job job = session.Get<Job>(jobId);

                if (job == null)
                    throw new ArgumentException(string.Format("{0} for id {1}", Messages.JobNotFound, jobId));

                session.Delete("From JobExecutionMapping mapping Where mapping.Job.Id = ?",
                    jobId, NHibernateUtil.Guid);

                job.Status = status;
                if (job.Status != JobStatus.Completed
                    && job.Status != JobStatus.Cancelled)
                {
                    job.ExecutionHost = null;
                }
                session.SaveOrUpdate(job);

                tr.Commit();
            }
            catch (NHibernate.HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error upmapping job: " + jobId, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session); //this does the null-check and ignores it
                session = null;
            }

        }

        internal static ApplicationSubmissionInfo GetApplicationSubmissionInfo(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException(Messages.ArgumentEmpty, "applicationId");

            DataStore ds = DataStore.GetInstance();
            return ds.GetAs<Application, ApplicationSubmissionInfo>(applicationId);
        }

        internal static List<JobInfo> GetJobInfo(Guid applicationId)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException(Messages.ArgumentEmpty, "applicationId");

            List<JobInfo> infos = null;

            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();

            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                string hql = "From Job job Left Join Fetch job.ExecutionHost Where job.Application.id = :id";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("id", applicationId);
                IList<Job> jobs = query.List<Job>();

                infos = DataStore.Transform<Job, JobInfo>(jobs);

                tr.Commit();
            }
            catch (NHibernate.HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting job info", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session); //this does the null-check and ignores it
                session = null;
            }

            if (infos == null)
                infos = new List<JobInfo>();

            return infos;
        }
    }
}
