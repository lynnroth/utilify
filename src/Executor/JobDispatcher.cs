using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Utilify.Platform.Execution.Shared;

namespace Utilify.Platform.Executor
{
    //Methods of this class are meant to be single-threaded.
    internal class JobDispatcher : IDisposable
    {
        private Logger logger = new Logger();
        private const string LocalMachine = "127.0.0.1";
        private const int MaxPendingConnections = 1;

        private Dictionary<ClientPlatform, ExecutionClientInfo> clientInfos;
        private TcpListener listener;
        private int port = 50210; //MAGIC

        private const int MaxRetries = 3;

        public JobDispatcher(int port)
        {
            clientInfos = new Dictionary<ClientPlatform, ExecutionClientInfo>();
            //todoDiscuss, todoPerf: check if we need to start the listener here. perhaps we could delay it till a child-process is actually started!?
            listener = new TcpListener(IPAddress.Parse(LocalMachine), port);
            listener.Start(MaxPendingConnections); //we only want ONE pending connection at the max
            this.port = port;
            logger.Debug("JobDispatcher successfully started on port " + port);
        }

        public void AbortJob(Job job)
        {
            VerifyDisposed();

            ValidationHelper.CheckNull(job, "job");

            lock (this)
            {
                logger.Debug("Aborting Job...");
                TcpClient client = GetClient(job.ClientPlatform);
                if (client == null)
                {
                    RemoveClient(job.ClientPlatform);
                    return;
                }

                NetworkStream ns = client.GetStream();
                IMessageData msgData = new AbortJobMessageData(job.JobId);
                logger.Debug("Got client for jobType {0}, Sending abortjob msg", job.JobType);                
                MessageDataHelper.SendMessage(ns, msgData);
            }
        }

        public void ExecuteJob(string jobId, string applicationId, string jobDirectory, string applicationDirectory, 
            JobType type, ClientPlatform clientPlatform, byte[] jobInstance)
        {
            VerifyDisposed();

            ValidationHelper.CheckNull(jobId, "jobId");
            ValidationHelper.CheckNull(applicationId, "applicationId");
            ValidationHelper.CheckNull(jobDirectory, "jobDirectory");
            ValidationHelper.CheckNull(applicationDirectory, "applicationDirectory");
            ValidationHelper.CheckNull(jobInstance, "jobInstance");

            lock (this)
            {
                int count = 1;
                bool success = false;
                while (count < MaxRetries && !success)
                {
                    try
                    {
                        logger.Debug("Getting connection client to execution engine.");
                        TcpClient client = GetClient(clientPlatform, true);
                        if (client != null)
                        {
                            NetworkStream ns = client.GetStream();

                            logger.Debug("Creating execute message.");
                            IMessageData data = new ExecuteJobMessageData(jobId,
                                applicationId, jobDirectory, applicationDirectory, type, jobInstance);

                            logger.Debug("Attempting to Execute Job. {0} {1}", type, clientPlatform);
                            success = MessageDataHelper.SendMessage(ns, data);
                        }
                        else
                        {
                            logger.Debug("Could not communicate with child process. Trying again...");
                        }
                    }
                    catch (IOException)
                    {
                        //remove it so that in the next try, we restart the process for this client.
                        RemoveClient(clientPlatform);
                        
                        count++;
                        if (count >= MaxRetries)
                            throw;
                        else
                        {
                            logger.Debug("Could not communicate with child process. Retrying...");
                            Thread.Sleep(250);
                        }
                    }
                }
            }
        }

        public JobCompletionInfo[] GetCompletedJobs(ClientPlatform platform)
        {
            VerifyDisposed();

            lock (this)
            {
                List<JobCompletionInfo> completed = new List<JobCompletionInfo>();

                var client = GetClient(platform);
                //nothing else we can do if we can't contact the client process?
                if (client == null)
                {
                    logger.Debug("Could not communicate with the client process.");
                    
                    //this client is not contactable - remove it so that we start fresh next time.
                    RemoveClient(platform);

                    throw new PlatformException(string.Format("Lost connection to {0} client process.", platform));
                }

                NetworkStream stream = client.GetStream();

                logger.Debug("Sending GetCompletedJobs Message");

                MessageDataHelper.SendMessage(stream, new EmptyMessageData(MessageType.GetCompletedJobs));
                IMessageData msgData = MessageDataHelper.ReceiveMessage(stream);

                bool valid = valid = (msgData == null) ? false : msgData.IsValid();
                if (valid)
                {
                    JobCompletionMessageData jobCompletionData = msgData as JobCompletionMessageData;
                    if (jobCompletionData != null)
                    {
                        foreach (JobCompletionInfo info in jobCompletionData.GetJobs())
                        {
                            completed.Add(info);
                        }
                        logger.Debug("Got {0} jobs from Executor", jobCompletionData.GetJobs().Length);
                    }
                    else
                    {
                        valid = false;
                    }
                } //nacks are sent internally by the helper

                if (!valid)
                    throw new IOException("Received invalid job completion message data...");

                return completed.ToArray();
            }
        }

        /// <summary>
        /// Removes the reference to the client process, if it is present.
        /// </summary>
        /// <param name="platform"></param>
        private void RemoveClient(ClientPlatform platform)
        {
            lock (clientInfos)
            {
                if (clientInfos.ContainsKey(platform))
                    clientInfos.Remove(platform);
            }
        }

        private TcpClient GetClient(ClientPlatform clientPlatform)
        {
            if (disposed)
                return null;

            return GetClient(clientPlatform, false);
        }

        private TcpClient GetClient(ClientPlatform clientPlatform, bool startNewIfNeeded)
        {
            if (disposed)
                return null;

            ExecutionClientInfo clientInfo = null;

            if (startNewIfNeeded)
            {
                //start a process if the job type doesn't have its 'real-executor' process
                clientInfo = EnsureAlive(clientPlatform);
                if (clientInfo != null)
                {
                    lock (clientInfos)
                    {
                        clientInfos[clientPlatform] = clientInfo;
                    }
                    return clientInfo.Client;
                }
            }
            else
            {
                logger.Debug("Getting client process for ClientPlatform:{0}", clientPlatform);
                //get access to the existing clientInfo, if there is one.
                lock (clientInfos)
                {
                    clientInfos.TryGetValue(clientPlatform, out clientInfo);
                }
                if (clientInfo != null && CheckAlive(clientInfo).GetValueOrDefault())
                {
                    return clientInfo.Client;
                }
            }

            return null;
        }

        #region Process Management Functions

        /// <summary>
        /// Returns a tri-state boolean indicating whether the process is alive or if the process
        /// status could not be checked.
        /// True = Process is alive.
        /// False = Process is not alive.
        /// Null = Process status could not be checked.
        /// </summary>
        /// <param name="clientInfo"></param>
        /// <returns></returns>
        private bool? CheckAlive(ExecutionClientInfo clientInfo)
        {
            if (disposed)
                return null;

            bool? isAlive = null;
            try
            {
                //1. check if the child process is still alive.
                if (clientInfo != null && clientInfo.ChildProcess != null && clientInfo.Client != null)
                {
                    Process proc = clientInfo.ChildProcess;
                    proc.Refresh();
                    if (proc.Handle != IntPtr.Zero && !proc.HasExited && proc.Responding)
                    {
                        //2. check if the socket connection is still ok.
                        isAlive = clientInfo.Client.Connected;
                    }
                }
            }
            catch (InvalidOperationException ix)
            {
                logger.Warn("Error checking client process status: " + ix.GetType().Name + " - " + ix.Message);
            }
            catch (Exception ex)
            {
                logger.Warn("Error checking client process status: " + ex.GetType().Name + " - " + ex.Message);
            }
            finally
            {
                try
                {
                    if (isAlive == null && clientInfo != null && clientInfo.ChildProcess != null)
                    {
                        clientInfo.ChildProcess.Close();
                    }
                }
                catch { } //ignore errors during clean up
            }
            return isAlive;
        }

        private ExecutionClientInfo EnsureAlive(ClientPlatform platform)
        {
            if (disposed)
                return null;

            ExecutionClientInfo clientInfo = null;
            lock (clientInfos)
            {
                clientInfos.TryGetValue(platform, out clientInfo);
            }

            int count = 0;
            while (count < MaxRetries)
            {
                try
                {
                    if (clientInfo == null)
                    {
                        //start a new one.
                        clientInfo = StartProcess(platform);
                    }

                    var isAlive = CheckAlive(clientInfo);
                    if (isAlive.GetValueOrDefault())
                        return clientInfo;

                    if (isAlive != null && isAlive == false)
                    {
                        //try to stop the client process
                        //most likely we won't be able to send a shutdown message, but should
                        //be able to kill the process - which is fine.
                        TryStopClient(clientInfo);
                        clientInfo = null;
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn("Error trying to ensure the child process is alive. Retrying... - Error message: {0}",
                        ex.ToString());
                }
                finally
                {
                    count++;
                    Thread.Sleep(100); //sleep 100ms and try again
                }
            }

            return clientInfo;
        }

        /// <summary>
        /// Disposes of the resources used by the given clientInfo. (This includes killing the process, if it is still alive)
        /// </summary>
        /// <param name="clientInfo"></param>
        private void TryStopClient(ExecutionClientInfo clientInfo)
        {
            if (disposed)
                return;

            if (clientInfo.Client != null && clientInfo.Client.Connected)
            {
                try
                {
                    //send a 'shutdown' message.
                    NetworkStream ns = clientInfo.Client.GetStream();
                    MessageDataHelper.SendMessage(ns, new EmptyMessageData(MessageType.Shutdown));
                }
                catch (SocketException sx)
                {
                    logger.Debug("Ignoring error when closing socket connection: " + sx.ToString());
                }
                finally
                {
                    //disposes the TcpClient object without closing the underlying connection: that's all we can do here :/
                    clientInfo.Client.Close(); 
                }
            }
            Process proc = clientInfo.ChildProcess;
            try
            {
                if (proc != null)
                {
                    proc.Refresh();
                    if (proc.Handle != IntPtr.Zero && !proc.HasExited)
                    {
                        try
                        {                            
                            proc.WaitForExit(1000);
                            //if it still has not exited, kill it!
                            if (!proc.HasExited)
                                proc.Kill();
                        }
                        catch (SystemException sx)
                        {
                            logger.Debug("Ignoring error when stopping process: " + sx.ToString());
                        }
                    }
                }
            }
            catch (InvalidOperationException) { } //occurs when the process has already exited : and we can't get the handle!, ignore
            catch (System.ComponentModel.Win32Exception) { } //if we can't access the process, so can't stop it: ignore
            finally
            {
                if (proc != null)
                    proc.Close(); //dispose the process object
            }
        }
        
        private ProcessStartInfo GetStartInfo(ClientPlatform clientPlatform)
        {
            ProcessStartInfo psi = new ProcessStartInfo();

            //get proper executable based on job type
            string ext = (InfoProvider.OSInfoHelper.GetPlatform() == PlatformID.Unix) ? ".sh" : ".bat";
            string logDir = IOHelper.GetTempDirectory(true);
            string modulename = "";
            switch (clientPlatform)
            {
                case ClientPlatform.DotNet:
                    modulename = "net-executor";
                    break;
                case ClientPlatform.Java:
                    modulename = "java-executor";
                    break;
            }

            psi.FileName = modulename + ext;
            psi.Arguments = string.Format("{0} \"{1}\"", port, logDir);

            psi.WorkingDirectory = Environment.CurrentDirectory;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = true;
            psi.RedirectStandardOutput = false;
            psi.RedirectStandardError = false;

            logger.Debug("Process command : \n\t{0} {1}", psi.FileName, psi.Arguments);

            return psi;
        }

        private ExecutionClientInfo StartProcess(ClientPlatform clientPlatform)
        {
            //todoTest: Check if this is reliable enough.
            //if the process dies soon after it was started. we will know, since no one will connect back to us
            //and we check that

            //Start the client process
            Process proc = new Process();
            proc.EnableRaisingEvents = true;
            proc.StartInfo = GetStartInfo(clientPlatform);
            proc.Exited += new EventHandler(proc_Exited);
            proc.Start();
            proc.PriorityClass = ProcessPriorityClass.High;

            //wait for the new process to connect to this server:
            //we're not a multi-threaded server. we only need to keep a connection to this process which we've started.
            int timeout = 5000; //todo: magic timeout. should make it configurable (even if not exposed to the user)
            TcpClient client = null;
            int counter = 0;
            while (counter < timeout)
            {
                if (listener.Pending())
                {
                    client = listener.AcceptTcpClient();
                    break;
                }
                counter += 100; //wait 100 ms each time
                Thread.Sleep(100);
            }

            if (client == null) //no one has connected back to us!
                throw new IOException("Could not start process for client platform " + clientPlatform);

            ExecutionClientInfo clientInfo = new ExecutionClientInfo(proc, clientPlatform, client);

            return clientInfo;
        }

        void proc_Exited(object sender, EventArgs e)
        {
            if (disposed)
                return;

            try
            {
                Process proc = sender as Process;
                if (proc != null && proc.Handle != IntPtr.Zero)
                {
                    //try to remove the process from our dictionary.
                    lock (clientInfos)
                    {
                        var itemsToRemove = new List<ClientPlatform>();
                        foreach (var item in clientInfos)
                        {
                            if (item.Value.Handle == proc.Handle)
                            {
                                itemsToRemove.Add(item.Key);
                            }
                        }

                        foreach (var item in itemsToRemove)
                        {
                            clientInfos.Remove(item);
                        }
                    }

                    logger.Info("Process {0} ended at {1}.", proc.StartInfo.FileName, proc.ExitTime);
                }
                else
                {
                    logger.Info("An unknown child process has ended");
                }
            }
            catch (Exception ex)
            {
                //log and ignore...
                logger.Warn("Error in proc_Exited event. Continuing...", ex);
            }
        }

        #endregion

        #region IDisposable Members

        private bool disposed = false;
        void IDisposable.Dispose()
        {
            if (disposed)
                return;

            foreach (ExecutionClientInfo clientInfo in clientInfos.Values)
            {
                TryStopClient(clientInfo);
            }

            try
            {
                if (listener != null)
                    listener.Stop();
            }
            catch (SocketException) { } //ignore

            listener = null;

            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
                throw new ObjectDisposedException("This JobDispatcher object was previously disposed.");
        }

        #endregion

        //class to keep track of an executor process that runs a certain type of job
        private class ExecutionClientInfo
        {
            private ClientPlatform clientPlatform;
            private Process childProcess;
            private TcpClient client;
            private IntPtr processHandle;

            public ExecutionClientInfo(Process childProcess, ClientPlatform clientPlatform, TcpClient client)
            {
                this.clientPlatform = clientPlatform;
                this.childProcess = childProcess;
                this.client = client;
                this.processHandle = childProcess.Handle;
            }

            public TcpClient Client
            {
                get { return client; }
            }
            public Process ChildProcess
            {
                get { return childProcess; }
            }
            public ClientPlatform ClientPlatform
            {
                get { return clientPlatform; }
            }
            public IntPtr Handle
            {
                get { return processHandle; }
            }
        }

    }
}
