using System;
using System.Text;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor
{
    internal class ExecutorConfiguration
    {
        /// <summary>
        /// Default ping interval for executors
        /// </summary>
        public const long DefaultPingInterval = 8000;

        private string id; //persistence Id : assigned at server

        private Processor[] processors;
        private Disk[] disks;
        private Memory memory;

        private OperatingSystem operatingSystem;

        private string uri;

        //todoDecide: should all these properties below be in a seperate class and/or db table?
        private bool dedicated;
        private long pingInterval = DefaultPingInterval; //this tells the manager when to expect the next ping
        private DateTime lastPingTime; //this is used only on the manager side to determine when the executor has last pinged.

        public ExecutorConfiguration(string id, SystemInfo info)
        {
            if (id == null)
                throw new ArgumentNullException("id");
            if (String.IsNullOrEmpty(id))
                throw new ArgumentException("id cannot be empty", "id");
            if (info == null)
                throw new ArgumentNullException("info");

            this.id = id;
            this.processors = new Processor[info.Processors.Length];
            for (int i=0;i<info.Processors.Length;i++) {
                this.processors[i] = new Processor(info.Processors[i]);
            }
            this.disks = new Disk[info.Disks.Length];
            for (int i = 0; i < info.Disks.Length; i++)
            {
                this.disks[i] = new Disk(info.Disks[i]);
            }
            this.memory = new Memory(info.Memory);
            this.operatingSystem = new OperatingSystem(info.OS);
            this.uri = info.Uri;
            this.dedicated = info.Dedicated;
            this.pingInterval = info.PingInterval;
            this.lastPingTime = info.LastPingTime;
        }

        public void UpdatePerformance(PerformanceInfo info)
        {
            if (!info.SystemId.Equals(this.id))
                throw new ArgumentException("The Ids do not match.", "id");

            for (int i = 0; i < this.processors.Length; i++)
            {
                for (int j = 0; j < info.Processors.Length; j++)
                {
                    if (this.processors[i].Name.Equals(info.Processors[j].Name))
                    {
                        this.processors[i].UpdatePerformance(info.Processors[j]);
                        break;
                    }
                }
            }
            for (int i = 0; i < this.disks.Length; i++)
            {
                for (int j = 0; j < info.Disks.Length; j++)
                {
                    if (this.disks[i].Root.Equals(info.Disks[j].Root))
                    {
                        this.disks[i].UpdatePerformance(info.Disks[j]);
                        break;
                    }
                }
            }
            this.memory.UpdatePerformance(info.Memory);
            this.lastPingTime = info.LastPingTime;
        }

        public void UpdateLimits(LimitInfo info)
        {
            if (!info.SystemId.Equals(this.id))
                throw new ArgumentException("The Ids do not match.", "id");

            for (int i = 0; i < this.processors.Length; i++)
            {
                for (int j = 0; j < info.Processors.Length; j++)
                {
                    if (this.processors[i].Name.Equals(info.Processors[j].Name))
                    {
                        this.processors[i].UpdateLimits(info.Processors[j]);
                        break;
                    }
                }
            }
            for (int i = 0; i < this.disks.Length; i++)
            {
                for (int j = 0; j < info.Disks.Length; j++)
                {
                    if (this.disks[i].Root.Equals(info.Disks[j].Root))
                    {
                        this.disks[i].UpdateLimits(info.Disks[j]);
                        break;
                    }
                }
            }
            this.memory.UpdateLimits(info.Memory);
        }

        /// <summary>
        /// Gets the id of the system
        /// </summary>
        public string Id
        {
            get { return id; }
        }

        /// <summary>
        /// Clears the Id in case we want to save as a new object
        /// </summary>
        public void ClearId()
        {
            this.id = null;
        }

        /// <summary>
        /// Gets the processor information of the system
        /// </summary>
        public Processor[] Processors
        {
            get { return processors; }
        }

        /// <summary>
        /// Gets the hard disk information of the system
        /// </summary>
        public Disk[] Disks
        {
            get { return disks; }
        }

        /// <summary>
        /// Gets the memory information of the system
        /// </summary>
        public Memory Memory
        {
            get { return memory; }
//            private set { this.memory = value; }
        }

        /// <summary>
        /// Gets the operation system information of the system
        /// </summary>
        public OperatingSystem OperatingSystem
        {
            get { return operatingSystem; }
        }

        /// <summary>
        /// Gets the uri of the system
        /// </summary>
        public string Uri
        {
            get { return uri; }
        }

        /// <summary>
        /// Gets a value indicating if this system is a dedicated node
        /// </summary>//todoDoc: provide more info / links to show what is meant by dedicated
        public bool Dedicated
        {
            get { return dedicated; }
        }

        // Commented out since it is currently unused.
        //
        ///// <summary>
        ///// Gets the interval (in milliseconds) between consecutive 'ping's of this Executor. This value tells the Manager when to expect the next 'ping' / contact. 
        ///// </summary>
        //public long PingInterval
        //{
        //    get { return pingInterval; }
        //}

        /// <summary>
        /// Gets the date when this Executor last contacted the manager.
        /// </summary>
        public DateTime LastPingTime
        {
            get { return lastPingTime; }
        }

        public SystemInfo ToSystemInfo()
        {
            ProcessorSystemInfo[] processors = new ProcessorSystemInfo[this.processors.Length];
            for (int i = 0; i < processors.Length; i++)
            {
                processors[i] = this.processors[i].ToProcessorSystemInfo();
            }
            DiskSystemInfo[] disks = new DiskSystemInfo[this.disks.Length];
            for (int i = 0; i < disks.Length; i++)
            {
                disks[i] = this.disks[i].ToDiskSystemInfo();
            }
            SystemInfo info = new SystemInfo(this.id, processors, disks, this.memory.ToMemorySystemInfo(), 
                this.operatingSystem.ToOSInfo(), this.uri, this.dedicated, this.pingInterval);
            info.LastPingTime = this.LastPingTime;
            return info;
        }

        public PerformanceInfo ToPerformanceInfo()
        {
            ProcessorPerformanceInfo[] processors = new ProcessorPerformanceInfo[this.processors.Length];
            for (int i = 0; i < processors.Length; i++)
            {
                processors[i] = this.processors[i].ToProcessorPerformanceInfo();
            }
            DiskPerformanceInfo[] disks = new DiskPerformanceInfo[this.disks.Length];
            for (int i = 0; i < disks.Length; i++)
            {
                disks[i] = this.disks[i].ToDiskPerformanceInfo();
            }
            PerformanceInfo info = new PerformanceInfo(this.id, processors, disks, this.memory.ToMemoryPerformanceInfo());
            info.LastPingTime = this.LastPingTime;
            return info;
        }

        public LimitInfo ToLimitInfo()
        {
            ProcessorLimitInfo[] processors = new ProcessorLimitInfo[this.processors.Length];
            for (int i = 0; i < processors.Length; i++)
            {
                processors[i] = this.processors[i].ToProcessorLimitInfo();
            }
            DiskLimitInfo[] disks = new DiskLimitInfo[this.disks.Length];
            for (int i = 0; i < disks.Length; i++)
            {
                disks[i] = this.disks[i].ToDiskLimitInfo();
            }
            LimitInfo info = new LimitInfo(this.id, processors, disks, this.memory.ToMemoryLimitInfo());

            return info;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Executor Configuration : {0} ", id).AppendLine();

            if (processors != null)
            {
                sb.AppendFormat("- Processors : {0}", processors.Length).AppendLine();
                for (int i = 0; i < processors.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, processors[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No processors.");
            }

            if (disks != null)
            {
                sb.AppendFormat("- Disks : {0}", disks.Length).AppendLine();
                for (int i = 0; i < disks.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, disks[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No disks.");
            }

            sb.AppendFormat("- Memory : {0}", memory).AppendLine();
            sb.AppendFormat("- OperatingSystem : {0}", operatingSystem).AppendLine();
            sb.AppendFormat("- Uri : {0}", uri).AppendLine();
            sb.AppendFormat("- Dedicated : {0}", dedicated).AppendLine();
            sb.AppendFormat("- Last Ping Time : {0}", lastPingTime).AppendLine();
            sb.AppendFormat("- Ping Interval : {0}", pingInterval).AppendLine();

            return sb.ToString();
        }
    }
}
