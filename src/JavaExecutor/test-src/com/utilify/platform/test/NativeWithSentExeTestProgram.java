package com.utilify.platform.test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.DependencyType;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.JobCompletedEvent;
import com.utilify.framework.JobCompletedListener;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.Job;
import com.utilify.framework.client.NativeExecutable;
import com.utilify.framework.client.ResultContent;
import com.utilify.framework.client.ResultStatusInfo;

public class NativeWithSentExeTestProgram implements JobCompletedListener, ErrorListener {

    private static Logger logger = Logger.getLogger(NativeWithSentExeTestProgram.class.getName());
    private static Application app = null;

    public static void main(String[] args) throws IOException, 
    	ApplicationInitializationException, InterruptedException {

    	Logger rootLogger = Logger.getLogger("");
    	Handler[] handlers = rootLogger.getHandlers();
    	for (int i = 0; i < handlers.length; i++){
    		if (handlers[i] instanceof ConsoleHandler){
    			handlers[i].setLevel(Level.ALL);
    		}
    	}
    	
        logger.info("Started logging...");

        NativeWithSentExeTestProgram prog = new NativeWithSentExeTestProgram();
        prog.test();
        
        System.out.println("Press enter to exit...");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }

    private void test() throws ApplicationInitializationException, InterruptedException, MalformedURLException, IOException {
    	logger.fine("Starting client api test...");

        app = new Application("My Native Application With Sent Exe");

        List<DependencyInfo> dependencies = new ArrayList<DependencyInfo>();
        dependencies.add(new DependencyInfo("test.exe", "D:/test.exe", DependencyType.NATIVE_MODULE));
        for (int i = 0; i < 5; i++) {
        	NativeExecutable job = new NativeExecutable("test.exe", null, dependencies, null);
            app.addJob(job);
        }

        app.addErrorListener(this);
        app.addJobCompletedListener(this);
        
        logger.info("Submitting Application");
        app.start();

        System.out.println("Waiting for results...");
    }
    
	public void onError(ErrorEvent event) {
		System.out.println("I blew up: \n" +  event.getException().toString());
		if (event.getException().getCause() != null)
			System.out.print("Cause: ");
			event.getException().getCause().printStackTrace();
	}

	public void onJobCompleted(JobCompletedEvent event)  {
		Job remoteJob = event.getJob();
        NativeExecutable myJob = (NativeExecutable)remoteJob.getCompletedInstance();
        String s = String.format("Job %s Completed in %d ms. Ran: %s %s. Out: %s. Err: %s",
                        remoteJob.getId(),
                        (event.getJob().getFinishTime().getTime() - remoteJob.getStartTime().getTime()),
                        myJob.getExecutablePath(),
                        myJob.getArguments(),
                        myJob.getStdOutFile(),
                        myJob.getStdErrorFile());
        System.out.println(s);
	}
}
