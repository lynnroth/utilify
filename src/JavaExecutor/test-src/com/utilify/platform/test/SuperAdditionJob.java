package com.utilify.platform.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;

import com.utilify.framework.DependencyType;
import com.utilify.framework.ExecutionContext;
import com.utilify.framework.ResultRetrievalMode;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.Dependent;
import com.utilify.framework.client.Executable;
import com.utilify.framework.client.ResultInfo;
import com.utilify.framework.client.Results;

public class SuperAdditionJob implements Executable, Dependent, Results {

	private static final long serialVersionUID = 1L;
	
	private int valueA;
	private int valueB;
	private int result;
	private int inputValue = 0;
	private String inputFilename = "addition.input";
	private String resultFilename = "addition.result";

	public SuperAdditionJob(int valueA, int valueB) {
		this.valueA = valueA;
		this.valueB = valueB;
	}

	public void execute(ExecutionContext context) throws IOException, InterruptedException {

		// Try to Add extra number from input file.  	 
		String inputPath = context.getWorkingDirectory() + File.separator
				+ inputFilename;
		File inputFile = new File(inputPath);
		if (inputFile.exists()) {
			//Try read extra number to add from file
			BufferedReader reader = new BufferedReader(
					new FileReader(inputFile));
			String inputValueString = reader.readLine();
			inputValue = Integer.parseInt(inputValueString);
			reader.close();
		}
		
		Thread.sleep(10000);
		
		// Do Addition
		String resultPath = context.getWorkingDirectory() + File.separator
				+ resultFilename;
		System.out.println("Adding " + valueA + " and " + valueB + " and "
				+ inputValue + " to " + resultPath + ".");
		this.result = this.valueA + this.valueB + this.inputValue;
		File outputFile = new File(resultPath);
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		String line = result + "\n";
		writer.write(line, 0, line.length());
		writer.flush();
		writer.close();
		
		//Random random = new Random(System.currentTimeMillis());
		//int next = random.nextInt((int)System.currentTimeMillis()/result * 1000);
		//System.out.print("Sleeping for " + next + " ms.");
	}

	public int getValueA() {
		return this.valueA;
	}

	public int getValueB() {
		return this.valueB;
	}

	public int GetInputValue() {
		return this.inputValue;
	}

	public int getResult() {
		return this.result;
	}

	public DependencyInfo[] getDependencies() {
		DependencyInfo info = null;
		try {
			info = new DependencyInfo("addition.input", "testData/addition.input",
					DependencyType.DATA_FILE);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new DependencyInfo[] { info };
	}

	public ResultInfo[] getExpectedResults() {
		ResultInfo info = new ResultInfo("addition.result",
				ResultRetrievalMode.BACK_TO_ORIGIN);
		return new ResultInfo[] { info };
	}
}