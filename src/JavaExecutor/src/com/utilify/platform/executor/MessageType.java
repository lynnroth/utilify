package com.utilify.platform.executor;

public enum MessageType {
    EXECUTE_JOB("ExecuteJob"),
    GET_COMPLETED_JOBS("GetCompletedJobs"),
    ABORT_JOB("AbortJob"),
    SHUTDOWN("Shutdown");
    
    private final String value;
    
    MessageType(String v) {
    	value = v;
    }

	public String value() {
	    return value;
	}

	public static MessageType fromValue(String v) {
	    for (MessageType c: MessageType.values()) {
	        if (c.value.equals(v)) {
	            return c;
	        }
	    }
	    throw new IllegalArgumentException(v);
	}

	public static boolean isDefined(String value) {
	    for (MessageType msgType: MessageType.values()) {
	        if (msgType.value.equals(value)) {
	            return true;
	        }
	    }
		return false;
	}    
}
