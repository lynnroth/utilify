﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using Utilify.Platform;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Utilify.Platform.Runtime")]
[assembly: AssemblyDescription("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

//required to make sure this library is usable by all .NET / CLR Languages
[assembly: CLSCompliant(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2240f02a-d446-462a-81f0-dfdd93f08bc4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]

#if TEST
//[assembly: InternalsVisibleTo("Utilify.Runtime.Tests")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Manager.Tests")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Executor.Tests")]
//[assembly: InternalsVisibleTo("TestConsole")]
//[assembly: InternalsVisibleTo("Client")]
#endif

//[assembly: InternalsVisibleTo("Utilify.Tools.ManagementDashboard")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Manager")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Executor")]
