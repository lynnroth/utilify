using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class JobExecutionInfo
    {
        private String executorId;
        private String[] jobIds;

        public JobExecutionInfo(String executorId, String[] jobIds)
        {
            this.ExecutorId = executorId;
            this.JobIds = jobIds;
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ExecutorId
        {
            get { return executorId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("executorId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("ExecutorId cannot be empty", "executorId");
                executorId = value;
            }
        }

        /// <summary>
        /// Gets the list of JobIds that the executor is running.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public String[] JobIds
        {
            get { return jobIds; }
            private set
            {
                if (value == null)
                    jobIds = new String[0];
                else
                    jobIds = value;
            }
        }
    }
}
