using System.ComponentModel;
using System.Configuration;
using Utilify.Platform;

namespace Utilify.Platform.Configuration
{
    //todoLater: have a single system to unify both xml-parsing validation (via attributes and our own validation)

    /// <summary>
    /// Represents the "utilify.platform" configuration section
    /// </summary>
    [Description("Configuration settings used by a client connect to the Utilify Platform Manager")]
    public class ClientSection : ConfigurationSectionBase, IConfigurationElement
    {
        /// <summary>
        /// The name of the configuration section.
        /// </summary>
        public const string SectionName = "utilify.platform";
        /// <summary>
        /// The name of the clients collection element in configuration.
        /// </summary>
        internal const string ClientElementCollectionName = "clients";

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientSection"/> class.
        /// </summary>
        public ClientSection()
        {            
        }

        //we need the Description attribute for the XsdExtractor tool, which can be used to generate an xsd out of this configuration section
        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        /// <value>The clients.</value>
        [ConfigurationProperty(ClientElementCollectionName, IsRequired = true, IsDefaultCollection = false)]
        [Description("Connection settings used to connect to the Manager")]
        public ClientElementCollection Clients
        {
            get
            {
                ClientElementCollection settings = (ClientElementCollection)this[ClientElementCollectionName];
                return settings;
            }
            set 
            {
                this[ClientElementCollectionName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            IConfigurationElement ice = (Clients as IConfigurationElement);
            ice.Validate();
        }

        #endregion

    }

    /// <summary>
    /// Represents the client element collection that contains configuration for client elements.
    /// </summary>
    [ConfigurationCollection(typeof(ClientElement), AddItemName = ClientElementCollection.ClientElementName)]
    public class ClientElementCollection : ConfigurationElementCollection, IConfigurationElement
    {
        /// <summary>
        /// The name of the client element in configuration.
        /// </summary>
        internal const string ClientElementName = "client";

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ClientElement();
        }

        /// <summary>
        /// Gets the element key for a client configuration element.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ClientElement).ClientType;
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //todo: check for duplicates
        }

        #endregion

        /// <summary>
        /// Looks up the client for the given service type in the clients collection.
        /// If the service type if ServiceType.Default, this method returns the first client in the collection.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns></returns>
        public ClientElement LookupClient(ServiceType type)
        {
            return this.LookupClient(type.ToString());
        }

        /// <summary>
        /// Looks up the client for the given service type in the clients collection.
        /// The service type string has to be one of the values defined in the <see cref="ServiceType">ServiceType</see> enumeration.
        /// If the service type if 'Default', this method returns the first client in the collection.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns></returns>
        public ClientElement LookupClient(string type)
        {
            ValidationHelper.CheckNull(type, "type");

            ClientElement client = null;
            if (this.Count > 0 && type == ServiceType.Default.ToString())
            {
                client = (ClientElement)BaseGet(0);
            }
            else
            {
                for (int i = 0; i < this.Count; i++)
                {
                    ClientElement temp = (ClientElement)BaseGet(i);
                    if (temp.ClientType == type)
                    {
                        client = temp;
                        break;
                    }
                }
            }
            return client;
        }
    }
    
    /// <summary>
    /// Represents the client configuration element in the Client configuration section.
    /// </summary>
    public class ClientElement : ConfigurationElement, IConfigurationElement
    {
        /// <summary>
        /// The default host name
        /// </summary>
        internal const string DefaultHost = "localhost";
        /// <summary>
        /// The default server port
        /// </summary>
        internal const int DefaultPort = 8080;
        /// <summary>
        /// The default value for whether this client is an 'interop' client. Interop clients are those that do not run on the .NET framework
        /// or run on non-Windows platforms.
        /// </summary>
        internal const bool DefaultIsInteropClient = false;

        internal const string ClientTypePropertyName = "type";
        internal const string ServiceHostPropertyName = "serviceHost";
        internal const string ServicePortPropertyName = "servicePort";
        internal const string IsInteropClientPropertyName = "isInteropClient";

        internal const string SecurityElementName = "security";

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientElement"/> class.
        /// </summary>
        public ClientElement() { }

        /// <summary>
        /// Gets or sets the service host
        /// </summary>
        [ConfigurationProperty(ServiceHostPropertyName, DefaultValue = DefaultHost, IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 255)]
        [Description("Host name of the Manager to connect to")]
        public string ServiceHost
        {
            get { return (string)this[ServiceHostPropertyName]; }
            set 
            { 
                if (value != this.ServiceHost)
                    this[ServiceHostPropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the service type name.
        /// </summary>
        [ConfigurationProperty(ClientTypePropertyName, IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\")] //, MinLength = 1, MaxLength = 255
        [Description("Type name of the service to connect to")]
        public string ClientType
        {
            get { return (string)this[ClientTypePropertyName]; }
            set
            {
                if (value != this.ClientType)
                    this[ClientTypePropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the service port
        /// </summary>
        [ConfigurationProperty(ServicePortPropertyName, DefaultValue = DefaultPort, IsRequired = false)]
        [IntegerValidator(MinValue = 0, MaxValue = 65536, ExcludeRange = false)]
        [Description("Port number of the Manager to connect to")]
        public int ServicePort
        {
            get { return (int)this[ServicePortPropertyName]; }
            set 
            {
                if (value != this.ServicePort)
                    this[ServicePortPropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the whether this client connects to an interop endpoint (such as http/https)
        /// </summary>
        [ConfigurationProperty(IsInteropClientPropertyName, DefaultValue = DefaultIsInteropClient, IsRequired = false)]
        [Description("Specifies whether this client connects to an interop endpoint (such as http/https)")]
        public bool IsInteropClient
        {
            get { return (bool)this[IsInteropClientPropertyName]; }
            set
            {
                if (value != this.IsInteropClient)
                    this[IsInteropClientPropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the security credentials
        /// </summary>
        [ConfigurationProperty(SecurityElementName, IsRequired = false)]
        [Description("Security settings used to connect to the Manager")]
        public SecurityElement Security
        {
            get 
            {
                SecurityElement se = (SecurityElement)this[SecurityElementName];
                return se; 
            }
            set 
            {
                this[SecurityElementName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            IConfigurationElement ice = (Security as IConfigurationElement);
            if (ice != null)
                ice.Validate();
        }

        #endregion
    }

    /// <summary>
    /// Represents a security configuration element used to define the security settings used by the client.
    /// </summary>
    public class SecurityElement : ConfigurationElement, IConfigurationElement
    {
        internal const string CredentialTypePropertyName = "credentialType";
        internal const string UserNameElementName = "username";
        //internal const string ClientCertificateElementName = "clientCertificate";
        internal const string ServiceCertificateElementName = "serviceCertificate";

        internal const CredentialType DefaultCredentialType = CredentialType.Windows;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityElement"/> class.
        /// </summary>
        public SecurityElement() { }

        /// <summary>
        /// Gets or sets the type of the credential.
        /// </summary>
        /// <value>The type of the credential.</value>
        [ConfigurationProperty(CredentialTypePropertyName, DefaultValue = DefaultCredentialType, IsRequired = true)]
        [Description("The type of credential used to authenticate to the Manager")]
        public CredentialType CredentialType
        {
            get { return (CredentialType)this[CredentialTypePropertyName]; }
            set 
            { 
                if (value != this.CredentialType)
                    this[CredentialTypePropertyName] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The name of the user.</value>
        [ConfigurationProperty(UserNameElementName, IsRequired = false)]
        [Description("The username credential used to authenticate to the Manager. Required if CredentialType = UserName")]
        public UserNameElement UserName
        {
            get 
            { 
                UserNameElement une = (UserNameElement)this[UserNameElementName];
                return une;
            }
            set 
            {
                this[UserNameElementName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the service certificate.
        /// </summary>
        /// <value>The service certificate.</value>
        [ConfigurationProperty(ServiceCertificateElementName, IsRequired = false)]
        [Description("The service certificate details expected from the Manager")]
        public ServiceCertificateElement ServiceCertificate
        {
            get 
            {
                ServiceCertificateElement sce = (ServiceCertificateElement)this[ServiceCertificateElementName];
                return sce;
            }
            set 
            {
                this[ServiceCertificateElementName] = value;
            }
        }

        //[ConfigurationProperty(ClientCertificateElementName, IsRequired = false)]
        //public ClientCertificateElement ClientCertificate
        //{
        //    get
        //    {
        //        ClientCertificateElement cce = (ClientCertificateElement)this[ClientCertificateElementName];
        //        return cce;
        //    }
        //    set
        //    {
        //        this[ClientCertificateElementName] = value;
        //    }
        //}

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //check if the settings are correct:
            switch (CredentialType)
            {
                case CredentialType.Username:
                    if (UserName == null)
                        throw new ConfigurationErrorsException("Username credential must be specified if credentialType = Username");

                    (UserName as IConfigurationElement).Validate();

                    break;
                //case CredentialType.Certificate:
                //    if (ClientCertificate == null)
                //        throw new ConfigurationErrorsException("Client certificate must be specified if credentialType = Certificate");

                //    (ClientCertificate as IConfigurationElement).Validate();

                //    break;
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents the username configuration element.
    /// </summary>
    public class UserNameElement : ConfigurationElement, IConfigurationElement
    {
        internal const string UsernamePropertyName = "name";
        internal const string PasswordPropertyName = "password";

        /// <summary>
        /// Initializes a new instance of the <see cref="UserNameElement"/> class.
        /// </summary>
        public UserNameElement()         
        {
        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The name.</value>
        [ConfigurationProperty(UsernamePropertyName, IsRequired = true)]
        //[StringValidator(MinLength = 3, MaxLength = 255)]
        [Description("The username used to authenticate to the Manager")]
        public string Name
        {
            get { return (string)this[UsernamePropertyName]; }
            set 
            {
                if (value != this.Name)
                    this[UsernamePropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [ConfigurationProperty(PasswordPropertyName, IsRequired = true)]
        //[StringValidator(MinLength = 6, MaxLength = 255)]
        [Description("The password used to authenticate to the Manager.")] //This is stored as an encrypted string, and is not generally meant to be edited manually.
        public string Password
        {
            get 
            {
                var tempPwd = (string)this[PasswordPropertyName];
                try
                {
                    var pwd = EncryptionHelper.Decrypt(tempPwd);
                    return pwd;
                }
                catch (System.Exception)
                {
                    return tempPwd;
                }                
            }
            set 
            { 
                //if (value != this.Password)
                //    this[PasswordPropertyName] = EncryptionHelper.Encrypt(value);
                if (value != this.Password)
                    this[PasswordPropertyName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //nothing to do.
        }

        #endregion
    }

    /// <summary>
    /// Represents the service certificate configuration element.
    /// </summary>
    public class ServiceCertificateElement : ConfigurationElement, IConfigurationElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceCertificateElement"/> class.
        /// </summary>
        public ServiceCertificateElement() { }

        internal const string DnsIdentityPropertyName = "dnsIdentity";
        internal const string ValidatePropertyName = "validate";

        /// <summary>
        /// Gets or sets the DNS identity of the service.
        /// </summary>
        /// <value>The DNS identity.</value>
        [ConfigurationProperty(DnsIdentityPropertyName, IsRequired = true)]
        [StringValidator(MaxLength = 255)]
        [Description("The dns identity expected from the Manager")]
        public string DnsIdentity
        {
            get { return (string)this[DnsIdentityPropertyName]; }
            set 
            { 
                if (value != this.DnsIdentity)
                    this[DnsIdentityPropertyName] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ServiceCertificateElement"/> is to be validated at the client.
        /// </summary>
        /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
        [ConfigurationProperty(ValidatePropertyName, IsRequired = true)]
        [Description("Specifies whether to validate the Manager certificate")]
        public bool Validate
        {
            get { return (bool)this[ValidatePropertyName]; }
            set 
            { 
                if (value != this.Validate)
                    this[ValidatePropertyName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //nothing to do
        }

        #endregion
    }

}
