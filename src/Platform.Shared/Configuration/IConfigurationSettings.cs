﻿using System.Configuration;

namespace Utilify.Platform.Configuration
{
    /// <summary>
    /// Specifies the contract for a configuration setting.
    /// </summary>
    /// <typeparam name="T">The type of the configuration element represented by the setting.</typeparam>
    public interface IConfigurationSettings<T> where T : ConfigurationElement
    {
        /// <summary>
        /// Gets the validation errors.
        /// </summary>
        /// <returns></returns>
        string GetValidationErrors();
        /// <summary>
        /// Updates the configuration element.
        /// </summary>
        /// <param name="element">The element whose values need to be updated.</param>
        void UpdateConfigurationElement(ref T element);
        /// <summary>
        /// Loads the configuration.
        /// </summary>
        /// <param name="element">The element to load configuration from.</param>
        /// <returns></returns>
        IConfigurationSettings<T> LoadConfiguration(T element); 
    }
}
