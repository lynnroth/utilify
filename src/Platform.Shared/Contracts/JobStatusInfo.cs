using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    //todoDoc (JobStatusInfo)

    /// <summary>
    /// Represents the status information of a job.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class JobStatusInfo
    {
        private string applicationId;
        private string executionHost;
        private DateTime finishTime;
        private string jobId;
        private DateTime startTime;
        private JobStatus status;

        private DateTime submittedTime;
        private DateTime scheduledTime;

        private JobStatusInfo() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobStatusInfo"/> class.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="jobId">The job id.</param>
        /// <param name="executionHost">The execution host.</param>
        /// <param name="submittedTime">The submitted time.</param>
        /// <param name="scheduledTime">The scheduled time.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="finishTime">The finish time.</param>
        /// <param name="status">The status.</param>
        public JobStatusInfo(string applicationId, string jobId, string executionHost,
            DateTime submittedTime, DateTime scheduledTime, DateTime startTime, DateTime finishTime,
            JobStatus status) : this()
        {
            this.ApplicationId = applicationId;
            this.JobId = jobId;
            this.ExecutionHost = executionHost;
            this.SubmittedTime = submittedTime;
            this.ScheduledTime = scheduledTime;
            this.StartTime = startTime;
            this.FinishTime = finishTime;
            this.Status = status;
        }

        /// <summary>
        /// Gets the application id.
        /// </summary>
        /// <value>The application id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationId
        {
            get { return applicationId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application id cannot be empty", "applicationId");
                
                applicationId = value;
            }
        }

        /// <summary>
        /// Gets the execution host.
        /// </summary>
        /// <value>The execution host.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ExecutionHost
        {
            get { return executionHost; }
            private set { executionHost = value; }
        }

        /// <summary>
        /// Gets the submitted time.
        /// </summary>
        /// <value>The submitted time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime SubmittedTime
        {
            get { return submittedTime; }
            private set
            {
                submittedTime = Helper.MakeUtc(value);
            }
        }

        /// <summary>
        /// Gets the scheduled time.
        /// </summary>
        /// <value>The scheduled time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime ScheduledTime
        {
            get { return scheduledTime; }
            private set
            {
                scheduledTime = Helper.MakeUtc(value);
            }
        }

        /// <summary>
        /// Gets the finish time.
        /// </summary>
        /// <value>The finish time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime FinishTime
        {
            get { return finishTime; }
            private set 
            {
                finishTime = Helper.MakeUtc(value);
            }
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The job id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string JobId
        {
            get { return jobId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("jobId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Job id cannot be empty", "jobId");

                jobId = value;
            }
        }

        /// <summary>
        /// Gets the start time.
        /// </summary>
        /// <value>The start time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime StartTime
        {
            get { return startTime; }
            private set
            {
                startTime = Helper.MakeUtc(value);
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public JobStatus Status
        {
            get { return status; }
            private set { status = value; }
        }
    }
}