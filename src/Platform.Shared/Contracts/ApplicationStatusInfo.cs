using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents the status information for an application.
    /// An instance of this class is used to transfer data between the service and the client to communicate status information.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ApplicationStatusInfo
    {
        private string applicationId;
        private long completedJobs;
        private long numberOfJobs;
        private DateTime startTime;
        private ApplicationStatus status;

        private ApplicationStatusInfo() {}

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationStatusInfo"/> class.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="status">The status.</param>
        /// <param name="completedJobs">The completed jobs.</param>
        /// <param name="numberOfJobs">The number of jobs.</param>
        public ApplicationStatusInfo(string applicationId, DateTime startTime, ApplicationStatus status, long completedJobs, long numberOfJobs)
            : this()
        {
            this.ApplicationId = applicationId;
            this.StartTime = startTime;
            this.Status = status;

            if ((completedJobs > numberOfJobs) && (completedJobs > 0) && (numberOfJobs > 0))
                throw new ArgumentException("Completed jobs cannot be greated than the total number of jobs");

            this.CompletedJobs = completedJobs;
            this.NumberOfJobs = numberOfJobs;
            
        }

        /// <summary>
        /// Gets or sets the application id.
        /// </summary>
        /// <value>The application id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationId
        {
            get { return applicationId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationId");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application id cannot be empty");

                applicationId = value;
            }
        }

        /// <summary>
        /// Gets or sets the completed jobs.
        /// </summary>
        /// <value>The completed jobs.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long CompletedJobs
        {
            get { return completedJobs; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("completedJobs", "Completed jobs cannot be less than zero");
                completedJobs = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of jobs.
        /// </summary>
        /// <value>The number of jobs.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long NumberOfJobs
        {
            get { return numberOfJobs; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("numberOfJobs", "Number of jobs cannot be less than zero");
                numberOfJobs = value;
            }
        }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>The start time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime StartTime
        {
            get { return startTime; }
            private set { startTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ApplicationStatus Status
        {
            get { return status; }
            private set { status = value; }
        }
    }
}