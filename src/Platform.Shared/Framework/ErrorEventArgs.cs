using System;
namespace Utilify.Framework
{
    /// <summary>
    /// Contains the event data for an error event, and encapsulates the Exception as an EventArgs subclass.
    /// </summary>
    public class ErrorEventArgs : EventArgs
    {
        private Exception error;
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorEventArgs"/> class.
        /// </summary>
        public ErrorEventArgs(): base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorEventArgs"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        public ErrorEventArgs(Exception error): base()
        {
            this.error = error;
        }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error.</value>
        public Exception Error
        {
            get { return error; }
            set { error = value; }
        }
    }
}