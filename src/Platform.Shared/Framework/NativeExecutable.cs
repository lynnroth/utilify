using System;
using Utilify.Platform;

namespace Utilify.Framework
{
    // Reasons why this approach is nice:
    // - More flexible. Not restricted to 1 execute. the post process may also be a native executable. or
    //   we may want some complex sequence of managed code and native code.
    // - Some values such as the arguments or executable path may need to be decided at runtime.
    //   (could be done in the pre-process though)
    // - Keeps the code style for clr and native EXECUTABLE very similar and could actually/almost combine them.
    // - Keeps the implementation of the EXECUTORS similar and could almost combine them too.
    // - If wanting to have a local executing and remote executing version of a job (eg. with the renderer),
    //   the exact same job can be used without adding special code within the job itself for handling the local execution.
    // - 

    /// <summary>
    /// The base class for user code that should run an existing executable file remotely.
    /// The derived class can optionally implement code for the Execute method - and must call the NativeExecute method (if it needs to run 
    /// a native executable such as a .exe file).
    /// </summary>
    [Serializable]
    public class NativeExecutable : IExecutable
    {
        // todoLater: this is made public so that we can listen to the event and also execute locally, but it could be confusing
        //       when creating jobs and thinking you can recieve an event when the job starts executing remotely.
        //
        // Maybe create some helper in the Client package (so it can access internal members) that can be used to listen to
        // and execute on ExecutionStarted Events.
        //

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeExecutable"/> class.
        /// </summary>
        protected NativeExecutable() { }

        private ExecutionArgs executionArgs;

        /// <summary>
        /// Gets the execution args used to run this native executable job.
        /// </summary>
        /// <value>The execution args.</value>
        public ExecutionArgs ExecutionArgs
        {
            get
            {
                return this.executionArgs;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeExecutable"/> class.
        /// </summary>
        /// <param name="executablePath">The executable path.</param>
        /// <param name="arguments">The arguments (optional).</param>
        public NativeExecutable(string executablePath, string arguments)
            : this(executablePath, arguments, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeExecutable"/> class.
        /// </summary>
        /// <param name="executablePath">The executable path.</param>
        /// <param name="arguments">The arguments (optional).</param>
        /// <param name="stdOutFile">The standard out file (optional).</param>
        /// <param name="stdErrFile">The standard err file (optional).</param>
        public NativeExecutable(string executablePath, string arguments, string stdOutFile, string stdErrFile)
            : this()
        {
            this.executionArgs = new ExecutionArgs(executablePath, arguments, stdOutFile, stdErrFile, null);
        }

        /// <summary>
        /// Occurs when execution is started.
        /// This event is for internal use only.
        /// It is not raised on the client / local machine. 
        /// </summary>
        public event EventHandler<ExecutionEventArgs> ExecutionStarted;

        /// <summary>
        /// Runs the executable file specified in the arguments.
        /// This method is meant to be called by a derived class as part of its Execute method.
        /// </summary>
        /// <param name="args">The args.</param>
        protected void NativeExecute(ExecutionArgs args)
        {
            // pass args/info onto OnExecutionStarted
            if (args == null)
                throw new ArgumentNullException("args");
            this.executionArgs = args;
            OnExecutionStarted(args);
        }

        private void OnExecutionStarted(ExecutionArgs args)
        {
            ExecutionEventArgs eventArgs = new ExecutionEventArgs(args);
            EventHelper.RaiseEvent<ExecutionEventArgs>(ExecutionStarted, this, eventArgs);
        }

        /// <summary>
        /// Removes all registered listeners.
        /// This method is for internal use only.
        /// </summary>
        public void RemoveListeners()
        {
            this.ExecutionStarted = null;
        }

        #region IExecutable Members

        /// <summary>
        /// Executes the job with the specified context.
        /// </summary>
        /// <param name="context">The job context.</param>
        public virtual void Execute(ExecutionContext context)
        {
            executionArgs.WorkingDirectory = context.WorkingDirectory;
            this.NativeExecute(executionArgs);
        }

        #endregion
    }

    /// <summary>
    /// Specifies the parameters required for running a native executable file (such as a .exe file on Windows).
    /// </summary>
    [Serializable]
    public class ExecutionArgs 
    {
        private String executablePath;
        private String arguments;
        private String stdOutFile;
        private String stdErrFile;
        private String workingDirectory;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExecutionArgs"/> class.
        /// </summary>
        /// <param name="executablePath">The executable path.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="stdOutFile">The standard out file.</param>
        /// <param name="stdErrFile">The standard error file.</param>
        /// <param name="workingDirectory">The working directory.</param>
        public ExecutionArgs(String executablePath, String arguments, String stdOutFile, String stdErrFile, String workingDirectory)
        {
            if (executablePath == null || executablePath.Trim().Length == 0)
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "executablePath");

            //we can have safe defaults for other values: stdout, stderr files : we ignore if not given
            //working directory can be set to current dir / context working directory.

            this.ExecutablePath = executablePath;
            this.Arguments = arguments;
            this.StdOutFile = stdOutFile;
            this.StdErrFile = stdErrFile;
            this.WorkingDirectory = workingDirectory;
        }

        /// <summary>
        /// Gets or sets the executable path.
        /// </summary>
        /// <value>The executable path.</value>
        public String ExecutablePath
        {
          get { return executablePath; }
          private set 
          {
              executablePath = value;
          }
        }

        /// <summary>
        /// Gets or sets the arguments to the executable.
        /// </summary>
        /// <value>The arguments.</value>
        public String Arguments
        {
          get { return arguments; }
          private set { arguments = value; }
        }

        /// <summary>
        /// Gets or sets the standard output file path that will be written to, by the process.
        /// </summary>
        /// <value>The path to the stdout file.</value>
        public String StdOutFile
        {
          get { return stdOutFile; }
          private set { stdOutFile = value; }
        }

        /// <summary>
        /// Gets or sets the standard error file path that will be written to, by the process.
        /// </summary>
        /// <value>The path to the stderr file.</value>
        public String StdErrFile
        {
          get { return stdErrFile; }
          private set { stdErrFile = value; }
        }

        /// <summary>
        /// Gets or sets the working directory for the process running the executable.
        /// </summary>
        /// <value>The working directory.</value>
        public String WorkingDirectory
        {
          get { return workingDirectory; }
          internal set { workingDirectory = value; }
        }
    }

    /// <summary>
    /// Contains the data for the execution started event. 
    /// This class is for internal use only.
    /// </summary>
    [Serializable]
    public class ExecutionEventArgs : EventArgs
    {
        private ExecutionArgs executionArgs;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExecutionEventArgs"/> class.
        /// </summary>
        /// <param name="executionArgs">The execution args.</param>
        public ExecutionEventArgs(ExecutionArgs executionArgs)
        {
            this.ExecutionArgs = executionArgs;
        }

        /// <summary>
        /// Gets or sets the execution args.
        /// </summary>
        /// <value>The execution args.</value>
        public ExecutionArgs ExecutionArgs
        {
            get { return executionArgs; }
            set { executionArgs = value; }
        }
    }
}
