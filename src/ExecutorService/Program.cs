using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using log4net;

namespace Utilify.Platform.Executor
{
    static class Program
    {
        private static ILog log;

        //todoConfig:
        //Do we need a UI for managing connection settings to connect to the Manager?
        //Should we connect using certificates instead of username/pwd?
        //Other options to config: 
        // - thread poll intervals
        // - executor resource usage limits, allocated times etc
        // - process management options for real-executors such as log file location, re-cycling options, port number to connect back
        // - default applications folder, 
        // - dependency cache options: default folder, max total size, how long to cache etc
        // - log file location.
        // - max. number of jobs to queue locally - to control when the executor asks for more jobs.

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            //the unhandled exception handler is set here as opposed to the constructor, 
            //since the Main does a lot more things that can cause errors.
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(DefaultErrorEventHandler);

            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            log = log4net.LogManager.GetLogger(typeof(Program));

#if DEBUG
            System.Diagnostics.Trace.Listeners.Add(new Log4netTraceListener(log));
#endif
            Logger.MessageLogged += new EventHandler<LogEventArgs>(Logger_MessageLogged_Log4Net);

            string opt = null;
            if (args.Length > 0)
            {
                opt = args[0];
                if (opt != null)
                    opt = opt.Trim();
            }

            if (opt != null && opt.Equals("/install", StringComparison.CurrentCultureIgnoreCase))
            {
                //install the service
                InstallService();
            }
            else if (opt != null && opt.Equals("/uninstall", StringComparison.CurrentCultureIgnoreCase))
            {
                //uninstall the service
                UninstallService();
            }
            else if ("/console".Equals(opt, StringComparison.CurrentCultureIgnoreCase))
            {
                ExecutorService es = new ExecutorService();

                var method = (typeof(ExecutorService)).GetMethod("OnStart", BindingFlags.NonPublic | BindingFlags.Instance);
                method.Invoke(es, new[] { args });

                Console.WriteLine("Press any key to stop");
                Console.ReadKey();

                method = (typeof(ExecutorService)).GetMethod("OnStop", BindingFlags.NonPublic | BindingFlags.Instance);
                method.Invoke(es, null);

            }
            else
            {
                //start the service
                ExecutorService es = new ExecutorService();
                if (!WindowsServiceHelper.CheckServiceInstallation(es.ServiceName))
                {
                    InstallService();
                }

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { es };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static void DefaultErrorEventHandler(object sender, UnhandledExceptionEventArgs args)
        {
            if (log != null)
            {
                Exception ex = (Exception)args.ExceptionObject;
                log.Fatal("Application terminating = " + args.IsTerminating + ". Fatal unhandled error from " + sender,
                             ex);
            }
        }

        private static void InstallService()
        {
#if !MONO
            string path = string.Format("/assemblypath={0}", Assembly.GetExecutingAssembly().Location);
            WindowsServiceHelper.InstallService(new ProjectInstaller(), path);
#endif
        }

        private static void UninstallService()
        {
#if !MONO
            string path = string.Format("/assemblypath={0}", Assembly.GetExecutingAssembly().Location);
            WindowsServiceHelper.UninstallService(new ProjectInstaller(), path);
#endif
        }

        //captures all framework log messages
        private static void Logger_MessageLogged_Log4Net(object sender, LogEventArgs e)
        {
            string format = "<{0}:{1}> {2} - {3} {4}";
            switch (e.Level)
            {
                case LogLevel.Debug:
                    log.DebugFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Info:
                    log.InfoFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Warn:
                case LogLevel.Error:
                    log.ErrorFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
            }
        }

    }
}